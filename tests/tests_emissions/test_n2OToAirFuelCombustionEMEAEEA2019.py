import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.emissions.n2OToAirFuelCombustionEMEAEEA2019 import N2OToAirFuelCombustionEMEAEEA2019


class Test_N2OToAirFuelCombustionEMEAEEA2019(unittest.TestCase):
    def __init__(self, *args, **kwargs):

        super(Test_N2OToAirFuelCombustionEMEAEEA2019, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()

        engine.import_data(cycle, site)

        self.model = N2OToAirFuelCombustionEMEAEEA2019()

        self.representation = engine.data.safe_representation

        self.n2OToAirFuelCombustion_expected = 0.002468128

    def test_check_n2OToAirFuelCombustion(self):

        check_computed = self.model.check_n2OToAirFuelCombustion(self.representation)

        self.assertEqual(True, check_computed)

    def test_n2OToAirFuelCombustion(self):

        condition = self.model.check_n2OToAirFuelCombustion(self.representation)
        n2OToAirFuelCombustion_computed = \
            self.model.calculate_n2OToAirFuelCombustion() if condition else self.n2OToAirFuelCombustion_expected

        self.assertEqual(self.n2OToAirFuelCombustion_expected, n2OToAirFuelCombustion_computed)

    def test_check_n2OToAirFuelCombustion_no_inputs(self):

        del(self.representation['inputs'])
        self.representation['dataCompleteness']['electricityFuel'] = False
        check_computed = self.model.check_n2OToAirFuelCombustion(self.representation)

        self.assertEqual(False, check_computed)


if __name__ == '__main__':
    unittest.main()
