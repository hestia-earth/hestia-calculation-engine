import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.emissions.noxToAirCropResidueBurningAkagiEtAl2011AndIpcc2006\
    import NOXToAirCropResidueBurningAkagiEtAl2011AndIpcc2006


class Test_noxToAirCropResidueBurningAkagiEtAl2011AndIpcc2006(unittest.TestCase):
    def __init__(self, *args, **kwargs):

        super(Test_noxToAirCropResidueBurningAkagiEtAl2011AndIpcc2006, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()

        engine.import_data(cycle, site)

        self.model = NOXToAirCropResidueBurningAkagiEtAl2011AndIpcc2006()

        self.representation = engine.data.safe_representation

        self.noxToAirCropResidueBurning_expected = 10.202354999999999

    def test_check_noxToAirCropResidueBurning(self):

        check_computed = self.model.check_noxToAirCropResidueBurning(self.representation)

        self.assertEqual(True, check_computed)

    def test_check_noxToAirCropResidueBurning_no_products(self):

        del(self.representation['products'])
        self.representation['dataCompleteness']['cropResidue'] = False
        check_computed = self.model.check_noxToAirCropResidueBurning(self.representation)

        self.assertEqual(False, check_computed)

    def test_noxToAirCropResidueBurning(self):

        condition = self.model.check_noxToAirCropResidueBurning(self.representation)
        noxToAirCropResidueBurning_computed = \
            self.model.calculate_noxToAirCropResidueBurning()\
            if condition else self.noxToAirCropResidueBurning_expected

        self.assertEqual(self.noxToAirCropResidueBurning_expected, noxToAirCropResidueBurning_computed)


if __name__ == '__main__':
    unittest.main()
