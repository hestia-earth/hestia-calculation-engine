import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.emissions.backgroundmodel import Backgroungmodel


class Test_backgroundmodel(unittest.TestCase):
    def __init__(self, *args, **kwargs):

        super(Test_backgroundmodel, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()
        engine.import_data(cycle, site)
        self.model = Backgroungmodel()
        self.representation = engine.data.safe_representation

    def test_check_backgroundmodel(self):

        check_computed = self.model.check(self.representation)

        self.assertEqual(check_computed, check_computed)

    def test_calculate_background(self):

        condition = self.model.check(self.representation)
        computed = self.model.calculate_background('co2ToAirInputsProduction') if condition else 0
        self.assertEqual(computed, computed)

    def test_check_backgroundmodel_no_inputs(self):

        del(self.representation['inputs'])
        check_computed = self.model.check(self.representation)

        self.assertEqual(check_computed, check_computed)


if __name__ == '__main__':
    unittest.main()
