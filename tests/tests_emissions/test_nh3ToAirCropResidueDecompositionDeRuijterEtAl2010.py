import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.emissions.nh3ToAirCropResidueDecompositionDeRuijterEtAl2010\
    import NH3ToAirCropResidueDecompositionDeRuijterEtAl2010


class Test_nh3ToAirCropResidueDecompositionDeRuijterEtAl2010(unittest.TestCase):
    def __init__(self, *args, **kwargs):

        super(Test_nh3ToAirCropResidueDecompositionDeRuijterEtAl2010, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()

        engine.import_data(cycle, site)

        self.representation = engine.data.safe_representation

        self.model = NH3ToAirCropResidueDecompositionDeRuijterEtAl2010()

        self.nh3ToAirCropResidueDecomposition_expected = 0.0

    def test_check_nh3ToAirCropResidueDecomposition(self):

        check_computed = self.model.check_nh3ToAirCropResidueDecomposition(self.representation)

        self.assertEqual(True, check_computed)

    def test_nh3ToAirCropResidueDecomposition(self):

        condition = self.model.check_nh3ToAirCropResidueDecomposition(self.representation)
        nh3ToAirCropResidueDecomposition_computed = \
            self.model.calculate_nh3ToAirCropResidueDecomposition()\
            if condition else self.nh3ToAirCropResidueDecomposition_expected

        self.assertEqual(self.nh3ToAirCropResidueDecomposition_expected, nh3ToAirCropResidueDecomposition_computed)

    def test_check_nh3ToAirCropResidueDecomposition_no_products(self):

        del(self.representation['products'])
        self.representation['dataCompleteness']['cropResidue'] = False
        check_computed = self.model.check_nh3ToAirCropResidueDecomposition(self.representation)

        self.assertEqual(False, check_computed)


if __name__ == '__main__':
    unittest.main()
