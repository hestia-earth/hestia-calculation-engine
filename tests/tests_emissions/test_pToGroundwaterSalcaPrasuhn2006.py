import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.emissions.pToGroundwaterSalcaPrasuhn2006 import PToGroundwaterSalcaPrasuhn2006


class Test_pToGroundwaterSalcaPrasuhn2006(unittest.TestCase):
    def __init__(self, *args, **kwargs):

        super(Test_pToGroundwaterSalcaPrasuhn2006, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()

        engine.import_data(cycle, site)

        self.model = PToGroundwaterSalcaPrasuhn2006()

        self.representation = engine.data.safe_representation

        self.pToGroundwaterAllOrigins_expected = 0.0

    def test_check_pToGroundwaterAllOrigins(self):

        check_computed = self.model.check_pToGroundwaterAllOrigins(self.representation)

        self.assertEqual(True, check_computed)

    def test_check_pToGroundwaterAllOrigins_no_inputs(self):

        del(self.representation['inputs'])
        self.representation['dataCompleteness']['fertilizer'] = False
        check_computed = self.model.check_pToGroundwaterAllOrigins(self.representation)

        self.assertEqual(False, check_computed)

    def test_check_pToGroundwaterAllOrigins_no_site(self):

        del(self.representation['site'])
        check_computed = self.model.check_pToGroundwaterAllOrigins(self.representation)

        self.assertEqual(True, check_computed)

    def test_pToGroundwaterAllOrigins(self):

        condition = self.model.check_pToGroundwaterAllOrigins(self.representation)
        pToGroundwaterAllOrigins_computed = self.model.calculate_pToGroundwaterAllOrigins()\
            if condition else self.pToGroundwaterAllOrigins_expected

        self.assertEqual(self.pToGroundwaterAllOrigins_expected, pToGroundwaterAllOrigins_computed)


if __name__ == '__main__':
    unittest.main()
