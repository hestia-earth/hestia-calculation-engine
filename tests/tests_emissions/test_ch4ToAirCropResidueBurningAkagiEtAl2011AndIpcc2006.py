import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.emissions.ch4ToAirCropResidueBurningAkagiEtAl2011AndIpcc2006\
    import CH4ToAirCropResidueBurningAkagiEtAl2011AndIpcc2006


class Test_ch4N2ONh3AndNoxToAirCropResidueBurningAkagiEtAl2011AndIpcc2006(unittest.TestCase):
    def __init__(self, *args, **kwargs):

        super(Test_ch4N2ONh3AndNoxToAirCropResidueBurningAkagiEtAl2011AndIpcc2006, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()

        engine.import_data(cycle=cycle, site=site)

        self.model = CH4ToAirCropResidueBurningAkagiEtAl2011AndIpcc2006()

        self.representation = engine.data.safe_representation

        self.ch4ToAirCropResidueBurning_expected = 19.09251

    def test_check_ch4ToAirCropResidueBurning(self):

        check_computed = self.model.check_ch4ToAirCropResidueBurning(self.representation)

        self.assertEqual(True, check_computed)

    def test_ch4ToAirCropResidueBurning_no_products(self):

        del (self.representation['products'])
        self.representation['dataCompleteness']['cropResidue'] = False
        check_computed = self.model.check_ch4ToAirCropResidueBurning(self.representation)

        self.assertEqual(False, check_computed)

    def test_ch4ToAirCropResidueBurning(self):

        condition = self.model.check_ch4ToAirCropResidueBurning(self.representation)
        ch4ToAirCropResidueBurning_computed = \
            self.model.calculate_ch4ToAirCropResidueBurning() if condition else self.ch4ToAirCropResidueBurning_expected

        self.assertEqual(self.ch4ToAirCropResidueBurning_expected, ch4ToAirCropResidueBurning_computed)


if __name__ == '__main__':
    unittest.main()
