import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.emissions.nErosionSalcaSchererPfister2015 import NErosionSalcaSchererPfister2015


class Test_nErosionSalcaSchererPfister2015(unittest.TestCase):
    def __init__(self, *args, **kwargs):

        super(Test_nErosionSalcaSchererPfister2015, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()

        engine.import_data(cycle, site)

        self.model = NErosionSalcaSchererPfister2015()

        self.representation = engine.data.safe_representation

        self.nErosionAllOrigins_expected = 0.2892120606807134

    def test_check_nErosionAllOrigins(self):

        check_computed = self.model.check_nErosionAllOrigins(self.representation)

        self.assertEqual(True, check_computed)

    def test_nErosionAllOrigins(self):

        condition = self.model.check_nErosionAllOrigins(self.representation)
        nErosionAllOrigins_computed = self.model.calculate_nErosionAllOrigins()\
            if condition else self.nErosionAllOrigins_expected

        self.assertEqual(self.nErosionAllOrigins_expected, nErosionAllOrigins_computed)

    def test_check_nErosionAllOrigins_no_inputs(self):

        del(self.representation['inputs'])
        del(self.representation['site']['measurements']['rainfallAnnual'])
        self.representation['dataCompleteness']['water'] = False
        check_computed = self.model.check_nErosionAllOrigins(self.representation)

        self.assertEqual(False, check_computed)

    def test_check_nErosionAllOrigins_no_site(self):

        del(self.representation['site'])
        check_computed = self.model.check_nErosionAllOrigins(self.representation)

        self.assertEqual(False, check_computed)

    def test_nErosionAllOrigins_extra(self):

        nErosionAllOrigins_expected = 58.89784293375469
        self.representation['site']['measurements']['rainfallAnnual']['value'] = [10000]
        condition = self.model.check_nErosionAllOrigins(self.representation)
        nErosionAllOrigins_computed = self.model.calculate_nErosionAllOrigins()\
            if condition else nErosionAllOrigins_expected

        self.assertEqual(nErosionAllOrigins_expected, nErosionAllOrigins_computed)


if __name__ == '__main__':
    unittest.main()
