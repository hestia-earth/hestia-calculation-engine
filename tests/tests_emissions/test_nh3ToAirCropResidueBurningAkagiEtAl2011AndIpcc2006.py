import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.emissions.nh3ToAirCropResidueBurningAkagiEtAl2011AndIpcc2006\
    import NH3ToAirCropResidueBurningAkagiEtAl2011AndIpcc2006


class Test_nh3ToAirCropResidueBurningAkagiEtAl2011AndIpcc2006(unittest.TestCase):
    def __init__(self, *args, **kwargs):

        super(Test_nh3ToAirCropResidueBurningAkagiEtAl2011AndIpcc2006, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()

        engine.import_data(cycle, site)

        self.model = NH3ToAirCropResidueBurningAkagiEtAl2011AndIpcc2006()

        self.representation = engine.data.safe_representation

        self.nh3ToAirCropResidueBurning_expected = 7.118685

    def test_check(self):

        check_computed = self.model.check_nh3ToAirCropResidueBurning(self.representation)

        self.assertEqual(True, check_computed)

    def test_check_nh3ToAirCropResidueBurning_no_products(self):

        del(self.representation['products'])
        self.representation['dataCompleteness']['cropResidue'] = False
        check_computed = self.model.check_nh3ToAirCropResidueBurning(self.representation)

        self.assertEqual(False, check_computed)

    def test_nh3ToAirCropResidueBurning(self):

        condition = self.model.check_nh3ToAirCropResidueBurning(self.representation)
        nh3ToAirCropResidueBurning_computed = \
            self.model.calculate_nh3ToAirCropResidueBurning()\
            if condition else self.nh3ToAirCropResidueBurning_expected

        self.assertEqual(self.nh3ToAirCropResidueBurning_expected, nh3ToAirCropResidueBurning_computed)


if __name__ == '__main__':
    unittest.main()
