import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.emissions.ipcc2019Tier1CountryAverageFertilizerMix\
    import Ipcc2019Tier1CountryAverageFertilizerMix


class Test_ipcc2019Tier1CountryAverageFertilizerMix(unittest.TestCase):

    def __init__(self, *args, **kwargs):

        super(Test_ipcc2019Tier1CountryAverageFertilizerMix, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()

        engine.import_data(cycle=cycle, site=site)

        self.model = Ipcc2019Tier1CountryAverageFertilizerMix()

        self.representation = engine.data.safe_representation

        self.co2ToAirUreaHydrolysis_expected = 0.0

    def test_check_co2ToAirUreaHydrolysis(self):

        check_computed = self.model.check_co2ToAirUreaHydrolysis(self.representation)

        self.assertEqual(True, check_computed)

    def test_calculate_co2ToAirUreaHydrolysis(self):

        condition = self.model.check_co2ToAirUreaHydrolysis(self.representation)
        co2ToAirUreaHydrolysis_computed = self.model.calculate_co2ToAirUreaHydrolysis()\
            if condition else self.co2ToAirUreaHydrolysis_expected

        self.assertEqual(self.co2ToAirUreaHydrolysis_expected, co2ToAirUreaHydrolysis_computed)

    def test_check_co2ToAirUreaHydrolysis_no_inputs(self):

        del(self.representation['inputs'])
        self.representation['dataCompleteness']['fertilizer'] = False
        check_computed = self.model.check_co2ToAirUreaHydrolysis(self.representation)

        self.assertEqual(False, check_computed)


if __name__ == '__main__':
    unittest.main()
