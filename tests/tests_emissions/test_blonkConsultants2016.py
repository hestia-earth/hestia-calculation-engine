import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.utils import EngineDict
from hestia_earth.calculation.emissions.blonkConsultants2016 import BlonkConsultants2016


class Test_BlonkConsultants2016(unittest.TestCase):
    def __init__(self, *args, **kwargs):

        super(Test_BlonkConsultants2016, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()

        engine.import_data(cycle=cycle, site=site)

        self.model = BlonkConsultants2016()

        self.representation = engine.data.safe_representation

        self.co2ToAirOrganicSoilBurning_expected = 0.0
        self.co2ToAirSoilCarbonStockChange_expected = 0.0

    def test_check_co2ToAirSoilCarbonStockChange(self):

        self.representation['site']['measurements']['landOccupation'] =\
            EngineDict({'value': [3.319494943]})
        self.representation['site']['measurements']['histosol'] = EngineDict({'value': [True]})
        check_computed = self.model.check_co2ToAirSoilCarbonStockChange(self.representation)

        self.assertEqual(True, check_computed)

    def test_check_co2ToAirOrganicSoilBurning(self):

        self.representation['site']['measurements']['landOccupation'] =\
            EngineDict({'value': [3.319494943]})
        check_computed = self.model.check_co2ToAirOrganicSoilBurning(self.representation)

        self.assertEqual(True, check_computed)

    def test_co2ToAirSoilCarbonStockChange(self):

        self.representation['site']['measurements']['landOccupation'] =\
            EngineDict({'value': [3.319494943]})
        condition = self.model.check_co2ToAirSoilCarbonStockChange(self.representation)
        co2ToAirSoilCarbonStockChange_computed = \
            self.model.calculate_co2ToAirSoilCarbonStockChange() if condition else\
            self.co2ToAirSoilCarbonStockChange_expected

        self.assertEqual(self.co2ToAirSoilCarbonStockChange_expected, co2ToAirSoilCarbonStockChange_computed)

    def test_co2ToAirOrganicSoilBurning(self):

        self.representation['site']['measurements']['landOccupation'] =\
            EngineDict({'value': [3.319494943]})
        condition = self.model.check_co2ToAirOrganicSoilBurning(self.representation)
        co2ToAirOrganicSoilBurning_computed = \
            self.model.calculate_co2ToAirOrganicSoilBurning() if condition else\
            self.co2ToAirOrganicSoilBurning_expected

        self.assertEqual(self.co2ToAirOrganicSoilBurning_expected, co2ToAirOrganicSoilBurning_computed)

    def test_check_co2ToAirSoilCarbonStockChange_no_inputs(self):
        del (self.representation['site'])
        check_computed = self.model.check_co2ToAirSoilCarbonStockChange(self.representation)

        self.assertEqual(False, check_computed)

    def test_check_co2ToAirOrganicSoilBurning_no_inputs(self):
        del (self.representation['site'])
        check_computed = self.model.check_co2ToAirOrganicSoilBurning(self.representation)

        self.assertEqual(False, check_computed)


if __name__ == '__main__':
    unittest.main()
