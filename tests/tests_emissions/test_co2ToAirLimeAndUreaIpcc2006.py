import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.emissions.co2ToAirLimeAndUreaIpcc2006\
    import CO2ToAirLimeAndUreaIpcc2006


class Test_co2ToAirLimeAndUreaIpcc2006(unittest.TestCase):

    def __init__(self, *args, **kwargs):

        super(Test_co2ToAirLimeAndUreaIpcc2006, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()

        engine.import_data(cycle=cycle, site=site)

        self.model = CO2ToAirLimeAndUreaIpcc2006()

        self.representation = engine.data.safe_representation

        self.co2ToAirLimeHydrolysis_expected = 0.0
        self.co2ToAirUreaHydrolysis_expected = 109.97001499250376

    def test_check_co2ToAirLimeHydrolysis(self):

        check_computed = self.model.check_co2ToAirLimeHydrolysis(self.representation)

        self.assertEqual(True, check_computed)

    def test_check_co2ToAirUreaHydrolysis(self):

        check_computed = self.model.check_co2ToAirUreaHydrolysis(self.representation)

        self.assertEqual(True, check_computed)

    def test_calculate_co2ToAirLimeHydrolysis(self):

        condition = self.model.check_co2ToAirLimeHydrolysis(self.representation)
        co2ToAirLimeHydrolysis_computed = self.model.calculate_co2ToAirLimeHydrolysis()\
            if condition else self.co2ToAirLimeHydrolysis_expected

        self.assertEqual(self.co2ToAirLimeHydrolysis_expected, co2ToAirLimeHydrolysis_computed)

    def test_calculate_co2ToAirUreaHydrolysis(self):

        condition = self.model.check_co2ToAirUreaHydrolysis(self.representation)
        co2ToAirUreaHydrolysis_computed = self.model.calculate_co2ToAirUreaHydrolysis()\
            if condition else self.co2ToAirUreaHydrolysis_expected

        self.assertEqual(self.co2ToAirUreaHydrolysis_expected, co2ToAirUreaHydrolysis_computed)

    def test_check_co2ToAirLimeHydrolysis_no_inputs(self):

        del(self.representation['inputs'])
        self.representation['dataCompleteness']['soilAmendments'] = False
        check_computed = self.model.check_co2ToAirLimeHydrolysis(self.representation)

        self.assertEqual(False, check_computed)

    def test_check_co2ToAirUreaHydrolysis_no_inputs(self):

        del(self.representation['inputs'])
        self.representation['dataCompleteness']['fertilizer'] = False
        check_computed = self.model.check_co2ToAirUreaHydrolysis(self.representation)

        self.assertEqual(False, check_computed)

    def test_calculate_co2ToAirLimeHydrolysis_non_zero(self):

        co2ToAirLimeHydrolysis_non_zero_expected = 43.96603396603397
        self.representation['inputs']['lime'] = {'term': {'@id': 'lime', 'termType': 'soilAmendment'}, 'value': [100]}
        condition = self.model.check_co2ToAirLimeHydrolysis(self.representation)
        co2ToAirLimeHydrolysis_computed = self.model.calculate_co2ToAirLimeHydrolysis()\
            if condition else self.co2ToAirLimeHydrolysis_expected

        self.assertEqual(co2ToAirLimeHydrolysis_non_zero_expected, co2ToAirLimeHydrolysis_computed)


if __name__ == '__main__':
    unittest.main()
