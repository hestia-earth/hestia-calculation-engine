import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.emissions.co2ToAirFuelCombustionEMEAEEA2019 import CO2ToAirFuelCombustionEMEAEEA2019


class Test_CO2ToAirFuelCombustionEMEAEEA2019(unittest.TestCase):
    def __init__(self, *args, **kwargs):

        super(Test_CO2ToAirFuelCombustionEMEAEEA2019, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()

        engine.import_data(cycle=cycle, site=site)

        self.model = CO2ToAirFuelCombustionEMEAEEA2019()

        self.representation = engine.data.safe_representation

        self.co2ToAirFuelCombustion_expected = 57.52916

    def test_check_co2ToAirFuelCombustion(self):

        check_computed = self.model.check_co2ToAirFuelCombustion(self.representation)

        self.assertEqual(True, check_computed)

    def test_co2ToAirFuelCombustion(self):

        condition = self.model.check_co2ToAirFuelCombustion(self.representation)
        co2ToAirFuelCombustion_computed = \
            self.model.calculate_co2ToAirFuelCombustion() if condition else self.co2ToAirFuelCombustion_expected

        self.assertEqual(self.co2ToAirFuelCombustion_expected, co2ToAirFuelCombustion_computed)

    def test_check_co2ToAirFuelCombustion_no_inputs(self):

        del(self.representation['inputs'])
        self.representation['dataCompleteness']['electricityFuel'] = False
        check_computed = self.model.check_co2ToAirFuelCombustion(self.representation)

        self.assertEqual(False, check_computed)


if __name__ == '__main__':
    unittest.main()
