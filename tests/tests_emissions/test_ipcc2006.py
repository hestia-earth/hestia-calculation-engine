import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.utils import EngineDict
from hestia_earth.calculation.emissions.ipcc2006 import IPCC2006


class Test_IPCC2006(unittest.TestCase):
    def __init__(self, *args, **kwargs):

        super(Test_IPCC2006, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()

        engine.import_data(cycle=cycle, site=site)

        self.model = IPCC2006()

        self.representation = engine.data.safe_representation

        self.co2ToAirOrganicSoilCultivation_expected = 24.34296291533333
        self.n2OToAirOrganicSoilCultivationDirect_expected = 0.0106223838176

    def test_check_co2ToAirOrganicSoilCultivation(self):

        self.representation['site']['measurements']['landTransformation20YearAverage'] =\
            EngineDict({'value': [3.319494943]})
        self.representation['site']['measurements']['histosol'] = EngineDict({'value': [2]})
        check_computed = self.model.check_co2ToAirOrganicSoilCultivation(self.representation)

        self.assertEqual(True, check_computed)

    def test_co2ToAirOrganicSoilCultivation(self):

        self.representation['site']['measurements']['landTransformation20YearAverage'] =\
            EngineDict({'value': [3.319494943]})
        self.representation['site']['measurements']['histosol'] = EngineDict({'value': [2]})
        condition = self.model.check_co2ToAirOrganicSoilCultivation(self.representation)
        co2ToAirOrganicSoilCultivation_computed = \
            self.model.calculate_co2ToAirOrganicSoilCultivation() if condition else\
            self.co2ToAirOrganicSoilCultivation_expected

        self.assertEqual(self.co2ToAirOrganicSoilCultivation_expected, co2ToAirOrganicSoilCultivation_computed)

    def test_check_co2ToAirOrganicSoilCultivation_no_inputs(self):

        del(self.representation['site'])
        check_computed = self.model.check_co2ToAirOrganicSoilCultivation(self.representation)

        self.assertEqual(False, check_computed)

    def test_check_n2OToAirOrganicSoilCultivationDirect(self):
        self.representation['site']['measurements']['landTransformation20YearAverage'] = \
            EngineDict({'value': [3.319494943]})
        self.representation['site']['measurements']['histosol'] = EngineDict({'value': [2]})
        check_computed = self.model.check_n2OToAirOrganicSoilCultivationDirect(self.representation)

        self.assertEqual(True, check_computed)

    def test_n2OToAirOrganicSoilCultivationDirect(self):
        self.representation['site']['measurements']['landTransformation20YearAverage'] = \
            EngineDict({'value': [3.319494943]})
        self.representation['site']['measurements']['histosol'] = EngineDict({'value': [2]})
        condition = self.model.check_n2OToAirOrganicSoilCultivationDirect(self.representation)
        n2OToAirOrganicSoilCultivationDirect_computed = \
            self.model.calculate_n2OToAirOrganicSoilCultivationDirect() if condition else \
            self.n2OToAirOrganicSoilCultivationDirect_expected

        self.assertEqual(self.n2OToAirOrganicSoilCultivationDirect_expected,
                         n2OToAirOrganicSoilCultivationDirect_computed)

    def test_check_n2OToAirOrganicSoilCultivationDirect_no_inputs(self):
        del (self.representation['site'])
        check_computed = self.model.check_n2OToAirOrganicSoilCultivationDirect(self.representation)

        self.assertEqual(False, check_computed)


if __name__ == '__main__':
    unittest.main()
