import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.emissions.pToSurfacewaterSalcaPrasuhn2006 import PToSurfacewaterSalcaPrasuhn2006


class Test_pToSurfacewaterSalcaPrasuhn2006(unittest.TestCase):
    def __init__(self, *args, **kwargs):

        super(Test_pToSurfacewaterSalcaPrasuhn2006, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()

        engine.import_data(cycle, site)

        self.model = PToSurfacewaterSalcaPrasuhn2006()

        self.representation = engine.data.safe_representation

        self.pToSurfacewaterAllOrigins_expected = 1.0

    def test_check_pToSurfacewaterAllOrigins(self):

        check_computed = self.model.check_pToSurfacewaterAllOrigins(self.representation)

        self.assertEqual(True, check_computed)

    def test_check_pToSurfacewaterAllOrigins_no_inputs(self):

        del(self.representation['inputs'])
        self.representation['dataCompleteness']['fertilizer'] = False
        check_computed = self.model.check_pToSurfacewaterAllOrigins(self.representation)

        self.assertEqual(False, check_computed)

    def test_check_pToSurfacewaterAllOrigins_no_products(self):

        del(self.representation['products'])
        self.representation['dataCompleteness']['products'] = False
        check_computed = self.model.check_pToSurfacewaterAllOrigins(self.representation)

        self.assertEqual(False, check_computed)

    def test_check_pToSurfacewaterAllOrigins_no_site(self):

        del(self.representation['site'])
        check_computed = self.model.check_pToSurfacewaterAllOrigins(self.representation)

        self.assertEqual(False, check_computed)

    def test_pToSurfacewaterAllOrigins(self):

        condition = self.model.check_pToSurfacewaterAllOrigins(self.representation)
        pToSurfacewaterAllOrigins_computed = self.model.calculate_pToSurfacewaterAllOrigins()\
            if condition else self.pToSurfacewaterAllOrigins_expected

        self.assertEqual(self.pToSurfacewaterAllOrigins_expected, pToSurfacewaterAllOrigins_computed)


if __name__ == '__main__':
    unittest.main()
