import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.emissions.n2OToAirCropResidueBurningAkagiEtAl2011AndIpcc2006\
    import N2OToAirCropResidueBurningAkagiEtAl2011AndIpcc2006


class Test_n2OToAirCropResidueBurningAkagiEtAl2011AndIpcc2006(unittest.TestCase):
    def __init__(self, *args, **kwargs):

        super(Test_n2OToAirCropResidueBurningAkagiEtAl2011AndIpcc2006, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()

        engine.import_data(cycle=cycle, site=site)

        self.model = N2OToAirCropResidueBurningAkagiEtAl2011AndIpcc2006()

        self.representation = engine.data.safe_representation

        self.n2OToAirCropResidueBurningDirect_expected = 0.22963500000000003

    def test_check(self):

        check_computed = self.model.check_n2OToAirCropResidueBurningDirect(self.representation)

        self.assertEqual(True, check_computed)

    def test_check_n2OToAirCropResidueBurningDirect_no_products(self):

        del(self.representation['products'])
        self.representation['dataCompleteness']['cropResidue'] = False
        check_computed = self.model.check_n2OToAirCropResidueBurningDirect(self.representation)

        self.assertEqual(False, check_computed)

    def test_n2OToAirCropResidueBurningDirect(self):

        condition = self.model.check_n2OToAirCropResidueBurningDirect(self.representation)
        n2OToAirCropResidueBurningDirect_computed = self.model.calculate_n2OToAirCropResidueBurningDirect()\
            if condition else self.n2OToAirCropResidueBurningDirect_expected

        self.assertEqual(self.n2OToAirCropResidueBurningDirect_expected, n2OToAirCropResidueBurningDirect_computed)


if __name__ == '__main__':
    unittest.main()
