import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.emissions.pToDrainageWaterSalcaPrasuhn2006 import PToDrainageWaterSalcaPrasuhn2006


class Test_pToDrainageWaterSalcaPrasuhn2006(unittest.TestCase):
    def __init__(self, *args, **kwargs):

        super(Test_pToDrainageWaterSalcaPrasuhn2006, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()

        engine.import_data(cycle=cycle, site=site)

        engine.data.safe_representation['emissions']['pToSurfacewaterAllOrigins'] = {
            'term': {
                'name': "pToSurfacewaterAllOrigins",
                '@id': "pToSurfacewaterAllOrigins"
            },
            'value': [0.0]
        }

        self.model = PToDrainageWaterSalcaPrasuhn2006()

        self.representation = engine.data.safe_representation

        self.pToDrainageWaterAllOrigins_expected = 0.42000000000000004

    def test_check_pToDrainageWaterAllOrigins(self):

        check_computed = self.model.check_pToDrainageWaterAllOrigins(self.representation)

        self.assertEqual(True, check_computed)

    def test_check_pToDrainageWaterAllOrigins_no_inputs(self):

        del(self.representation['inputs'])
        self.representation['dataCompleteness']['fertilizer'] = False
        check_computed = self.model.check_pToDrainageWaterAllOrigins(self.representation)

        self.assertEqual(False, check_computed)

    def test_check_pToDrainageWaterAllOrigins_no_site(self):

        del(self.representation['site'])
        check_computed = self.model.check_pToDrainageWaterAllOrigins(self.representation)

        self.assertEqual(False, check_computed)

    def test_check_pToDrainageWaterAllOrigins_no_emissions(self):

        del(self.representation['emissions'])
        check_computed = self.model.check_pToDrainageWaterAllOrigins(self.representation)

        self.assertEqual(False, check_computed)

    def test_pToDrainageWaterAllOrigins(self):

        condition = self.model.check_pToDrainageWaterAllOrigins(self.representation)
        pToDrainageWaterAllOrigins_computed = self.model.calculate_pToDrainageWaterAllOrigins()\
            if condition else self.pToDrainageWaterAllOrigins_expected

        self.assertEqual(self.pToDrainageWaterAllOrigins_expected, pToDrainageWaterAllOrigins_computed)


if __name__ == '__main__':
    unittest.main()
