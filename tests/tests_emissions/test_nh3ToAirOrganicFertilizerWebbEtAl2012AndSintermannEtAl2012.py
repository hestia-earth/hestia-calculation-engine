import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.emissions.nh3ToAirOrganicFertilizerWebbEtAl2012AndSintermannEtAl2012\
    import NH3ToAirOrganicFertilizerWebbEtAl2012AndSintermannEtAl2012


class Test_nh3ToAirOrganicFertilizerWebbEtAl2012AndSintermannEtAl2012(unittest.TestCase):
    def __init__(self, *args, **kwargs):

        super(Test_nh3ToAirOrganicFertilizerWebbEtAl2012AndSintermannEtAl2012, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()

        engine.import_data(cycle, site)

        engine.data.safe_representation['inputs']['slurryAndSludgeAsN'] =\
            {'term': {'@id': 'slurryAndSludgeAsN',
                      'units': 'kg N',
                      'termType': 'organicFertilizer'},
             'value': [0]}

        engine.data.safe_representation['inputs']['compostAsN'] = {'term': {'@id': 'compostAsN',
                                                                            'units': 'kg N',
                                                                            'termType': 'organicFertilizer'},
                                                                   'value': [0]}

        engine.data.safe_representation['inputs']['greenManureAsN'] = \
            {'term': {'@id': 'greenManureAsN',
                      'units': 'kg N',
                      'termType': 'organicFertilizer'},
             'value': [0]}

        self.model = NH3ToAirOrganicFertilizerWebbEtAl2012AndSintermannEtAl2012()

        self.representation = engine.data.safe_representation

        self.nh3ToAirOrganicFertilizer_expected = 0.0

    def test_check_nh3ToAirOrganicFertilizer(self):

        check_computed = self.model.check_nh3ToAirOrganicFertilizer(self.representation)

        self.assertEqual(True, check_computed)

    def test_nh3ToAirOrganicFertilizer(self):

        condition = self.model.check_nh3ToAirOrganicFertilizer(self.representation)
        nh3ToAirOrganicFertilizer_computed = \
            self.model.calculate_nh3ToAirOrganicFertilizer()\
            if condition else self.nh3ToAirOrganicFertilizer_expected

        self.assertEqual(self.nh3ToAirOrganicFertilizer_expected, nh3ToAirOrganicFertilizer_computed)

    def test_check_nh3ToAirOrganicFertilizer_no_inputs(self):

        del (self.representation['inputs'])
        self.representation['dataCompleteness']['fertilizer'] = False
        check_computed = self.model.check_nh3ToAirOrganicFertilizer(self.representation)

        self.assertEqual(False, check_computed)


if __name__ == '__main__':
    unittest.main()
