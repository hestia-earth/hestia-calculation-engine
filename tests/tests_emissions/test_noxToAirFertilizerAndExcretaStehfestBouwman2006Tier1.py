import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.emissions.noxToAirFertilizerAndExcretaStehfestBouwman2006Tier1\
    import NOXToAirFertilizerAndExcretaStehfestBouwman2006Tier1


class Test_noxToAirFertilizerAndExcretaStehfestBouwman2006Tier1(unittest.TestCase):
    def __init__(self, *args, **kwargs):

        super(Test_noxToAirFertilizerAndExcretaStehfestBouwman2006Tier1, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()

        engine.import_data(cycle, site)

        self.model = NOXToAirFertilizerAndExcretaStehfestBouwman2006Tier1()

        self.representation = engine.data.safe_representation

        self.noxToAirAllOrigins_expected = 0.827190993748
        self.noxToAirInorganicFertilizer_expected = 0.7981002399999999
        self.noxToAirOrganicFertilizer_expected = 0.0
        self.noxToAirExcreta_expected = 0.0
        self.noxToAirCropResidueDecomposition_expected = 0.029090753747999992

    def test_check_noxToAirAllOrigins(self):

        check_computed = self.model.check_noxToAirAllOrigins(self.representation)

        self.assertEqual(True, check_computed)

    def test_check_noxToAirInorganicFertilizer(self):

        check_computed = self.model.check_noxToAirInorganicFertilizer(self.representation)

        self.assertEqual(True, check_computed)

    def test_check_noxToAirOrganicFertilizer(self):

        self.representation['dataCompleteness']['fertilizer'] = False
        check_computed = self.model.check_noxToAirOrganicFertilizer(self.representation)

        self.assertEqual(False, check_computed)

    def test_check_noxToAirExcreta(self):

        self.representation['dataCompleteness']['products'] = False
        check_computed = self.model.check_noxToAirExcreta(self.representation)

        self.assertEqual(False, check_computed)

    def test_check_noxToAirCropResidueDecomposition(self):

        check_computed = self.model.check_noxToAirCropResidueDecomposition(self.representation)

        self.assertEqual(True, check_computed)

    def test_check_noxToAirAllOrigins_no_inputs(self):

        del(self.representation['inputs'])
        self.representation['dataCompleteness']['fertilizer'] = False
        self.representation['dataCompleteness']['cropResidue'] = False
        check_computed = self.model.check_noxToAirAllOrigins(self.representation)

        self.assertEqual(True, check_computed)

    def test_check_noxToAirAllOrigins_no_site(self):

        del(self.representation['site'])
        check_computed = self.model.check_noxToAirAllOrigins(self.representation)

        self.assertEqual(False, check_computed)

    def test_noxToAirAllOrigins(self):

        condition = self.model.check_noxToAirAllOrigins(self.representation)
        noxToAirAllOrigins_computed = \
            self.model.calculate_noxToAirAllOrigins() if condition else self.noxToAirAllOrigins_expected

        self.assertEqual(self.noxToAirAllOrigins_expected, noxToAirAllOrigins_computed)

    def test_noxToAirInorganicFertilizer(self):

        condition = self.model.check_noxToAirInorganicFertilizer(self.representation)
        noxToAirInorganicFertilizer_computed = self.model.calculate_noxToAirInorganicFertilizer() \
            if condition else self.noxToAirInorganicFertilizer_expected

        self.assertEqual(self.noxToAirInorganicFertilizer_expected, noxToAirInorganicFertilizer_computed)

    def test_noxToAirOrganicFertilizer(self):

        condition = self.model.check_noxToAirOrganicFertilizer(self.representation)
        noxToAirOrganicFertilizer_computed = self.model.calculate_noxToAirOrganicFertilizer()\
            if condition else self.noxToAirOrganicFertilizer_expected

        self.assertEqual(self.noxToAirOrganicFertilizer_expected, noxToAirOrganicFertilizer_computed)

    def test_noxToAirExcreta(self):

        condition = self.model.check_noxToAirExcreta(self.representation)
        noxToAirExcreta_computed = self.model.calculate_noxToAirExcreta()\
            if condition else self.noxToAirExcreta_expected

        self.assertEqual(self.noxToAirExcreta_expected, noxToAirExcreta_computed)

    def test_noxToAirCropResidueDecomposition(self):

        condition = self.model.check_noxToAirCropResidueDecomposition(self.representation)
        noxToAirCropResidueDecomposition_computed = self.model.calculate_noxToAirCropResidueDecomposition() \
            if condition else self.noxToAirCropResidueDecomposition_expected

        self.assertEqual(self.noxToAirCropResidueDecomposition_expected, noxToAirCropResidueDecomposition_computed)


if __name__ == '__main__':
    unittest.main()
