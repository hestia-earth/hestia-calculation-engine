import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.emissions.n2OToAirFertilizerAndExcretaIndirectIpcc2006 import\
    N2OToAirFertilizerAndExcretaIndirectIpcc2006


class Test_n2OToAirFertilizerAndExcretaIndirectIpcc2006(unittest.TestCase):
    def __init__(self, *args, **kwargs):

        super(Test_n2OToAirFertilizerAndExcretaIndirectIpcc2006, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()

        engine.import_data(cycle, site)

        engine.data.safe_representation['emissions']['nh3ToAirInorganicFertilizer'] = {
            'term': {
                'name': "nh3ToAirInorganicFertilizer",
                '@id': "nh3ToAirInorganicFertilizer"
            },
            'value': [11.13]
        }

        engine.data.safe_representation['emissions']['noxToAirInorganicFertilizer'] = {
            'term': {
                'name': "noxToAirInorganicFertilizer",
                '@id': "noxToAirInorganicFertilizer"
            },
            'value': [0.4435692463763601]

        }

        engine.data.safe_representation['emissions']['no3ToGroundwaterInorganicFertilizer'] = {
            'term': {
                'name': "no3ToGroundwaterInorganicFertilizer",
                '@id': "no3ToGroundwaterInorganicFertilizer"
            },
            'value': [20.760959520239883]
        }

        engine.data.safe_representation['emissions']['nh3ToAirOrganicFertilizer'] = {
            'term': {
                'name': "nh3ToAirOrganicFertilizer",
                '@id': "nh3ToAirOrganicFertilizer"
            },
            'value': [0.0]
        }

        engine.data.safe_representation['emissions']['noxToAirOrganicFertilizer'] = {
            'term': {
                'name': "noxToAirOrganicFertilizer",
                '@id': "noxToAirOrganicFertilizer"
            },
            'value': [0.0]

        }

        engine.data.safe_representation['emissions']['no3ToGroundwaterOrganicFertilizer'] = {
            'term': {
                'name': "no3ToGroundwaterOrganicFertilizer",
                '@id': "no3ToGroundwaterOrganicFertilizer"
            },
            'value': [0]
        }

        engine.data.safe_representation['emissions']['nh3ToAirExcreta'] = {
            'term': {
                'name': "nh3ToAirExcreta",
                '@id': "nh3ToAirExcreta"
            },
            'value': [0.0]
        }

        engine.data.safe_representation['emissions']['noxToAirExcreta'] = {
            'term': {
                'name': "noxToAirExcreta",
                '@id': "noxToAirExcreta"
            },
            'value': [0.0]

        }

        engine.data.safe_representation['emissions']['no3ToGroundwaterExcreta'] = {
            'term': {
                'name': "no3ToGroundwaterExcreta",
                '@id': "no3ToGroundwaterExcreta"
            },
            'value': [0.0]
        }

        engine.data.safe_representation['emissions']['nh3ToAirCropResidueDecomposition'] = {
            'term': {
                'name': "nh3ToAirCropResidueDecomposition",
                '@id': "nh3ToAirCropResidueDecomposition"
            },
            'value': [0.0]
        }

        engine.data.safe_representation['emissions']['noxToAirCropResidueDecomposition'] = {
            'term': {
                'name': "noxToAirCropResidueDecomposition",
                '@id': "noxToAirCropResidueDecomposition"
            },
            'value': [0.0]

        }

        engine.data.safe_representation['emissions']['no3ToGroundwaterCropResidueDecomposition'] = {
            'term': {
                'name': "no3ToGroundwaterCropResidueDecomposition",
                '@id': "no3ToGroundwaterCropResidueDecomposition"
            },
            'value': [0.7585535584707646]
        }

        self.model = N2OToAirFertilizerAndExcretaIndirectIpcc2006()

        self.representation = engine.data.safe_representation

        self.n2OToAirInorganicFertilizerIndirect_expected = 0.20233244900390537
        self.n2OToAirOrganicFertilizerIndirect_expected = 0
        self.n2OToAirExcretaIndirect_expected = 0
        self.n2OToAirCropResidueDecompositionIndirect_expected = 0.0020191974168384556

    def test_check_n2OToAirInorganicFertilizerIndirect(self):

        check_computed = self.model.check_n2OToAirInorganicFertilizerIndirect(self.representation)

        self.assertEqual(True, check_computed)

    def test_check_n2OToAirOrganicFertilizerIndirect(self):

        check_computed = self.model.check_n2OToAirOrganicFertilizerIndirect(self.representation)

        self.assertEqual(True, check_computed)

    def test_check_n2OToAirExcretaIndirect(self):

        check_computed = self.model.check_n2OToAirExcretaIndirect(self.representation)

        self.assertEqual(True, check_computed)

    def test_check_n2OToAirCropResidueDecompositionIndirect(self):

        check_computed = self.model.check_n2OToAirCropResidueDecompositionIndirect(self.representation)

        self.assertEqual(True, check_computed)

    def test_n2OToAirInorganicFertilizerIndirect(self):

        condition = self.model.check_n2OToAirInorganicFertilizerIndirect(self.representation)
        n2OToAirInorganicFertilizerIndirect_computed = self.model.calculate_n2OToAirInorganicFertilizerIndirect()\
            if condition else self.n2OToAirInorganicFertilizerIndirect_expected

        self.assertEqual(self.n2OToAirInorganicFertilizerIndirect_expected,
                         n2OToAirInorganicFertilizerIndirect_computed)

    def test_n2OToAirOrganicFertilizerIndirect(self):

        condition = self.model.check_n2OToAirOrganicFertilizerIndirect(self.representation)
        n2OToAirOrganicFertilizerIndirect_computed = self.model.calculate_n2OToAirOrganicFertilizerIndirect()\
            if condition else self.n2OToAirOrganicFertilizerIndirect_expected

        self.assertEqual(self.n2OToAirOrganicFertilizerIndirect_expected, n2OToAirOrganicFertilizerIndirect_computed)

    def test_n2OToAirExcretaIndirect(self):

        condition = self.model.check_n2OToAirExcretaIndirect(self.representation)
        n2OToAirExcretaIndirect_computed = self.model.calculate_n2OToAirExcretaIndirect()\
            if condition else self.n2OToAirExcretaIndirect_expected

        self.assertEqual(self.n2OToAirExcretaIndirect_expected, n2OToAirExcretaIndirect_computed)

    def test_n2OToAirCropResidueDecompositionIndirect(self):

        condition = self.model.check_n2OToAirCropResidueDecompositionIndirect(self.representation)
        n2OToAirCropResidueDecompositionIndirect_computed =\
            self.model.calculate_n2OToAirCropResidueDecompositionIndirect()\
            if condition else self.n2OToAirCropResidueDecompositionIndirect_expected

        self.assertEqual(self.n2OToAirCropResidueDecompositionIndirect_expected,
                         n2OToAirCropResidueDecompositionIndirect_computed)

    def test_check_n2OToAirInorganicFertilizerIndirect_neither_inputs_nor_emissions(self):

        del(self.representation['inputs'])
        del(self.representation['emissions'])
        check_computed = self.model.check_n2OToAirInorganicFertilizerIndirect(self.representation)

        self.assertEqual(False, check_computed)

    def test_check_n2OToAirOrganicFertilizerIndirect_neither_inputs_nor_emissions(self):

        del(self.representation['inputs'])
        del(self.representation['emissions'])
        check_computed = self.model.check_n2OToAirOrganicFertilizerIndirect(self.representation)

        self.assertEqual(False, check_computed)

    def test_check_n2OToAirExcretaIndirect_neither_products_nor_emissions(self):

        del(self.representation['products'])
        del(self.representation['emissions'])
        check_computed = self.model.check_n2OToAirExcretaIndirect(self.representation)

        self.assertEqual(False, check_computed)

    def test_check_n2OToAirCropResidueDecompositionIndirect_no_emissions(self):

        del(self.representation['emissions'])
        check_computed = self.model.check_n2OToAirCropResidueDecompositionIndirect(self.representation)

        self.assertEqual(False, check_computed)

    def test_n2OToAirOrganicFertilizerIndirect_extra(self):

        n2OToAirOrganicFertilizerIndirect_expected = 0.026619048771047028
        self.representation['inputs']['compostAsN'] = {'term': {'@id': 'compostAsN', 'units': 'kg N',
                                                                'termType': 'organicFertilizer'}, 'value': [60]}
        self.representation['emissions']['no3ToGroundwaterOrganicFertilizer'] = {
            'term': {
                'name': "no3ToGroundwaterOrganicFertilizer",
                '@id': "no3ToGroundwaterOrganicFertilizer"
            },
            'value': [10]
        }
        condition = self.model.check_n2OToAirOrganicFertilizerIndirect(self.representation)
        n2OToAirOrganicFertilizerIndirect_computed = self.model.calculate_n2OToAirOrganicFertilizerIndirect()\
            if condition else n2OToAirOrganicFertilizerIndirect_expected

        self.assertEqual(n2OToAirOrganicFertilizerIndirect_expected, n2OToAirOrganicFertilizerIndirect_computed)

    def test_n2OToAirExcretaIndirect_extra(self):

        n2OToAirExcretaIndirect_expected = 0.026619048771047028
        self.representation['products']['cattle'] = {'term': {'@id': 'cattle', 'units': 'kg N',
                                                              'termType': 'animalProduct'}, 'value': [60]}
        self.representation['emissions']['no3ToGroundwaterExcreta'] = {
            'term': {
                'name': "no3ToGroundwaterExcreta",
                '@id': "no3ToGroundwaterExcreta"
            },
            'value': [10]
        }
        condition = self.model.check_n2OToAirExcretaIndirect(self.representation)
        n2OToAirExcretaIndirect_computed = self.model.calculate_n2OToAirExcretaIndirect() \
            if condition else self.n2OToAirExcretaIndirect_expected

        self.assertEqual(n2OToAirExcretaIndirect_expected, n2OToAirExcretaIndirect_computed)


if __name__ == '__main__':
    unittest.main()
