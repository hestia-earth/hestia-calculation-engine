import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.emissions.pErosionSalcaSchererPfister2015 import PErosionSalcaSchererPfister2015


class Test_pErosionSalcaSchererPfister2015(unittest.TestCase):
    def __init__(self, *args, **kwargs):

        super(Test_pErosionSalcaSchererPfister2015, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()

        engine.import_data(cycle=cycle, site=site)

        self.model = PErosionSalcaSchererPfister2015()

        self.representation = engine.data.safe_representation

        self.pErosionAllOrigins_expected = 0.1596603220714433

    def test_check_pErosionAllOrigins(self):

        check_computed = self.model.check_pErosionAllOrigins(self.representation)

        self.assertEqual(True, check_computed)

    def test_pErosionAllOrigins(self):

        condition = self.model.check_pErosionAllOrigins(self.representation)
        pErosionAllOrigins_computed = self.model.calculate_pErosionAllOrigins()\
            if condition else self.pErosionAllOrigins_expected

        self.assertEqual(self.pErosionAllOrigins_expected, pErosionAllOrigins_computed)

    def test_check_pErosionAllOrigins_no_inputs(self):

        del(self.representation['inputs'])
        del(self.representation['site']['measurements']['rainfallAnnual'])
        self.representation['dataCompleteness']['water'] = False
        check_computed = self.model.check_pErosionAllOrigins(self.representation)

        self.assertEqual(False, check_computed)

    def test_check_pErosionAllOrigins_no_site(self):

        del(self.representation['site'])
        check_computed = self.model.check_pErosionAllOrigins(self.representation)

        self.assertEqual(False, check_computed)

    def test_pErosionAllOrigins_extra(self):

        pErosionAllOrigins_expected = 32.51471792007342
        self.representation['site']['measurements']['rainfallAnnual']['value'] = [10000]
        condition = self.model.check_pErosionAllOrigins(self.representation)
        pErosionAllOrigins_computed = self.model.calculate_pErosionAllOrigins()\
            if condition else pErosionAllOrigins_expected

        self.assertEqual(pErosionAllOrigins_expected, pErosionAllOrigins_computed)


if __name__ == '__main__':
    unittest.main()
