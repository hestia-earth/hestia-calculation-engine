import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.emissions.noxToAirFuelCombustionEMEAEEA2019 import NOXToAirFuelCombustionEMEAEEA2019


class Test_NOXToAirFuelCombustionEMEAEEA2019(unittest.TestCase):
    def __init__(self, *args, **kwargs):

        super(Test_NOXToAirFuelCombustionEMEAEEA2019, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()

        engine.import_data(cycle, site)

        self.model = NOXToAirFuelCombustionEMEAEEA2019()

        self.representation = engine.data.safe_representation

        self.noxToAirFuelCombustion_expected = 0.5921510919999999

    def test_check_noxToAirFuelCombustion(self):

        check_computed = self.model.check_noxToAirFuelCombustion(self.representation)

        self.assertEqual(True, check_computed)

    def test_noxToAirFuelCombustion(self):

        condition = self.model.check_noxToAirFuelCombustion(self.representation)
        noxToAirFuelCombustion_computed = \
            self.model.calculate_noxToAirFuelCombustion() if condition else self.noxToAirFuelCombustion_expected

        self.assertEqual(self.noxToAirFuelCombustion_expected, noxToAirFuelCombustion_computed)

    def test_check_noxToAirFuelCombustion_no_inputs(self):

        del(self.representation['inputs'])
        self.representation['dataCompleteness']['electricityFuel'] = False
        check_computed = self.model.check_noxToAirFuelCombustion(self.representation)

        self.assertEqual(False, check_computed)


if __name__ == '__main__':
    unittest.main()
