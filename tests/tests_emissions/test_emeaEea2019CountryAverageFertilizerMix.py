import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.emissions.emeaEea2019CountryAverageFertilizerMix\
    import EmeaEea2019CountryAverageFertilizerMix


class Test_emeaEea2019CountryAverageFertilizerMix(unittest.TestCase):
    def __init__(self, *args, **kwargs):

        super(Test_emeaEea2019CountryAverageFertilizerMix, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()

        engine.import_data(cycle=cycle, site=site)

        self.model = EmeaEea2019CountryAverageFertilizerMix()

        self.representation = engine.data.safe_representation

        self.nh3ToAirInorganicFertilizer_expected = 0

    def test_check_nh3ToAirInorganicFertilizer(self):

        check_computed = self.model.check_nh3ToAirInorganicFertilizer(self.representation)

        self.assertEqual(True, check_computed)

    def test_nh3ToAirInorganicFertilizer(self):

        condition = self.model.check_nh3ToAirInorganicFertilizer(self.representation)
        nh3ToAirInorganicFertilizer_computed = \
            self.model.calculate_nh3ToAirInorganicFertilizer()\
            if condition else self.nh3ToAirInorganicFertilizer_expected

        self.assertEqual(self.nh3ToAirInorganicFertilizer_expected, nh3ToAirInorganicFertilizer_computed)

    def test_check_nh3ToAirInorganicFertilizer_no_inputs(self):

        del (self.representation['inputs'])
        self.representation['dataCompleteness']['fertilizer'] = False
        check_computed = self.model.check_nh3ToAirInorganicFertilizer(self.representation)

        self.assertEqual(False, check_computed)

    def test_check_nh3ToAirInorganicFertilizer_no_site(self):

        del (self.representation['site'])
        check_computed = self.model.check_nh3ToAirInorganicFertilizer(self.representation)

        self.assertEqual(False, check_computed)


if __name__ == '__main__':
    unittest.main()
