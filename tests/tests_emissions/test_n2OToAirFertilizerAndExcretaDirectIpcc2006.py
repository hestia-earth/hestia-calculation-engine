import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.utils import EngineDict
from hestia_earth.calculation.emissions.n2OToAirFertilizerAndExcretaDirectIpcc2006\
    import N2OToAirFertilizerAndExcretaDirectIpcc2006


class Test_n2OToAirFertilizerAndExcretaDirectIpcc2006(unittest.TestCase):
    def __init__(self, *args, **kwargs):

        super(Test_n2OToAirFertilizerAndExcretaDirectIpcc2006, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()

        self.model = N2OToAirFertilizerAndExcretaDirectIpcc2006()

        engine.import_data(cycle, site)

        self.representation = engine.data.safe_representation

        self.check_expected = True
        self.n2OToAirInorganicFertilizerDirect_expected = 1.0997751124437782
        self.n2OToAirOrganicFertilizerDirect_expected = 0.0

    def test_check_n2OToAirInorganicFertilizerDirect(self):

        check_computed = self.model.check_n2OToAirInorganicFertilizerDirect(self.representation)

        self.assertEqual(self.check_expected, check_computed)

    def test_check_n2OToAirOrganicFertilizerDirect(self):

        check_computed = self.model.check_n2OToAirOrganicFertilizerDirect(self.representation)

        self.assertEqual(self.check_expected, check_computed)

    def test_calculate_n2OToAirInorganicFertilizerDirect(self):

        condition = self.model.check_n2OToAirInorganicFertilizerDirect(self.representation)
        n2OToAirInorganicFertilizerDirect_computed =\
            self.model.calculate_n2OToAirInorganicFertilizerDirect()\
            if condition else self.n2OToAirInorganicFertilizerDirect_expected

        self.assertEqual(self.n2OToAirInorganicFertilizerDirect_expected, n2OToAirInorganicFertilizerDirect_computed)

    def test_calculate_n2OToAirOrganicFertilizerDirect(self):

        condition = self.model.check_n2OToAirOrganicFertilizerDirect(self.representation)
        n2OToAirOrganicFertilizerDirect_computed = self.model.calculate_n2OToAirOrganicFertilizerDirect()\
            if condition else self.n2OToAirOrganicFertilizerDirect_expected

        self.assertEqual(self.n2OToAirOrganicFertilizerDirect_expected, n2OToAirOrganicFertilizerDirect_computed)

    def test_check_n2OToAirInorganicFertilizerDirect_neither_inputs_nor_products(self):

        del(self.representation['inputs'])
        del(self.representation['products'])
        check_computed = self.model.check_n2OToAirInorganicFertilizerDirect(self.representation)

        self.assertEqual(False, check_computed)

    def test_check_n2OToAirOrganicFertilizerDirect_neither_inputs_nor_products(self):

        del(self.representation['inputs'])
        del(self.representation['products'])
        check_computed = self.model.check_n2OToAirOrganicFertilizerDirect(self.representation)

        self.assertEqual(False, check_computed)

    def test_n2OToAirInorganicFertilizerDirect_paddy_rice(self):

        n2OToAirInorganicFertilizerDirect_expected = 0.7164249303919469  # Spreadsheet 608
        del(self.representation['inputs'])
        del(self.representation['products'])
        self.representation['inputs'] = EngineDict({'ammoniumNitrateAsN': {'term': {'@id': 'compostAsN',
                                                                                    'units': 'kg N',
                                                                                    'termType': 'inorganicFertilizer'},
                                                                           'value': [152]}
                                                    })
        self.representation['products'] = EngineDict({'riceGrainPaddy': {'term': {'@id': 'riceGrainPaddy',
                                                                                  'termType': 'crop'},
                                                                         'value': [7500],
                                                                         'primary': True}})
        condition = self.model.check_n2OToAirInorganicFertilizerDirect(self.representation)
        n2OToAirInorganicFertilizerDirect_computed = \
            self.model.calculate_n2OToAirInorganicFertilizerDirect() if condition else\
            self.n2OToAirInorganicFertilizerDirect_expected

        self.assertEqual(n2OToAirInorganicFertilizerDirect_expected, n2OToAirInorganicFertilizerDirect_computed)

    def test_n2OToAirOrganicFertilizerDirect_paddy_rice(self):

        n2OToAirOrganicFertilizerDirect_expected = 0.2892094324266438  # Spreadsheet 609
        del(self.representation['products'])
        self.representation['inputs']['compostAsN'] = EngineDict({'term': {'@id': 'compostAsN', 'units': 'kg N',
                                                                           'termType': 'organicFertilizer'},
                                                                  'value': [61.36]})
        self.representation['products'] = EngineDict({'riceGrainPaddy': {'term': {'@id': 'riceGrainPaddy',
                                                                                  'termType': 'crop'},
                                                                         'value': [5170],
                                                                         'primary': True}})
        condition = self.model.check_n2OToAirOrganicFertilizerDirect(self.representation)
        n2OToAirOrganicFertilizerDirect_computed = \
            self.model.calculate_n2OToAirOrganicFertilizerDirect() if condition else\
            self.n2OToAirOrganicFertilizerDirect_expected

        self.assertEqual(n2OToAirOrganicFertilizerDirect_expected, n2OToAirOrganicFertilizerDirect_computed)


if __name__ == '__main__':
    unittest.main()
