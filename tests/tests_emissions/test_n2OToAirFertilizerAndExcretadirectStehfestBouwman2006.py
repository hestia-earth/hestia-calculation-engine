import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.emissions.n2OToAirFertilizerAndExcretadirectStehfestBouwman2006\
    import N2OToAirFertilizerAndExcretadirectStehfestBouwman2006


class Test_n2OToAirFertilizerAndExcretadirectStehfestBouwman2006(unittest.TestCase):
    def __init__(self, *args, **kwargs):

        super(Test_n2OToAirFertilizerAndExcretadirectStehfestBouwman2006, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()

        engine.import_data(cycle=cycle, site=site)

        self.model = N2OToAirFertilizerAndExcretadirectStehfestBouwman2006()
        self.representation = engine.data.safe_representation

        self.check_expected = True
        self.n2OToAirAllOriginsDirect_expected = 2.0382021488243454
        self.n2OToAirInorganicFertilizerDirect_expected = 2.0382021488243454
        self.n2OToAirOrganicFertilizerDirect_expected = 0.0
        self.n2OToAirExcretaDirect_expected = 0.0
        self.n2OToAirCropResidueDecompositionDirect_expected = 0.04008680284857571

    def test_check_n2OToAirAllOriginsDirect(self):

        check_computed = self.model.check_n2OToAirAllOriginsDirect(self.representation)

        self.assertEqual(True, check_computed)

    def test_check_n2OToAirInorganicFertilizerDirect(self):

        check_computed = self.model.check_n2OToAirInorganicFertilizerDirect(self.representation)

        self.assertEqual(True, check_computed)

    def test_check_n2OToAirOrganicFertilizerDirect(self):

        check_computed = self.model.check_n2OToAirOrganicFertilizerDirect(self.representation)

        self.assertEqual(True, check_computed)

    def test_check_n2OToAirExcretaDirect(self):

        check_computed = self.model.check_n2OToAirExcretaDirect(self.representation)

        self.assertEqual(True, check_computed)

    def test_check_n2OToAirCropResidueDecompositionDirect(self):

        check_computed = self.model.check_n2OToAirCropResidueDecompositionDirect(self.representation)

        self.assertEqual(True, check_computed)

    def test_check_n2OToAirAllOriginsDirect_no_products(self):

        del(self.representation['products'])
        check_computed = self.model.check_n2OToAirAllOriginsDirect(self.representation)

        self.assertEqual(False, check_computed)

    def test_check_n2OToAirAllOriginsDirect_no_inputs(self):

        del(self.representation['inputs'])
        self.representation['dataCompleteness']['fertilizer'] = False
        check_computed = self.model.check_n2OToAirAllOriginsDirect(self.representation)

        self.assertEqual(False, check_computed)

    def test_check_n2OToAirAllOriginsDirect_no_site(self):

        del(self.representation['site'])
        check_computed = self.model.check_n2OToAirAllOriginsDirect(self.representation)

        self.assertEqual(False, check_computed)

    def test_n2OToAirAllOriginsDirect(self):

        condition = self.model.check_n2OToAirAllOriginsDirect(self.representation)
        n2OToAirAllOriginsDirect_computed = self.model.calculate_n2OToAirAllOriginsDirect()\
            if condition else self.n2OToAirAllOriginsDirect_expected

        self.assertEqual(self.n2OToAirAllOriginsDirect_expected, n2OToAirAllOriginsDirect_computed)

    def test_n2OToAirInorganicFertilizerDirect(self):

        condition = self.model.check_n2OToAirInorganicFertilizerDirect(self.representation)
        n2OToAirInorganicFertilizerDirect_computed =\
            self.model.calculate_n2OToAirInorganicFertilizerDirect()\
            if condition else self.n2OToAirInorganicFertilizerDirect_expected

        self.assertEqual(self.n2OToAirInorganicFertilizerDirect_expected, n2OToAirInorganicFertilizerDirect_computed)

    def test_n2OToAirOrganicFertilizerDirect(self):

        condition = self.model.check_n2OToAirOrganicFertilizerDirect(self.representation)
        n2OToAirOrganicFertilizerDirect_computed =\
            self.model.calculate_n2OToAirOrganicFertilizerDirect()\
            if condition else self.n2OToAirOrganicFertilizerDirect_expected

        self.assertEqual(self.n2OToAirOrganicFertilizerDirect_expected, n2OToAirOrganicFertilizerDirect_computed)


if __name__ == '__main__':
    unittest.main()
