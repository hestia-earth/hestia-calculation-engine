import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.emissions.so2ToAirFuelCombustionEMEAEEA2019 import SO2ToAirFuelCombustionEMEAEEA2019


class Test_SO2ToAirFuelCombustionEMEAEEA2019(unittest.TestCase):
    def __init__(self, *args, **kwargs):

        super(Test_SO2ToAirFuelCombustionEMEAEEA2019, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()

        engine.import_data(cycle, site)

        self.model = SO2ToAirFuelCombustionEMEAEEA2019()

        self.representation = engine.data.safe_representation

        self.check_expected = True
        self.so2ToAirFuelCombustion_expected = 0.036296

    def test_check_so2ToAirFuelCombustion(self):

        check_computed = self.model.check_so2ToAirFuelCombustion(self.representation)

        self.assertEqual(check_computed, self.check_expected)

    def test_so2ToAirFuelCombustion(self):

        self.model.check_so2ToAirFuelCombustion(self.representation)
        so2ToAirFuelCombustion_computed = \
            self.model.calculate_so2ToAirFuelCombustion()

        self.assertEqual(self.so2ToAirFuelCombustion_expected, so2ToAirFuelCombustion_computed)

    def test_check_so2ToAirFuelCombustion_no_inputs(self):

        del(self.representation['inputs'])
        self.representation['dataCompleteness']['electricityFuel'] = False
        check_computed = self.model.check_so2ToAirFuelCombustion(self.representation)

        self.assertEqual(False, check_computed)


if __name__ == '__main__':
    unittest.main()
