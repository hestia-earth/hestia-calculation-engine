import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.utils import EngineDict
from hestia_earth.calculation.emissions.no3ToGroundwaterAllInputsPooreNemecek2018\
    import NO3ToGroundwaterAllInputsPooreNemecek2018


class Test_no3ToGroundwaterAllInputsPooreNemecek2018(unittest.TestCase):
    def __init__(self, *args, **kwargs):

        super(Test_no3ToGroundwaterAllInputsPooreNemecek2018, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()

        self.model = NO3ToGroundwaterAllInputsPooreNemecek2018()

        engine.import_data(cycle, site)

        self.representation = engine.data.representation_cycle

        self.check_leaching_expected = 0.067
        self.no3ToGroundwaterAllOrigins_expected = 21.517696494752624
        self.no3ToGroundwaterOrganicFertilizer_expected = 0.0
        self.no3ToGroundwaterInorganicFertilizer_expected = 20.76095952023988
        self.no3ToGroundwaterExcreta_expected = 0.0
        self.no3ToGroundwaterCropResidueDecomposition_expected = 0.7567369745127435

    def test_check_no3ToGroundwaterAllOrigins(self):

        check_computed = self.model.check_no3ToGroundwaterAllOrigins(self.representation)

        self.assertEqual(True, check_computed)

    def test_check_no3ToGroundwaterOrganicFertilizer(self):

        self.representation['dataCompleteness']['fertilizer'] = False
        check_computed = self.model.check_no3ToGroundwaterOrganicFertilizer(self.representation)

        self.assertEqual(False, check_computed)

    def test_check_no3ToGroundwaterInorganicFertilizer(self):

        check_computed = self.model.check_no3ToGroundwaterInorganicFertilizer(self.representation)

        self.assertEqual(True, check_computed)

    def test_check_no3ToGroundwaterExcreta(self):

        self.representation['dataCompleteness']['products'] = False
        check_computed = self.model.check_no3ToGroundwaterExcreta(self.representation)

        self.assertEqual(False, check_computed)

    def test_check_no3ToGroundwaterCropResidueDecomposition(self):

        check_computed = self.model.check_no3ToGroundwaterCropResidueDecomposition(self.representation)

        self.assertEqual(True, check_computed)

    def test_check_no3ToGroundwaterAllOrigins_no_products(self):

        del(self.representation['products'])
        check_computed = self.model.check_no3ToGroundwaterAllOrigins(self.representation)

        self.assertEqual(False, check_computed)

    def test_check_no3ToGroundwaterAllOrigins_no_site(self):

        del(self.representation['site'])
        check_computed = self.model.check_no3ToGroundwaterAllOrigins(self.representation)

        self.assertEqual(True, check_computed)

    def test_check_no3ToGroundwaterInorganicFertilizer_no_inputs(self):

        del(self.representation['inputs'])
        self.representation['dataCompleteness']['fertilizer'] = False
        check_computed = self.model.check_no3ToGroundwaterInorganicFertilizer(self.representation)

        self.assertEqual(False, check_computed)

    def test_check_no3ToGroundwaterOrganicFertilizer_no_inputs(self):

        del(self.representation['inputs'])
        self.representation['dataCompleteness']['fertilizer'] = False
        check_computed = self.model.check_no3ToGroundwaterOrganicFertilizer(self.representation)

        self.assertEqual(False, check_computed)

    def test_check_no3ToGroundwaterExcreta_no_products(self):

        del(self.representation['products'])
        check_computed = self.model.check_no3ToGroundwaterExcreta(self.representation)

        self.assertEqual(False, check_computed)

    def test_check_no3ToGroundwaterCropResidueDecomposition_no_products(self):

        del(self.representation['products'])
        check_computed = self.model.check_no3ToGroundwaterCropResidueDecomposition(self.representation)

        self.assertEqual(False, check_computed)

    def test_no3ToGroundwaterAllOrigins(self):

        condition = self.model.check_no3ToGroundwaterAllOrigins(self.representation)
        no3ToGroundwaterAllOrigins_computed = self.model.calculate_no3ToGroundwaterAllOrigins()\
            if condition else self.no3ToGroundwaterAllOrigins_expected

        self.assertEqual(self.no3ToGroundwaterAllOrigins_expected, no3ToGroundwaterAllOrigins_computed)

    def test_no3ToGroundwaterInorganicFertilizer(self):

        condition = self.model.check_no3ToGroundwaterInorganicFertilizer(self.representation)
        self.model.calculate_no3ToGroundwaterInorganicFertilizer()
        no3ToGroundwaterInorganicFertilizer_computed =\
            self.model.calculate_no3ToGroundwaterInorganicFertilizer()\
            if condition else self.no3ToGroundwaterInorganicFertilizer_expected

        self.assertEqual(self.no3ToGroundwaterInorganicFertilizer_expected,
                         no3ToGroundwaterInorganicFertilizer_computed)

    def test_no3ToGroundwaterOrganicFertilizer(self):

        condition = self.model.check_no3ToGroundwaterOrganicFertilizer(self.representation)
        no3ToGroundwaterOrganicFertilizer_computed =\
            self.model.calculate_no3ToGroundwaterOrganicFertilizer()\
            if condition else self.no3ToGroundwaterOrganicFertilizer_expected

        self.assertEqual(self.no3ToGroundwaterOrganicFertilizer_expected, no3ToGroundwaterOrganicFertilizer_computed)

    def test_no3ToGroundwaterExcreta(self):

        condition = self.model.check_no3ToGroundwaterExcreta(self.representation)
        no3ToGroundwaterExcreta_computed = self.model.calculate_no3ToGroundwaterExcreta()\
            if condition else self.no3ToGroundwaterExcreta_expected

        self.assertEqual(self.no3ToGroundwaterExcreta_expected, no3ToGroundwaterExcreta_computed)

    def test_no3ToGroundwaterCropResidueDecomposition(self):

        condition = self.model.check_no3ToGroundwaterCropResidueDecomposition(self.representation)
        no3ToGroundwaterCropResidueDecomposition_computed = \
            self.model.calculate_no3ToGroundwaterCropResidueDecomposition()\
            if condition else self.no3ToGroundwaterCropResidueDecomposition_expected

        self.assertEqual(self.no3ToGroundwaterCropResidueDecomposition_expected,
                         no3ToGroundwaterCropResidueDecomposition_computed)

    def test_no3ToGroundwaterOrganicFertilizer_extra(self):

        no3ToGroundwaterOrganicFertilizer_expected = 29.65851360034268
        self.representation['inputs']['compostAsN'] = EngineDict({'term': {'@id': 'compostAsN', 'units': 'kg N',
                                                                           'termType': 'organicFertilizer'},
                                                                  'value': [100]})
        condition = self.model.check_no3ToGroundwaterOrganicFertilizer(self.representation)
        no3ToGroundwaterOrganicFertilizer_computed =\
            self.model.calculate_no3ToGroundwaterOrganicFertilizer()\
            if condition else no3ToGroundwaterOrganicFertilizer_expected

        self.assertEqual(no3ToGroundwaterOrganicFertilizer_expected, no3ToGroundwaterOrganicFertilizer_computed)

    def test_no3ToGroundwaterExcreta_extra(self):

        no3ToGroundwaterExcreta_expected = 29.658513600342683  # Sah mod
        self.representation['products']['test'] = EngineDict({'term': {'@id': 'test',
                                                                       'termType': 'animalProduct',
                                                                       'units': 'kg N'},
                                                              'value': [100]})
        condition = self.model.check_no3ToGroundwaterExcreta(self.representation)
        no3ToGroundwaterExcreta_computed =\
            self.model.calculate_no3ToGroundwaterExcreta()\
            if condition else no3ToGroundwaterExcreta_expected

        self.assertEqual(no3ToGroundwaterExcreta_expected, no3ToGroundwaterExcreta_computed)

    def test_get_leaching_factor_1(self):

        self.model.check_no3ToGroundwaterAllOrigins(self.representation)
        check_leaching_computed = self.model.get_leaching_factor()

        self.assertEqual(self.check_leaching_expected, check_leaching_computed)

    def test_get_leaching_factor_2(self):

        self.model.rootingDepth = 0.1
        self.model.clay = 0.4
        self.model.sand = 0.9
        self.model.precipitation = 1400
        check_leaching_expected = 0.23
        check_leaching_computed = self.model.get_leaching_factor()

        self.assertEqual(check_leaching_expected, check_leaching_computed)

    def test_get_leaching_factor_3(self):

        self.model.rootingDepth = 0.5
        self.model.clay = 0.5
        self.model.sand = 0.85
        self.model.precipitation = 800
        check_leaching_expected = 0.12
        check_leaching_computed = self.model.get_leaching_factor()

        self.assertEqual(check_leaching_expected, check_leaching_computed)


if __name__ == '__main__':
    unittest.main()
