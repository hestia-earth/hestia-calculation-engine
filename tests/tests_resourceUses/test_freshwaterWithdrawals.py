import unittest
import copy
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.utils import EngineDict
from hestia_earth.calculation.emissionsResourceUses.freshwaterWithdrawals\
    import FreshwaterWithdrawals


class Test_freshwaterWithdrawals(unittest.TestCase):

    def __init__(self, *args, **kwargs):

        super(Test_freshwaterWithdrawals, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()
        engine.import_data(cycle, site)

        self.model = FreshwaterWithdrawals()
        self.representation = engine.data.safe_representation
        self.emissionsResourceUse = engine.data.safe_representation['emissionsResourceUse']

    def test_check_freshwaterWithdrawals(self):

        products = copy.deepcopy(self.representation['products'].evalues())
        check_computed = self.model.check(self.representation, products[0])
        self.assertEqual(True, check_computed)

    def test_check_freshwaterWithdrawals_no_cycle(self):

        products = copy.deepcopy(self.representation['products'].evalues())
        check_computed = self.model.check(EngineDict(), products[0])
        self.assertEqual(False, check_computed)

    def test_check_freshwaterWithdrawals_no_product(self):

        check_computed = self.model.check(self.representation, EngineDict())
        self.assertEqual(False, check_computed)

    def test_freshwaterWithdrawals(self):

        expected = {'freshwaterWithdrawals': 0.0}

        products = copy.deepcopy(self.representation['products'].evalues())
        condition = self.model.check(self.representation, products[0])
        computed = self.model.calculate() if condition else expected
        self.assertEqual(expected, computed)


if __name__ == '__main__':
    unittest.main()
