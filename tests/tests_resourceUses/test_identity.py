import unittest
import copy
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.utils import EngineDict
from hestia_earth.calculation.emissionsResourceUses.identity\
    import Identity


class Test_identity(unittest.TestCase):

    def __init__(self, *args, **kwargs):

        super(Test_identity, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()
        engine.import_data(cycle, site)

        self.model = Identity()
        self.representation = engine.data.safe_representation
        self.emissionsResourceUse = engine.data.safe_representation['emissionsResourceUse']

    def test_check_identity(self):

        products = copy.deepcopy(self.representation['products'].evalues())
        check_computed = self.model.check(self.representation, products[0])
        self.assertEqual(True, check_computed)

    def test_check_identity_no_cycle(self):

        products = copy.deepcopy(self.representation['products'].evalues())
        check_computed = self.model.check(EngineDict(), products[0])
        self.assertEqual(False, check_computed)

    def test_check_identity_no_product(self):

        check_computed = self.model.check(self.representation, EngineDict())
        self.assertEqual(False, check_computed)

    def test_identity(self):

        expected = {'ch4ToAirSoil': 5.5172413793103446e-05,
                    'gwp100': 0.010689655172413793,
                    'co2ToAirFuelCombustion': 0.01689655172413793,
                    'co2ToAirSoilCarbonStockChange': 0.08758620689655172,
                    'co2ToAirUreaHydrolysis': 0.03793103448275862,
                    'n2OToAirCropResidueDecompositionDirect': 9.258620689655172e-06,
                    'n2OToAirInorganicFertilizerDirect': 0.00019787241379310344,
                    'n2OToAirInorganicFertilizerIndirect': 3.818620689655173e-05}

        products = copy.deepcopy(self.representation['products'].evalues())
        condition = self.model.check(self.representation, products[0])
        computed = self.model.calculate() if condition else expected
        self.assertEqual(expected, computed)


if __name__ == '__main__':
    unittest.main()
