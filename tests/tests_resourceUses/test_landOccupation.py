import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.emissionsResourceUses.landOccupation import LandOccupation


class Test_landOccupation(unittest.TestCase):
    def test_calculate_landOccupation(self):

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()
        self.model = LandOccupation()
        engine.import_data(cycle, site)
        self.representation_cycle = engine.data.representation_cycle
        self.landOccupation_expected = {'landOccupation': 1.7241379310344827}

        condition = self.model.check(self.representation_cycle, self.representation_cycle['products']['cerealsGrain'])
        landOccupation_computed = self.model.calculate() if condition else None

        self.assertEqual(self.landOccupation_expected, landOccupation_computed)

    def test_hazelnuts(self):
        # Iranian Hazelnut example
        with open(f"{fixtures_path}/landOccupation/treenut_example/cycle.jsonld") as jsonld_file:
            nut_cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/landOccupation/treenut_example/site.jsonld") as jsonld_file:
            nut_site = json.load(jsonld_file)

        engine = Engine()
        self.model = LandOccupation()
        engine.import_data(cycle=nut_cycle, site=nut_site)
        self.representation_cycle = engine.data.representation_cycle
        self.landOccupation_HZ_expected = {'landOccupation': 68.3173451126091}

        condition = self.model.check(self.representation_cycle,
                                     self.representation_cycle['products']['hazelnutInShell'])
        landOccupation_computed = self.model.calculate() if condition else None

        self.assertEqual(self.landOccupation_HZ_expected, landOccupation_computed)


if __name__ == '__main__':
    unittest.main()
