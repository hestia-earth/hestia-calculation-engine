import unittest
import copy
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.utils import EngineDict
from hestia_earth.calculation.impacts.leqScarcityWeightedWaterUseAware\
    import LeqScarcityWeightedWaterUseAware


class Test_leqScarcityWeightedWaterUseAware(unittest.TestCase):

    def __init__(self, *args, **kwargs):

        super(Test_leqScarcityWeightedWaterUseAware, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/calculated_impact_assessment.jsonld") as jsonld_file:
            assessment = json.load(jsonld_file)

        engine = Engine()
        engine.import_data(cycle=cycle, site=site, assessments=[assessment[0]])

        self.model = LeqScarcityWeightedWaterUseAware()
        self.representation = engine.data.safe_representation
        self.emissionsResourceUse = engine.data.safe_representation['emissionsResourceUse']

    def test_check_leqScarcityWeightedWaterUseAware(self):

        self.representation['site']['awareWaterBasinId'] = "21"
        product = copy.deepcopy(self.representation['product'])
        product['term'] = {'@id': product['@id']}
        check_computed = self.model.check(self.representation, self.representation['products'].evalues()[0],
                                          self.emissionsResourceUse)
        self.assertEqual(True, check_computed)

    def test_check_leqScarcityWeightedWaterUseAware_no_product(self):

        check_computed = self.model.check(self.representation, EngineDict(), self.emissionsResourceUse)
        self.assertEqual(False, check_computed)

    def test_check_leqScarcityWeightedWaterUseAware_no_site(self):

        del(self.representation['site'])
        product = copy.deepcopy(self.representation['product'])
        check_computed = self.model.check(self.representation, product, self.emissionsResourceUse)
        self.assertEqual(False, check_computed)

    def test_check_leqScarcityWeightedWaterUseAware_no_emissionsResourceUse_1(self):

        product = copy.deepcopy(self.representation['product'])
        product['term'] = {'@id': product['@id']}
        check_computed = self.model.check(self.representation, product, [])
        self.assertEqual(False, check_computed)

    def test_check_leqScarcityWeightedWaterUseAware_no_emissionsResourceUse_2(self):

        product = copy.deepcopy(self.representation['product'])
        product['term'] = {'@id': product['@id']}
        check_computed = self.model.check(self.representation, product, {})
        self.assertEqual(False, check_computed)

    def test_leqScarcityWeightedWaterUseAware(self):

        expected = 0.0

        self.representation['site']['awareWaterBasinId'] = "25"
        product = copy.deepcopy(self.representation['product'])
        product['term'] = {'@id': product['@id']}
        condition = self.model.check(self.representation, self.representation['products'].evalues()[0],
                                     self.emissionsResourceUse)
        computed = self.model.calculate() if condition else expected
        self.assertEqual(expected, computed)

    def test_leqScarcityWeightedWaterUseAware_no_basin(self):

        expected = 0.0

        self.representation['site']['awareWaterBasinId'] = "1000"
        product = copy.deepcopy(self.representation['product'])
        product['term'] = {'@id': product['@id']}
        condition = self.model.check(self.representation, self.representation['products'].evalues()[0],
                                     self.emissionsResourceUse)
        computed = self.model.calculate() if condition else expected
        self.assertEqual(expected, computed)

    def test_leqScarcityWeightedWaterUseAware_duplicated(self):

        expected = 0.0

        self.representation['site']['awareWaterBasinId'] = "53561"
        product = copy.deepcopy(self.representation['product'])
        product['term'] = {'@id': product['@id']}
        condition = self.model.check(self.representation, self.representation['products'].evalues()[0],
                                     self.emissionsResourceUse)
        computed = self.model.calculate() if condition else expected
        self.assertEqual(expected, computed)

    def test_annual_characterization_factor_1(self):

        basin_id = "b7372"  # Sah
        self.model.siteType = 'cropland'
        expected = 13.82119037
        computed = self.model.annual_characterization_factor(basin_id)
        self.assertEqual(expected, computed)

    def test_annual_characterization_factor_2(self):

        basin_id = "b10492"  # Dalal
        self.model.siteType = 'other'
        expected = 100
        computed = self.model.annual_characterization_factor(basin_id)
        self.assertEqual(expected, computed)

    def test_annual_characterization_factor_3(self):

        basin_id = "b1000000"
        self.model.siteType = 'other'
        expected = {}
        computed = self.model.annual_characterization_factor(basin_id)
        self.assertEqual(expected, computed)


if __name__ == '__main__':
    unittest.main()
