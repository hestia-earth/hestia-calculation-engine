import unittest
import copy
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.utils import EngineDict, EngineList
from hestia_earth.calculation.impacts.so2EqTerrestrialAcidificationExcludingFateCml2001NonBaseline\
    import So2EqTerrestrialAcidificationExcludingFateCml2001NonBaseline


class Test_so2EqTerrestrialAcidificationExcludingFateCml2001NonBaseline(unittest.TestCase):

    def __init__(self, *args, **kwargs):

        super(Test_so2EqTerrestrialAcidificationExcludingFateCml2001NonBaseline, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/calculated_impact_assessment.jsonld") as jsonld_file:
            assessment = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()
        engine.import_data(cycle=None, site=site, assessments=[assessment[0]])

        self.model = So2EqTerrestrialAcidificationExcludingFateCml2001NonBaseline()
        self.representation = engine.data.safe_representation
        self.emissionsResourceUse = EngineList(assessment[0]['emissionsResourceUse']).to_edict()

    def test_check_so2EqTerrestrialAcidificationExcludingFateCml2001NonBaseline(self):

        product = copy.deepcopy(self.representation['product'])
        product['term'] = {'@id': product['@id']}
        check_computed = self.model.check(self.representation, product, self.emissionsResourceUse)
        self.assertEqual(True, check_computed)

    def test_check_so2EqTerrestrialAcidificationExcludingFateCml2001NonBaseline_no_cycle(self):

        product = copy.deepcopy(self.representation['product'])
        product['term'] = {'@id': product['@id']}
        check_computed = self.model.check({}, product, self.emissionsResourceUse)
        self.assertEqual(True, check_computed)

    def test_check_so2EqTerrestrialAcidificationExcludingFateCml2001NonBaseline_no_product(self):

        check_computed = self.model.check(self.representation, {}, self.emissionsResourceUse)
        self.assertEqual(True, check_computed)

    def test_check_so2EqTerrestrialAcidificationExcludingFateCml2001NonBaseline_no_emissionsResourceUse_1(self):

        product = copy.deepcopy(self.representation['product'])
        product['term'] = {'@id': product['@id']}
        check_computed = self.model.check(self.representation, product, EngineDict())
        self.assertEqual(False, check_computed)

    def test_check_so2EqTerrestrialAcidificationExcludingFateCml2001NonBaseline_no_emissionsResourceUse_2(self):

        product = copy.deepcopy(self.representation['product'])
        product['term'] = {'@id': product['@id']}
        check_computed = self.model.check(self.representation, product, EngineDict())
        self.assertEqual(False, check_computed)

    def test_so2EqTerrestrialAcidificationExcludingFateCml2001NonBaseline(self):

        expected = 0.015025866163507798

        product = copy.deepcopy(self.representation['product'])
        product['term'] = {'@id': product['@id']}
        condition = self.model.check(self.representation, product, self.emissionsResourceUse)
        computed = self.model.calculate() if condition else expected
        self.assertEqual(expected, computed)


if __name__ == '__main__':
    unittest.main()
