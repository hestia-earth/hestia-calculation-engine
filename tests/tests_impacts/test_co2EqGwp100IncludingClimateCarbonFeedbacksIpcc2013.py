import unittest
import copy
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.utils import EngineDict, EngineList
from hestia_earth.calculation.impacts.co2EqGwp100IncludingClimateCarbonFeedbacksIpcc2013\
    import Co2EqGwp100IncludingClimateCarbonFeedbacksIpcc2013


class Test_co2EqGwp100IncludingClimateCarbonFeedbacksIpcc2013(unittest.TestCase):

    def __init__(self, *args, **kwargs):

        super(Test_co2EqGwp100IncludingClimateCarbonFeedbacksIpcc2013, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/WI5hWtsqlu/calculated_impact_assessment.jsonld") as jsonld_file:
            assessment = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()
        engine.import_data(cycle=None, site=site, assessments=[assessment[0]])

        self.model = Co2EqGwp100IncludingClimateCarbonFeedbacksIpcc2013()
        self.representation = engine.data.safe_representation
        self.emissionsResourceUse = EngineList(assessment[0]['emissionsResourceUse']).to_edict()

    def test_check_co2EqGwp100IncludingClimateCarbonFeedbacksIpcc2013(self):

        product = copy.deepcopy(self.representation['product'])
        product['term'] = {'@id': product['@id']}
        check_computed = self.model.check(self.representation, product, self.emissionsResourceUse)
        self.assertEqual(True, check_computed)

    def test_check_co2EqGwp100IncludingClimateCarbonFeedbacksIpcc2013_no_cycle(self):

        product = copy.deepcopy(self.representation['product'])
        product['term'] = {'@id': product['@id']}
        check_computed = self.model.check({}, product, self.emissionsResourceUse)
        self.assertEqual(True, check_computed)

    def test_check_co2EqGwp100IncludingClimateCarbonFeedbacksIpcc2013_no_product(self):

        check_computed = self.model.check(self.representation, {}, self.emissionsResourceUse)
        self.assertEqual(True, check_computed)

    def test_check_co2EqGwp100IncludingClimateCarbonFeedbacksIpcc2013_no_emissionsResourceUse_1(self):

        product = copy.deepcopy(self.representation['product'])
        product['term'] = {'@id': product['@id']}
        check_computed = self.model.check(self.representation, product, EngineDict())
        self.assertEqual(False, check_computed)

    def test_check_co2EqGwp100IncludingClimateCarbonFeedbacksIpcc2013_no_emissionsResourceUse_2(self):

        product = copy.deepcopy(self.representation['product'])
        product['term'] = {'@id': product['@id']}
        check_computed = self.model.check(self.representation, product, EngineDict())
        self.assertEqual(False, check_computed)

    def test_co2EqGwp100IncludingClimateCarbonFeedbacksIpcc2013(self):

        expected = 0.5648307934051733

        product = copy.deepcopy(self.representation['product'])
        product['term'] = {'@id': product['@id']}
        condition = self.model.check(self.representation, product, self.emissionsResourceUse)
        computed = self.model.calculate() if condition else expected
        self.assertEqual(expected, computed)


if __name__ == '__main__':
    unittest.main()
