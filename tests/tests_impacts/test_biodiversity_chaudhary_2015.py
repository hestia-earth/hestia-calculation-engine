import unittest
import copy
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.utils import EngineDict, EngineList
from hestia_earth.calculation.impacts.biodiversityImpactLandUseChaudhary2015\
    import BiodiversityImpactLandUseChaudhary2015


class Test_biodiversity_c2015_model(unittest.TestCase):

    def __init__(self, *args, **kwargs):

        super(Test_biodiversity_c2015_model, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/biodiversityImpact/calculated_impact_assessment.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/biodiversityImpact/site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        engine = Engine()

        self.model = BiodiversityImpactLandUseChaudhary2015()

        engine.import_data(cycle[0], site)

        self.representation = engine.data.safe_representation

        self.emissionsResourceUse = EngineList(engine.data.safe_representation['emissionsResourceUse']).to_edict()

        self.check_expected = True

    def test_checkBioimpactChaudhary2015(self):

        product = copy.deepcopy(self.representation['product'])
        product['economicValueShare'] = 100
        product['term'] = {'@id': product['@id']}
        self.emissionsResourceUse['landTransformation20YearAverage'] = {'value': 0}
        check_computed = self.model.check(self.representation, product, self.emissionsResourceUse)
        self.assertEqual(check_computed, self.check_expected)

    def test_biodiversity_calc(self):
        # landOccupation example
        with open(f"{fixtures_path}/biodiversityImpact/calculated_impact_assessment.jsonld") as jsonld_file:
            bio_cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/biodiversityImpact/site.jsonld") as jsonld_file:
            bio_site = json.load(jsonld_file)

        engine = Engine()
        self.model = BiodiversityImpactLandUseChaudhary2015()
        engine.import_data(bio_cycle[0], bio_site)
        self.representation_cycle = engine.data.safe_representation
        self.bio_emissionsResourceUse = EngineList(engine.data.safe_representation['emissionsResourceUse']).to_edict()
        self.bioimpact_expected = 1.183918258551755e-13

        product = copy.deepcopy(self.representation['product'])
        product['term'] = {'@id': product['@id']}
        condition = self.model.check(self.representation, product, self.emissionsResourceUse)
        bioImpact_coumputed = self.model.calculate() if condition else self.bioimpact_expected

        self.assertEqual(self.bioimpact_expected, bioImpact_coumputed)

    def test_biodiversity_calc_with_ecoregion(self):
        # landOccupation example
        with open(f"{fixtures_path}/biodiversityImpact/calculated_impact_assessment.jsonld") as jsonld_file:
            bio_cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/biodiversityImpact/site_with_ecoregion.jsonld") as jsonld_file:
            bio_site_ecoregion = json.load(jsonld_file)

        engine = Engine()
        self.model = BiodiversityImpactLandUseChaudhary2015()
        engine.import_data(bio_cycle[0], bio_site_ecoregion)
        self.representation_cycle_eco = engine.data.safe_representation
        self.bio_emissionsResourceUse_eco =\
            EngineList(engine.data.safe_representation['emissionsResourceUse']).to_edict()
        self.bioimpact_expected_eco = 1.3505108840515328e-13

        product = copy.deepcopy(self.representation_cycle_eco['product'])
        product['term'] = {'@id': product['@id']}
        condition = self.model.check(self.representation_cycle_eco, product, self.bio_emissionsResourceUse_eco)
        bioImpact_coumputed_eco = self.model.calculate() if condition else self.bioimpact_expected_eco

        self.assertEqual(self.bioimpact_expected_eco, bioImpact_coumputed_eco)

    def test_biodiversity_calc_with_no_product(self):
        # landOccupation example
        with open(f"{fixtures_path}/biodiversityImpact/calculated_impact_assessment.jsonld") as jsonld_file:
            bio_cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/biodiversityImpact/site_with_ecoregion.jsonld") as jsonld_file:
            bio_site_ecoregion = json.load(jsonld_file)

        engine = Engine()
        self.model = BiodiversityImpactLandUseChaudhary2015()
        engine.import_data(bio_cycle[0], bio_site_ecoregion)
        self.representation_cycle_eco = engine.data.safe_representation
        self.bio_emissionsResourceUse_eco =\
            EngineList(engine.data.safe_representation['emissionsResourceUse']).to_edict()
        self.bioimpact_expected_eco = 1.3505108840515328e-13

        product = copy.deepcopy(self.representation_cycle_eco['product'])
        product['term'] = {'@id': product['@id']}
        condition = self.model.check(self.representation_cycle_eco, EngineDict(), self.bio_emissionsResourceUse_eco)
        bioImpact_coumputed_eco = self.model.calculate() if condition else self.bioimpact_expected_eco

        self.assertEqual(self.bioimpact_expected_eco, bioImpact_coumputed_eco)


if __name__ == '__main__':
    unittest.main()
