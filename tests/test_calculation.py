import unittest
import json
from .utils import fixtures_path
from hestia_earth.calculation import CalculationEngine

CONTEXT = 'https://www-staging.hestia.earth'


class Test_calculation(unittest.TestCase):
    def test_engine_run_old(self):
        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle_old = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site_old = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/calculated.jsonld", 'r') as f:
            expected_old = json.load(f)

        engine = CalculationEngine(CONTEXT)
        engine.import_cycle(cycle=cycle_old, site=site_old)
        engine.calculate_emissions()
        self.assertEqual(expected_old, engine.export_cycle())

    def test_engine_run_new(self):
        with open(f"{fixtures_path}/Vgzlsfi1bT_/Cycle/Vgzlsfi1bT_.jsonld") as jsonld_file:
            cycle_new = json.load(jsonld_file)

        with open(f"{fixtures_path}/Vgzlsfi1bT_/calculated.jsonld", 'r') as f:
            expected_new = json.load(f)

        engine = CalculationEngine(CONTEXT)
        engine.import_cycle(cycle=cycle_new)
        engine.calculate_emissions()
        self.assertEqual(expected_new, engine.export_cycle())

    def test_engine_run_gap_filled(self):
        with open(f"{fixtures_path}/WI5hWtsqlu/calculated.jsonld", 'r') as f:
            data = json.load(f)

        engine = CalculationEngine(CONTEXT)
        engine.import_cycle(cycle=data)
        engine.calculate_emissions()
        self.assertEqual(data, engine.export_cycle())

    def test_engine_no_site(self):
        with open(f"{fixtures_path}/Vgzlsfi1bT_/Cycle/Vgzlsfi1bT_.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/Vgzlsfi1bT_/calculated_no_site.jsonld", 'r') as f:
            expected_new = json.load(f)

        engine = CalculationEngine(CONTEXT)
        del(cycle['site'])
        engine.import_cycle(cycle=cycle)
        engine.calculate_emissions()
        del(expected_new['site'])
        self.assertEqual(expected_new, engine.export_cycle())


if __name__ == '__main__':
    unittest.main()
