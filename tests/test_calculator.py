import unittest
import json
from .utils import fixtures_path
from hestia_earth.calculation.calculator import calculate_emission


class Test_calculator(unittest.TestCase):

    def test_check_co2ToAirFuelCombustion(self):

        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            self.cycle = json.load(jsonld_file)

        expected = 109.97001499250376
        calculated = calculate_emission(self.cycle, 'co2ToAirUreaHydrolysis', 'co2ToAirLimeAndUreaIpcc2006')

        self.assertEqual(expected, sum(calculated['value']))

    def test_check_empty_cycle(self):

        expected = None
        calculated = calculate_emission({}, 'co2ToAirUreaHydrolysis', 'co2ToAirLimeAndUreaIpcc2006')

        self.assertEqual(expected, calculated)


if __name__ == '__main__':
    unittest.main()
