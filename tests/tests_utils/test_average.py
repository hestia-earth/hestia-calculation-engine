import unittest
from hestia_earth.calculation.utils import average, EngineDict


class Test_average(unittest.TestCase):
    def test_partial_empty_elements_average(self):

        a = [{}, EngineDict(), 9, 10]
        b = average(a)

        self.assertEqual(9.5, b)

    def test_full_empty_elements_average(self):

        a = [{}, EngineDict(), {}, EngineDict()]
        b = average(a)

        self.assertEqual({}, b)

    def test_empty_average(self):

        a = []
        b = average(a)

        self.assertEqual({}, b)

    def test_average(self):

        a = [1, 2, 3, 4, 5]
        b = average(a)

        self.assertEqual(3, b)


if __name__ == '__main__':
    unittest.main()
