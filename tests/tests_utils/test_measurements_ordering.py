import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.utils import most_recents, most_relevant_measurement, shallowest_measurement
from dateutil import parser


class Test_measurements(unittest.TestCase):
    def test_most_relevant_measurement_by_year(self):

        with open(f"{fixtures_path}/parts/soilPh.jsonld") as jsonld_file:
            measurements = json.load(jsonld_file)

        expected_measurement = 2010

        picked_measurement = most_relevant_measurement(measurements, '2011')

        self.assertEqual(expected_measurement, picked_measurement)

    def test_most_relevant_measurement_by_year_month(self):

        with open(f"{fixtures_path}/parts/soilPh.jsonld") as jsonld_file:
            measurements = json.load(jsonld_file)

        expected_measurement = 2001

        picked_measurement = most_relevant_measurement(measurements, '2001-10')

        self.assertEqual(expected_measurement, picked_measurement)

    def test_most_relevant_measurement_by_year_month_day(self):

        with open(f"{fixtures_path}/parts/soilPh.jsonld") as jsonld_file:
            measurements = json.load(jsonld_file)

        expected_measurement = 2030

        picked_measurement = most_relevant_measurement(measurements, '2030-01-07')

        self.assertEqual(expected_measurement, picked_measurement)

    def test_most_recents(self):

        with open(f"{fixtures_path}/parts/soilPh.jsonld") as jsonld_file:
            measurements = json.load(jsonld_file)

        with open(f"{fixtures_path}/parts/most_recent.jsonld") as jsonld_file:
            expected_measurements = json.load(jsonld_file)

        picked_measurements = most_recents(measurements, parser.isoparse('2011'))

        self.assertEqual(expected_measurements, picked_measurements)

    def test_shallowest_measurement(self):

        with open(f"{fixtures_path}/parts/soilPh.jsonld") as jsonld_file:
            measurements = json.load(jsonld_file)

        with open(f"{fixtures_path}/parts/shallowest.jsonld") as jsonld_file:
            expected_shallowest = json.load(jsonld_file)

        most_recent_measurement = most_recents(measurements, parser.isoparse('2011'))
        picked_shallowest = shallowest_measurement(most_recent_measurement)

        self.assertEqual(expected_shallowest, picked_shallowest)

    def test_empty_most_relevant_measurement_1(self):

        expected_measurement = {}

        picked_measurement = most_relevant_measurement([], '1990')

        self.assertEqual(expected_measurement, picked_measurement)

    def test_empty_most_relevant_measurement_2(self):

        expected_measurement = {}

        picked_measurement = most_relevant_measurement({}, {})

        self.assertEqual(expected_measurement, picked_measurement)

    def test_empty_most_relevant_measurement_3(self):

        with open(f"{fixtures_path}/parts/soilPh.jsonld") as jsonld_file:
            measurements = json.load(jsonld_file)

        expected_measurement = {}

        picked_measurement = most_relevant_measurement(measurements, {})

        self.assertEqual(expected_measurement, picked_measurement)

    def test_empty_shallowest_measurement(self):

        expected_measurement = []

        picked_measurement = shallowest_measurement([])

        self.assertEqual(expected_measurement, picked_measurement)

    def test_empty_most_recents(self):

        expected_measurement = []

        picked_measurement = most_recents([], '2050')

        self.assertEqual(expected_measurement, picked_measurement)


if __name__ == '__main__':
    unittest.main()
