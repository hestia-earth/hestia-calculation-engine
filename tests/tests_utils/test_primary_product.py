import json
import unittest
from hestia_earth.calculation.utils import EngineDict, primary_product
from tests.utils import fixtures_path


class Test_primary_product(unittest.TestCase):
    def test_empty_products(self):

        product = EngineDict()
        b = primary_product(product)

        self.assertEqual({}, b)

    def test_non_empty_products(self):

        with open(f"{fixtures_path}/parts/products.jsonld") as jsonld_file:
            products = json.load(jsonld_file)

        products = EngineDict({key: EngineDict(products[key]) for key in products})
        b = primary_product(products)

        self.assertEqual(products['wheatGrain'], b)

    def test_no_primary(self):

        with open(f"{fixtures_path}/parts/products.jsonld") as jsonld_file:
            products = json.load(jsonld_file)

        products = EngineDict({key: EngineDict(products[key]) for key in products})
        c = products['wheatGrain']
        del(c['primary'])
        b = primary_product(EngineDict({'wheatGrain': c}))

        self.assertEqual({}, b)

    def test_second_primary(self):

        with open(f"{fixtures_path}/parts/products.jsonld") as jsonld_file:
            products = json.load(jsonld_file)

        products = EngineDict({key: EngineDict(products[key]) for key in products})
        c = products['wheatGrain']
        products['test'] = c

        self.assertEqual(products['wheatGrain'], c)


if __name__ == '__main__':
    unittest.main()
