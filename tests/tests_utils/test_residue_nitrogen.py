import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.utils import residue_nitrogen, residue_nitrogen_aboveground,\
    residue_nitrogen_belowground, EngineDict, EngineList


class Test_residue_nitrogen(unittest.TestCase):
    def test_residue_nitrogen(self):

        with open(f"{fixtures_path}/parts/products.jsonld") as jsonld_file:
            products = json.load(jsonld_file)

        products = EngineDict(products)
        c = residue_nitrogen(products)

        self.assertEqual(14.451118221473681, c)

    def test_residue_nitrogen_only_aboveground(self):

        with open(f"{fixtures_path}/parts/products.jsonld") as jsonld_file:
            products = json.load(jsonld_file)

        products = EngineDict(products)
        del(products['belowGroundCropResidue'])
        c = residue_nitrogen(products)

        self.assertEqual(0.8445757894736851, c)

    def test_residue_nitrogen_only_belowground(self):

        with open(f"{fixtures_path}/parts/products.jsonld") as jsonld_file:
            products = json.load(jsonld_file)

        products = EngineDict(products)
        products['aboveGroundCropResidueTotal'] = EngineDict(products['aboveGroundCropResidueTotal'])
        del(products['aboveGroundCropResidueTotal'])
        c = residue_nitrogen(products)

        self.assertEqual(13.606542431999996, c)

    def test_residue_nitrogen_nitrogen_content(self):

        with open(f"{fixtures_path}/parts/products.jsonld") as jsonld_file:
            products = json.load(jsonld_file)

        products = EngineDict(products)
        products['aboveGroundCropResidueTotal'] = EngineDict(products['aboveGroundCropResidueTotal'])
        properties = EngineList(products['aboveGroundCropResidueTotal']['properties'])
        products['aboveGroundCropResidueTotal']['properties'] = properties.to_edict()
        del(products['aboveGroundCropResidueLeftOnField'])
        del(products['belowGroundCropResidue'])
        c = residue_nitrogen(products)

        self.assertEqual({}, c)

    def test_empty_residue_nitrogen(self):

        products = EngineDict()
        c = residue_nitrogen(products)

        self.assertEqual({}, c)

    def test_empty_residue_nitrogen_aboveground(self):

        products = EngineDict()
        c = residue_nitrogen_aboveground(products)

        self.assertEqual({}, c)

    def test_empty_residue_nitrogen_belowground(self):

        products = EngineDict()
        c = residue_nitrogen_belowground(products)

        self.assertEqual({}, c)


if __name__ == '__main__':
    unittest.main()
