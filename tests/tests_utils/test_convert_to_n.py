import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.utils import convert_to_n


class Test_convert_to_n(unittest.TestCase):
    def __init__(self, *args, **kwargs):

        super(Test_convert_to_n, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/parts/inputs.jsonld") as jsonld_file:
            self.inputs = json.load(jsonld_file)

    def test_no_defaultProperties(self):

        test_input = self.inputs[0]
        del(test_input['term']['defaultProperties'])
        nitrogen_computed = convert_to_n(test_input)
        self.assertEqual(2.0369506619263, nitrogen_computed)

    def test_no_Properties(self):

        test_input = self.inputs[0]
        del(test_input['properties'])
        nitrogen_computed = convert_to_n(test_input)
        self.assertEqual(2.0369506619263, nitrogen_computed)

    def test_nitrogenContent(self):

        test_input = self.inputs[0]
        del(test_input['properties'])
        del(test_input['term']['defaultProperties'])
        test_input['properties'] = {'nitrogenContent': {'value': 0.529724771}}
        nitrogen_computed = convert_to_n(test_input)
        self.assertEqual(2.0369506619263, nitrogen_computed)

    def test_no_nitrogenContent(self):

        test_input = self.inputs[0]
        del(test_input['properties'][0])
        del(test_input['term']['defaultProperties'][0])
        nitrogen_computed = convert_to_n(test_input)
        self.assertEqual(2.0369506619263, nitrogen_computed)

    def test_no_id(self):

        test_input = self.inputs[0]
        test_input['term']['@id'] = 'test'
        nitrogen_computed = convert_to_n(test_input)
        self.assertEqual({}, nitrogen_computed)


if __name__ == '__main__':
    unittest.main()
