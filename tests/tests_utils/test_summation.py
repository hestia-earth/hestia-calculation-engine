import unittest
from hestia_earth.calculation.utils import summation, EngineDict


class Test_summation(unittest.TestCase):
    def test_partial_empty_elements_summation(self):

        a = [{}, EngineDict(), 9, 10]
        b = summation(a)

        self.assertEqual(19, b)

    def test_full_empty_elements_summation(self):

        a = [{}, EngineDict(), {}, EngineDict()]
        b = summation(a)

        self.assertEqual({}, b)

    def test_empty_summation(self):

        a = []
        b = summation(a)

        self.assertEqual({}, b)

    def test_summation(self):

        a = [1, 2, 3, 4, 5]
        b = summation(a)

        self.assertEqual(15, b)


if __name__ == '__main__':
    unittest.main()
