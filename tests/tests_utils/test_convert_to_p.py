import unittest
import json
from tests.utils import fixtures_path
from hestia_earth.calculation.utils import convert_to_p


class Test_convert_to_n(unittest.TestCase):
    def __init__(self, *args, **kwargs):

        super(Test_convert_to_n, self).__init__(*args, **kwargs)

        with open(f"{fixtures_path}/parts/inputs.jsonld") as jsonld_file:
            self.inputs = json.load(jsonld_file)

    def test_no_defaultProperties(self):

        test_input = self.inputs[0]
        del(test_input['term']['defaultProperties'])
        phos_computed = convert_to_p(test_input)
        self.assertEqual(1.0572998350093998, phos_computed)

    def test_no_Properties(self):

        test_input = self.inputs[0]
        del(test_input['properties'])
        phos_computed = convert_to_p(test_input)
        self.assertEqual(1.0572998350093998, phos_computed)

    def test_phosphateContentAsP2O5(self):

        test_input = self.inputs[0]
        del(test_input['properties'])
        del(test_input['term']['defaultProperties'])
        test_input['properties'] = {'phosphateContentAsP2O5': {'value': 0.274958998}}
        phos_computed = convert_to_p(test_input)
        self.assertEqual(1.0572998350093998, phos_computed)

    def test_no_phosphateContentAsP2O5(self):

        test_input = self.inputs[0]
        del(test_input['properties'][0])
        del(test_input['term']['defaultProperties'][0])
        phos_computed = convert_to_p(test_input)
        self.assertEqual(1.0572998350093998, phos_computed)

    def test_no_id(self):

        test_input = self.inputs[0]
        test_input['term']['@id'] = 'test'
        phos_computed = convert_to_p(test_input)
        self.assertEqual({}, phos_computed)

    def test_no_kg_p2o5(self):

        test_input = self.inputs[1]
        test_input['term']['@id'] = 'test'
        phos_computed = convert_to_p(test_input)
        self.assertEqual(384.53, phos_computed)


if __name__ == '__main__':
    unittest.main()
