import unittest
import json
from .utils import fixtures_path
from hestia_earth.calculation import CalculationEngine

CONTEXT = 'https://www-staging.hestia.earth'


class Test_assessment(unittest.TestCase):
    def test_assessment_Dalal(self):
        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/calculated_impact_assessment.jsonld", 'r') as f:
            expected_assessment = json.load(f)

        engine = CalculationEngine(CONTEXT)
        engine.import_data(cycle=cycle, site=site)
        engine.run()
        recalculated_cycle = engine.export_data()

        engine.import_data(cycle=recalculated_cycle, site=site, assessments=[])
        engine.run()

        if expected_assessment != []:
            for i in range(len(expected_assessment)):
                hid = expected_assessment[i]['product']['@id']
                engine.data.representation_assessment[hid]['endDate'] = expected_assessment[i]['endDate']

        self.assertEqual(expected_assessment, engine.export_data())

    def test_no_assessment_Dalal(self):
        with open(f"{fixtures_path}/WI5hWtsqlu/Cycle/Dalal_cycle.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/Site/Dalal_site.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        with open(f"{fixtures_path}/WI5hWtsqlu/calculated.jsonld", 'r') as f:
            cycle_calculated = json.load(f)

        engine = CalculationEngine(CONTEXT)
        engine.import_data(cycle=cycle, site=site)
        engine.run()
        del(cycle_calculated['site'])
        self.assertEqual(cycle_calculated, engine.export_data())

    def test_assessment_Sah(self):
        with open(f"{fixtures_path}/Vgzlsfi1bT_/Cycle/Vgzlsfi1bT_.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/Vgzlsfi1bT_/Site/8-gSWoPHEQx.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        with open(f"{fixtures_path}/Vgzlsfi1bT_/calculated_impact_assessment.jsonld", 'r') as f:
            expected_assessment = json.load(f)

        engine = CalculationEngine(CONTEXT)
        engine.import_data(cycle=cycle, site=site)
        engine.run()
        recalculated_cycle = engine.export_data()

        engine.import_data(cycle=recalculated_cycle, site=site, assessments=[])
        engine.run()

        if expected_assessment != []:
            for i in range(len(expected_assessment)):
                hid = expected_assessment[i]['product']['@id']
                engine.data.representation_assessment[hid]['endDate'] = expected_assessment[i]['endDate']

        self.assertEqual(expected_assessment, engine.export_data())

    def test_assessment_uploaded(self):
        with open(f"{fixtures_path}/Vgzlsfi1bT_/Cycle/Vgzlsfi1bT_.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/Vgzlsfi1bT_/Site/8-gSWoPHEQx.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        with open(f"{fixtures_path}/Vgzlsfi1bT_/Assessment/tKWwae_k21i.jsonld") as jsonld_file:
            assessments = json.load(jsonld_file)

        with open(f"{fixtures_path}/Vgzlsfi1bT_/calculated_impact_assessment_uploaded.jsonld", 'r') as f:
            expected_assessment = json.load(f)

        engine = CalculationEngine(CONTEXT)
        engine.import_data(cycle=cycle, site=site)
        engine.run()
        recalculated_cycle = engine.export_data()

        engine.import_data(cycle=recalculated_cycle, site=site, assessments=assessments)
        engine.run()

        if expected_assessment != []:
            for i in range(len(expected_assessment)):
                hid = expected_assessment[i]['product']['@id']
                engine.data.representation_assessment[hid]['endDate'] = expected_assessment[i]['endDate']

        self.assertEqual(expected_assessment, engine.export_data())

    def test_assessment_no_cycle(self):
        with open(f"{fixtures_path}/assessments_without_cycle/assessments.jsonld") as jsonld_file:
            assessment = json.load(jsonld_file)

        with open(f"{fixtures_path}/assessments_without_cycle/calculated_impact_assessment.jsonld") as jsonld_file:
            expected_assessments = json.load(jsonld_file)

        engine = CalculationEngine(CONTEXT)
        engine.import_data(assessments=assessment)
        engine.run()
        recalculated_assessments = engine.export_data()

        self.assertEqual(expected_assessments, recalculated_assessments)

    def test_assessment_selective(self):

        with open(f"{fixtures_path}/Vgzlsfi1bT_/Cycle/Vgzlsfi1bT_.jsonld") as jsonld_file:
            cycle = json.load(jsonld_file)

        with open(f"{fixtures_path}/Vgzlsfi1bT_/Site/8-gSWoPHEQx.jsonld") as jsonld_file:
            site = json.load(jsonld_file)

        with open(f"{fixtures_path}/Vgzlsfi1bT_/calculated_impact_assessment.jsonld", 'r') as f:
            expected_assessment = json.load(f)

        assessment = expected_assessment[0]
        expected_assessment = expected_assessment[0]

        engine = CalculationEngine(CONTEXT)
        engine.import_data(cycle=cycle, site=site)
        engine.run()
        recalculated_cycle = engine.export_data()

        engine.import_data(cycle=recalculated_cycle, site=site, assessments=[assessment])
        engine.run()
        self.assertEqual([expected_assessment], engine.export_data())

    def test_assessment_empty(self):

        engine = CalculationEngine(CONTEXT)
        engine.import_data(assessments=[])
        engine.run()
        recalculated_assessments = engine.export_data()

        self.assertEqual([], recalculated_assessments)


if __name__ == '__main__':
    unittest.main()
