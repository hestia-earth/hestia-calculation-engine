#!/bin/sh
API_URL=$1

docker build \
  -t hestia-calculation-engine:test \
  -f tests/Dockerfile \
  --build-arg API_URL=$API_URL \
  .
docker run --rm \
  --env-file .env \
  -v ${PWD}/coverage:/app/coverage \
  hestia-calculation-engine:test
