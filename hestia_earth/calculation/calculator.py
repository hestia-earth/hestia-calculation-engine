import importlib
import sys
import inspect
from hestia_earth.schema import EmissionJSONLD, SchemaType
from hestia_earth.utils.api import download_hestia
from .version import VERSION
from .representation import DataStore


def load(model):
    module = importlib.import_module(f"hestia_earth.calculation.emissions.{model}")
    for name, obj in inspect.getmembers(sys.modules[module.__name__]):
        if inspect.isclass(obj) and model.lower() == name.lower():
            return obj()


def build_emission(model, result, tier):

    term = download_hestia(model)
    emission = EmissionJSONLD()
    emission.fields['@type'] = SchemaType.EMISSION.value
    emission.fields['term'] = dict(term)
    emission.fields['description'] = term['description'] if term.get('description') else None
    emission.fields['value'] = [result]
    emission.fields['methodTier'] = tier
    emission.fields['recalculated'] = ['value']
    emission.fields['recalculatedVersion'] = [VERSION]

    return dict(emission.to_dict())


def calculate_emission(cycle, emission, modelstr):

    check = f"check_{emission}"
    calculate = f"calculate_{emission}"
    model = load(modelstr)
    data = DataStore()
    data.import_cycle(cycle, None)
    if getattr(model, check)(data.representation_cycle):
        result = getattr(model, calculate)()
        return build_emission(modelstr, result, getattr(model, 'tier'))
    else:
        return None
