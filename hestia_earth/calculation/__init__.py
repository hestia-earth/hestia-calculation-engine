import concurrent.futures
from hestia_earth.calculation.abstract_engine import Engine
from hestia_earth.calculation.utils import instantiate_hierarchy
from hestia_earth.calculation.emissions import EMISSIONS_HIERARCHIES_ORDER
from hestia_earth.calculation.abstract_hierarchy import Hierarchy
from hestia_earth.calculation.assessment_hierarchies import Assessments_hierarchy


class CalculationEngine(Engine):
    """Engine class. This is the class that runs all the models for all emissions and impacts."""
    def __init__(self, baseUrl) -> object:
        """Constructor method.
            Arguments:
                None
            Returns instance."""

        super().__init__()

        Hierarchy.context = baseUrl

        self.assessment = Assessments_hierarchy()

        with concurrent.futures.ThreadPoolExecutor() as executor:
            self.hierarchies = list(executor.map(instantiate_hierarchy, EMISSIONS_HIERARCHIES_ORDER))
