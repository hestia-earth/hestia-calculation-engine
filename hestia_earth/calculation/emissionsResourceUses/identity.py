from hestia_earth.calculation.abstract_model import Model


class Identity(Model):
    def __init__(self):

        super().__init__()

        # Define model requirements
        self.emissions = None
        self.econ = None
        self.product_amount = None

    def calculate(self):
        # Calculate emissionsResourceUse

        emissionsResourceUses = {}
        for emission in self.emissions:
            if self.product_amount > 0:
                emissionsResourceUses[emission['term']['@id']] = \
                    sum(emission['value']) * self.econ / self.product_amount

        return emissionsResourceUses

    def check(self, cycle, product):
        # Check requirements

        self.emissions = cycle['emissions'].evalues()
        self.econ = product['economicValueShare'] / 100 if product['economicValueShare'] != {} else 1
        self.product_amount = sum(product['value'])

        return len(self.emissions) > 0 and self.econ != {} and self.product_amount > 0
