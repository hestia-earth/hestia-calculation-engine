from hestia_earth.calculation.abstract_model import Model
from hestia_earth.calculation.utils import summation

MODEL_KEY = 'landOccupation'


class LandOccupation(Model):
    def __init__(self):

        super().__init__()

        # Define model requirements
        self.cycleDuration = None
        self.fallowCorrection = None
        self.functionalUnitMeasure = None
        self.product_amount = None
        self.economicValueShare = None

        # Instantiate variables
        self.lan_use = None

    def calculate(self):
        # calculate amount of land per kg of product for temporary and permanent crops
        # landOccupation model = (10000 * (cycleDuration/365) * fallowCorrection * economicValueShare)/product_amount

        # 1) Account for crop duration (for example multiple crops on a given field in a given year)
        # TODO: Add example fixtures with and without cycleDuration to test this
        land_use_1 = 10000 * (float(self.cycleDuration) / 365) if self.cycleDuration is not None else 10000

        # 2) Account for fallow period in crop production
        # TODO: Add example fixtures with and without fallowCorrection to test this
        land_use_2 = (land_use_1 * self.fallowCorrection) if self.fallowCorrection != {} else land_use_1

        # 3) reduce the impact by economic value share
        land_use_3 = (land_use_2 * (self.economicValueShare/100)) if self.economicValueShare is not None else land_use_2

        # 4) divide by product value to estimate land occupation (use) per kg.
        land_use_4 = (land_use_3 / self.product_amount) if self.product_amount is not None else None

        self.lan_use = land_use_4

        # create emissions resource use node
        emissionsResourceUses = {}
        emissionsResourceUses[MODEL_KEY] = self.lan_use
        return emissionsResourceUses

    def check(self, cycle, product):
        # set cycle level variables
        self.cycleDuration = cycle["cycleDuration"]
        self.fallowCorrection = cycle['site']['measurements']['fallowCorrection']['value'][0]
        self.functionalUnitMeasure = cycle['functionalUnitMeasure']
        self.product_amount = summation(product['value']) if 'value' in product else {}
        self.economicValueShare = product['economicValueShare'] if 'economicValueShare' in product else {}

        return self.product_amount != {} and self.product_amount > 0 and self.economicValueShare != {} and\
            self.economicValueShare > 0 and self.functionalUnitMeasure == '1 ha'
