from hestia_earth.calculation.abstract_model import Model
from hestia_earth.utils.lookup import get_table_value, download_lookup
from hestia_earth.calculation.data.lookups import load_lookup


class LandTransformation20YearAverage(Model):
    def __init__(self):

        super().__init__()

        # Define model requirements
        self.country = None
        self.prod_id = None

        # Instantiate variables
        self.lan_use_change = None

        # look up tables
        self.crop_lookup = download_lookup('crop.csv')

        self.look_up_biodiversity_country_LUC =\
            load_lookup('Biodiversity_LandUseChange_PercentPerYear_BlonkConsultants2014.csv')

    def calculate(self):

        # Identify the broad land use category (cropGrouping) based on crop type
        # TODO: Add a catch to break if greater than 1 land use type here
        LU_type =\
            get_table_value(self.crop_lookup, 'termid', self.prod_id, 'cropgroupingfao').replace(" ", "_").lower()

        lut_1 = LU_type.replace("_", "")  # reformat to match the column header in the LUC lookup

        # Estimate the amount of land transformed for the given product
        # Use LU_type rather than product if we have no data for the country-product combination
        luc_column_names = self.look_up_biodiversity_country_LUC.dtype.names
        if self.prod_id.lower() in luc_column_names:
            LandUseChange_percent = float(get_table_value(self.look_up_biodiversity_country_LUC, 'termid',
                                                          self.country, self.prod_id.lower()))
        else:
            LandUseChange_percent = float(get_table_value(self.look_up_biodiversity_country_LUC, 'termid',
                                                          self.country, lut_1))

        self.lan_use_change = LandUseChange_percent/100

        # create emissions resource use node
        emissionsResourceUses = {'landTransformation20YearAverage': self.lan_use_change}

        return emissionsResourceUses

    def check(self, cycle, product):

        self.country = cycle['site']['country']['@id'] if cycle != {} else {}
        self.prod_id = product['term']['@id'] if cycle != {} else {}

        return self.prod_id != {} and self.country != {}
