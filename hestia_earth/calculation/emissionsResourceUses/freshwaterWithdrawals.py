from hestia_earth.calculation.abstract_model import Model
from hestia_earth.calculation.utils import summation

MODEL_KEY = 'freshwaterWithdrawals'


class FreshwaterWithdrawals(Model):
    def __init__(self):

        super().__init__()

        # Define model requirements
        self.irrigation = None
        self.pyield = None
        self.seed = None
        self.econ = None

        # Instantiate variables
        self.freshwaterWithdrawals = None

    def calculate(self):
        # Calculate water use
        emissionsResourceUse = {}
        self.freshwaterWithdrawals =\
            (self.irrigation/self.pyield) * (self.seed / (self.seed + self.pyield)) * self.econ/100

        emissionsResourceUse[MODEL_KEY] = self.freshwaterWithdrawals

        return emissionsResourceUse

    def complete(self, completeness):

        self.irrigation = 0 if self.irrigation == {} and completeness['water'] else self.irrigation
        self.seed = 0 if self.seed == {} and completeness['other'] else self.seed

    def check(self, cycle, product):
        # Calculate water use

        inputs = cycle['inputs'].evalues()
        self.irrigation = summation([sum(inp['value'])
                                    if 'units' in inp['term'] and inp['term']['termType'] == 'water'
                                    else 0 for inp in inputs])
        self.pyield = summation(product['value'])
        self.seed = summation(cycle['inputs']['seed']['value'])
        self.econ = product['economicValueShare']

        self.complete(cycle['dataCompleteness'])

        return self.irrigation != {} and self.pyield != {} and self.pyield > 0 and self.seed != {} and self.econ != {}
