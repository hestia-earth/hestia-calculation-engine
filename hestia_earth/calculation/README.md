# Hestia Calculation Engine

You can find documentations for every model in:
* [Emissions](./emissions)
* [Emissions & Resource Use](./emissionsResourceUses)
* [Impacts](./impatcs)

## Usage Examples

### Recalculating a Cycle

If only a cycle is provided it only recalculates the cycle,

```python
cycle = {"@type": "Cycle", ...}
engine.import_data(cycle=cycle)
engine.run()
data = engine.export_data()
# data = {"@type": "Cycle", ...}  (recalculated cycle)
```
and the exported object is the recalculated cycle.
In this case there are no assessments calculated.

### Recalculating a Cycle with a Site

If a cycle and an a site are provided it recalculates everything with the data of the cycle and the site,

```python
cycle = {"@type": "Cycle", ...}
site = {"@type": "Site", ...}
engine.import_data(cycle=cycle, site=site)
engine.run()
data = engine.export_data()
# data = {"@type": "Cycle", ...}  (recalculated cycle)
```
and the exported object is again a recalculated cycle.
In this case there are no assessments calculated.

### Recalculating ImpactAssessment

If only assessment is provided then the engine recalculates the assessments with the cycle specified inside every assessment.

```python
assessment = [{"@type": "ImpactAssessment", "name": "Assessment_1" ...}, {"@type": "ImpactAssessment", "name": "Assessment_2" ...}, ...]
engine.import_data(assessment=assessment)
engine.run()
data = engine.export_data()
# data = [{"@type": "ImpactAssessment", "name": "Assessment_1" ...}, {"@type": "ImpactAssessment", "name": "Assessment_2" ...}, {"@type": "ImpactAssessment", "name": "Assessment_3", ...] (recalculated and updated assessments)
```

The engine then does the calculations and updates the assessments for the values that are different if they exist or creating new assessments if they do not exist.
The exported object is a list with the recalculated assessments.

### Recalculating ImpactAssessment with Cycle and Site

If the three arguments are provided (cycle, site, and assessment) then the calculations are done with the cycle and site especifically indicated and not taking into account the one inside every assessment.

```python
cycle = {"@type": "Cycle", ...}
site = {"@type": "Site", ...}
assessment = [{"@type": "ImpactAssessment", "name": "Assessment_1" ...}, {"@type": "ImpactAssessment", "name": "Assessment_2" ...}, ...]
engine.import_data(cycle=cycle, site=site, assessment=assessment)
engine.run()
data = engine.export_data()
# data = [{"@type": "ImpactAssessment", "name": "Assessment_1" ...}, {"@type": "ImpactAssessment", "name": "Assessment_2" ...}, ...] (recalculated and updated assessments)
```

The engine then does the calculations and updates the assessments for the values that are different if they exist or creating new assessments if they do not exist.
The exported object is a list with the recalculated assessments.

### Data Flow

```mermaid
graph TD
    A(Cycle gap-filled) --> C[Calculate direct emissions]
    B(Site gap-filled) --> C[Calculate direct emissions]
    C --> D[Calculate indirect emissions]
    D --> G(Cycle recalculated)

    O(Site gap-filled) -->R
    P(Cycle recalculated) --> R[Calculate original]
    Q(Impact Assessment original) --> R[Calculate emissionsResourceUse]
    R --> S[Calculate impact indicators]
    S --> T(Impact Assessment recalculated)
```

### Logic to update the emissions

```mermaid
graph TD
B(Calculated emission)
    B --> C{Tier higher than original}
    C -->|No| D(Do not update cycle)
    C -->|Yes| E{Difference higher than 1%}
    E --> |Yes| F(Update cycle)
    E --> |No| G(Do not update cycle)
```
