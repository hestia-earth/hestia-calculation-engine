from hestia_earth.calculation.abstract_model import Model
from hestia_earth.calculation.data.constants.ch4 import DRY_MATTER_FACTOR_TO_CH4
from hestia_earth.utils.api import download_hestia
from hestia_earth.calculation.utils import summation

MODEL_KEY = 'ch4ToAirCropResidueBurningAkagiEtAl2011AndIpcc2006'


class CH4ToAirCropResidueBurningAkagiEtAl2011AndIpcc2006(Model):
    def __init__(self):
        # Define model
        self.tier = 1
        self.term = download_hestia(MODEL_KEY)

        # Define model requirements
        self.dry_matter = None

        # Instantiate variables
        self.ch4ToAirCropResidueBurning = None

    def calculate_ch4ToAirCropResidueBurning(self):
        # Calculate total CH4 emissions

        self.ch4ToAirCropResidueBurning = self.dry_matter * DRY_MATTER_FACTOR_TO_CH4

        return self.ch4ToAirCropResidueBurning

    def complete(self, completeness):

        self.dry_matter = 0 if self.dry_matter == {} and completeness['cropResidue'] else self.dry_matter

    def check_ch4ToAirCropResidueBurning(self, cycle):
        # Check that we have all the inputs

        self.dry_matter = summation(cycle['products']['aboveGroundCropResidueBurnt']['value'])

        self.complete(cycle['dataCompleteness'])

        return self.dry_matter != {}
