from hestia_earth.calculation.abstract_model import Model
from hestia_earth.utils.api import download_hestia
from hestia_earth.calculation.utils import residue_nitrogen_aboveground

MODEL_KEY = 'nh3ToAirCropResidueDecompositionDeRuijterEtAl2010'


class NH3ToAirCropResidueDecompositionDeRuijterEtAl2010(Model):
    def __init__(self):

        # Define model tier
        self.tier = 1
        self.term = download_hestia(MODEL_KEY)

        # Define model requirements
        self.res_nitrogen = None
        self.nitrogenContent = None
        self.a = None

        # Instantiate variables
        self.nh3ToAirCropResidueDecomposition = None

    def calculate_nh3ToAirCropResidueDecomposition(self):
        # Calculate total nh3 emissions

        self.a = max([(0.38 * 1000 * self.nitrogenContent/100 - 5.44) / 100, 0])

        NH3_N = self.a * self.res_nitrogen

        self.nh3ToAirCropResidueDecomposition = NH3_N * 1.21589

        return self.nh3ToAirCropResidueDecomposition

    def complete(self, completeness):

        self.res_nitrogen = 0 if self.res_nitrogen == {} and completeness['cropResidue'] else self.res_nitrogen
        self.nitrogenContent = 0 if self.nitrogenContent == {} and completeness['cropResidue'] else self.nitrogenContent

    def check_nh3ToAirCropResidueDecomposition(self, cycle):
        # Check that we have all the inputs

        self.res_nitrogen = residue_nitrogen_aboveground(cycle['products'])
        self.nitrogenContent = \
            cycle['products']['aboveGroundCropResidueTotal']['properties']['nitrogenContent']['value']

        self.complete(cycle['dataCompleteness'])

        return self.res_nitrogen != {} and self.nitrogenContent != {} and self.nitrogenContent <= 5
