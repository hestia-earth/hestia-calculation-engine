# N2O, to air, fertilizer and excreta, direct, Stehfest & Bouwman (2006)

This model calculates the direct N2O emissions due to the use of fertilizer using the regression model detailed in [Stehfest & Bouwman (2006)](https://link.springer.com/article/10.1007/s10705-006-9000-7). It takes data on factors including soil, climate, and crop type. Here we also extend it to crop residue and animal excreta deposited directly on pasture.

## Calculates

- [emission.value](https://hestia.earth/schema/Emission#value) as [n2OToAirInorganicFertilizerDirect](https://hestia.earth/term/n2OToAirInorganicFertilizerDirect)
- [emission.value](https://hestia.earth/schema/Emission#value) as [n2OToAirOrganicFertilizerDirect](https://hestia.earth/term/n2OToAirOrganicFertilizerDirect)
- [emission.value](https://hestia.earth/schema/Emission#value) as [n2OToAirCropResidueDecompositionDirect](https://hestia.earth/term/n2OToAirCropResidueDecompositionDirect)
- [emission.value](https://hestia.earth/schema/Emission#value) as [n2OToAirExcretaDirect](https://hestia.earth/term/n2OToAirExcretaDirect)

## Requirements

### n2OToAirInorganicFertilizerDirect

- [measurement.value](https://www.hestia.earth/schema/Measurement#value) WITH
[clayContent](https://hestia.earth/term/clayContent)
AND [sandContent](https://hestia.earth/term/sandContent)
AND [soilOrganicCarbonContent](https://hestia.earth/term/soilOrganicCarbonContent)
AND [soilPh](https://hestia.earth/term/soilPh)
AND [ecoClimateZone](https://hestia.earth/term/ecoClimateZone)

- [input.value](https://www.hestia.earth/schema/Input#value) WITH
[kg N](https://hestia.earth/schema/Term#units) AND [inorganicFertilizer](https://hestia.earth/schema/Term#termType)

### n2OToAirOrganicFertilizerDirect

- [measurement.value](https://www.hestia.earth/schema/Measurement#value) WITH
[clayContent](https://hestia.earth/term/clayContent)
AND [sandContent](https://hestia.earth/term/sandContent)
AND [soilOrganicCarbonContent](https://hestia.earth/term/soilOrganicCarbonContent)
AND [soilPh](https://hestia.earth/term/soilPh)
AND [ecoClimateZone](https://hestia.earth/term/ecoClimateZone)

- [input.value](https://www.hestia.earth/schema/Input#term) WITH
[kg N](https://hestia.earth/schema/Term#units) AND [organicFertilizer](https://hestia.earth/schema/Term#termType)


### n2OToAirCropResidueDecompositionDirect

- [measurement.value](https://www.hestia.earth/schema/Measurement#value) WITH
[clayContent](https://hestia.earth/term/clayContent)
AND [sandContent](https://hestia.earth/term/sandContent)
AND [soilOrganicCarbonContent](https://hestia.earth/term/soilOrganicCarbonContent)
AND [soilPh](https://hestia.earth/term/soilPh)
AND [ecoClimateZone](https://hestia.earth/term/ecoClimateZone)

- [product.value](https://www.hestia.earth/schema/Product#value) WITH\
  [aboveGroundCropResidueLeftOnField](https://hestia.earth/term/aboveGroundCropResidueLeftOnField) AND
  [aboveGroundCropResidueIncorporated](https://hestia.earth/term/aboveGroundCropResidueIncorporated)
  or\
  [aboveGroundCropResidueTotal](https://hestia.earth/term/aboveGroundCropResidueTotal) AND
  [aboveGroundCropResidueBurnt](https://hestia.earth/term/aboveGroundCropResidueBurnt) AND
  [aboveGroundCropResidueRemoved](https://hestia.earth/term/aboveGroundCropResidueRemoved)

### n2OToAirExcretaDirect

- [measurement.value](https://www.hestia.earth/schema/Measurement#value) WITH
[clayContent](https://hestia.earth/term/clayContent)
AND [sandContent](https://hestia.earth/term/sandContent)
AND [soilOrganicCarbonContent](https://hestia.earth/term/soilOrganicCarbonContent)
AND [soilPh](https://hestia.earth/term/soilPh)
AND [ecoClimateZone](https://hestia.earth/term/ecoClimateZone)

- [Product.value](https://www.hestia.earth/schema/Product#term) WITH
[kg N](https://hestia.earth/schema/Term#units) AND [animalProduct](https://hestia.earth/schema/Term#termType)


## Implementation Status

- `Implemented`
- [Tested](../../../tests/tests_emissions/test_n2OToAirFertilizerAndExcretadirectStehfestBouwman2006.py)
