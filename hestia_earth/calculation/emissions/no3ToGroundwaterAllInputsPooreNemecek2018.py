import numpy as np
from hestia_earth.calculation.abstract_model import Model
from hestia_earth.calculation.utils import convert_to_n, most_relevant_measurement, primary_product, residue_nitrogen,\
    summation, average
from hestia_earth.calculation.data.constants.no3 import NO3_LEECHING_FACTORS
from hestia_earth.calculation.data.constants.generic import ATOMIC_WEIGHT_CONVERSIONS
from hestia_earth.utils.api import download_hestia

MODEL_KEY = 'no3ToGroundwaterAllInputsPooreNemecek2018'


class NO3ToGroundwaterAllInputsPooreNemecek2018(Model):
    def __init__(self):

        # Define model tier
        self.tier = 2
        self.term = download_hestia(MODEL_KEY)

        # Define model requirements
        self.rootingDepth = None
        self.clay = None
        self.sand = None
        self.precipitation = None
        self.inorgN_total = None
        self.orgN_total = None
        self.excretaN_total = None
        self.res_nitrogen = None
        self.N_total = None

        # Instantiate variables
        self.no3ToGroundwaterAllOrigins = None
        self.no3ToGroundwaterOrganicFertilizer = None
        self.no3ToGroundwaterInorganicFertilizer = None
        self.no3ToGroundwaterExcreta = None
        self.no3ToGroundwaterCropResidueDecomposition = None

        # Define model coefficients
        self.leaching = NO3_LEECHING_FACTORS
        self.conv_no3n_no3 = ATOMIC_WEIGHT_CONVERSIONS['Conv_Mol_NO3N_NO3']

    def calculate_no3ToGroundwaterInorganicFertilizer(self):

        self.no3ToGroundwaterInorganicFertilizer = \
            self.calculate_no3ToGroundwaterAllOrigins() * self.inorgN_total / self.N_total

        return self.no3ToGroundwaterInorganicFertilizer

    def calculate_no3ToGroundwaterOrganicFertilizer(self):

        self.no3ToGroundwaterOrganicFertilizer = \
            self.calculate_no3ToGroundwaterAllOrigins() * self.orgN_total / self.N_total

        return self.no3ToGroundwaterOrganicFertilizer

    def calculate_no3ToGroundwaterExcreta(self):

        self.no3ToGroundwaterExcreta = self.calculate_no3ToGroundwaterAllOrigins() * self.excretaN_total / self.N_total

        return self.no3ToGroundwaterExcreta

    def calculate_no3ToGroundwaterCropResidueDecomposition(self):

        self.no3ToGroundwaterCropResidueDecomposition = \
            self.calculate_no3ToGroundwaterAllOrigins() * self.res_nitrogen / self.N_total

        return self.no3ToGroundwaterCropResidueDecomposition

    def calculate_no3ToGroundwaterAllOrigins(self):

        self.no3ToGroundwaterAllOrigins = self.N_total * (0.035 if self.product == 'ricePlantsPaddy' or
                                                          self.product == 'riceGrainPaddy' else
                                                          self.get_leaching_factor()) * self.conv_no3n_no3

        return self.no3ToGroundwaterAllOrigins

    def get_total_n(self, cycle):

        inputs = cycle['inputs'].evalues()

        N_total_converted = [convert_to_n(input)
                             if 'units' in input['term'] and 'defaultProperties' in input['term'] and 'nitrogenContent'
                                in input['term']['defaultProperties'] and input['term']['units'] == 'kg'
                             else 0 for input in inputs]

        N_total_direct = [sum(input['value']) if 'units' in input['term'] and
                                                 input['term']['units'] == 'kg N' else 0 for input in inputs]

        return N_total_converted + N_total_direct

    def get_leaching_factor(self):
        if (self.rootingDepth > 1.3 or self.clay/100 > 0.50 or self.precipitation < 500) and\
                (self.rootingDepth > 0.4 or self.sand/100 < 0.85 or self.precipitation < 1300):
            return self.leaching['low']
        elif (self.rootingDepth < 0.4 or self.sand/100 > 0.85 or self.precipitation > 1300) and\
                (self.rootingDepth > 1.3 or self.clay/100 < 0.5 or self.precipitation > 500):
            return self.leaching['high']
        else:
            return self.leaching['other']

    def complete(self, completeness):

        self.inorgN_total = 0 if self.inorgN_total == {} and completeness['fertilizer'] else self.inorgN_total
        self.orgN_total = 0 if self.orgN_total == {} and completeness['fertilizer'] else self.orgN_total
        self.excretaN_total = 0 if self.excretaN_total == {} and completeness['products'] else self.excretaN_total
        self.res_nitrogen = 0 if self.res_nitrogen == {} and completeness['cropResidue'] else self.res_nitrogen
        self.N_total = 0 if self.N_total == {} and completeness['fertilizer'] and completeness['products'] and\
            completeness['cropResidue'] else self.N_total
        self.sand = self.sand if self.sand != {} else np.nan
        self.clay = self.clay if self.clay != {} else np.nan
        self.precipitation = self.precipitation if self.precipitation != {} else np.nan

    def check_no3ToGroundwaterInorganicFertilizer(self, cycle):

        inputs = cycle['inputs'].evalues()

        self.inorgN_total = summation([sum(input['value']) if 'units' in input['term'] and
                                                              input['term']['units'] == 'kg N' and
                                                              input['term']['termType'] == 'inorganicFertilizer'
                                       else {} for input in inputs])

        self.complete(cycle['dataCompleteness'])

        return self.inorgN_total != {} and self.check_no3ToGroundwaterAllOrigins(cycle) and self.N_total != 0

    def check_no3ToGroundwaterOrganicFertilizer(self, cycle):

        inputs = cycle['inputs'].evalues()

        N_total_converted = [convert_to_n(input)
                             if 'units' in input['term'] and
                                input['term']['units'] == 'kg' and
                                input['term']['termType'] == 'organicFertilizer'
                             else {} for input in inputs]

        N_total_direct = [sum(input['value']) if 'units' in input['term'] and input['term']['units'] == 'kg N' and
                                                 input['term']['termType'] == 'organicFertilizer' else {}
                          for input in inputs]

        self.orgN_total = summation(N_total_converted + N_total_direct)

        self.complete(cycle['dataCompleteness'])

        return self.orgN_total != {} and self.check_no3ToGroundwaterAllOrigins(cycle) and self.N_total != 0

    def check_no3ToGroundwaterExcreta(self, cycle):

        products = cycle['products'].evalues()

        self.excretaN_total = summation([sum(product['value']) if 'units' in product['term'] and
                                                                  product['term']['units'] == 'kg N' and
                                                                  product['term']['termType'] == 'animalProduct' else {}
                                         for product in products])

        self.complete(cycle['dataCompleteness'])

        return self.excretaN_total != {} and self.check_no3ToGroundwaterAllOrigins(cycle) and self.N_total != 0

    def check_no3ToGroundwaterCropResidueDecomposition(self, cycle):

        self.res_nitrogen = residue_nitrogen(cycle['products'])

        self.complete(cycle['dataCompleteness'])

        return self.res_nitrogen != {} and self.check_no3ToGroundwaterAllOrigins(cycle) and self.N_total != 0

    def check_no3ToGroundwaterAllOrigins(self, cycle):

        self.rootingDepth = average([float(product['properties']['rootingDepth']['value'])
                                     if product['properties']['rootingDepth']['value'] != {} else {}
                                     for product in cycle['products'].evalues()])
        self.clay = most_relevant_measurement(cycle['site']['measurements']['clayContent'], cycle['endDate'])
        self.sand = most_relevant_measurement(cycle['site']['measurements']['sandContent'], cycle['endDate'])

        cycle_rainfall = most_relevant_measurement(cycle['site']['measurements']['rainfallAnnual'],
                                                   cycle['endDate'])
        annualrainfall = most_relevant_measurement(cycle['site']['measurements']['rainfallLongTermAnnualMean'],
                                                   cycle['endDate'])
        self.precipitation = cycle_rainfall if cycle_rainfall != {} else annualrainfall if annualrainfall != {} else {}

        product = primary_product(cycle['products']) if cycle['products'] != {} else {}
        self.product = product['term']['@id'] if product != {} else {}

        self.N_total = summation(self.get_total_n(cycle) + [residue_nitrogen(cycle['products'])])

        self.complete(cycle['dataCompleteness'])

        return self.N_total != {} and self.rootingDepth != {}
