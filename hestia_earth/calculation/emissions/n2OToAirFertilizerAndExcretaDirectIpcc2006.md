# N2O, to air, fertilizer and excreta, direct, IPCC (2006)

This model calculates the direct N2O emissions from organic and inorganic fertilizer and excreta deposited directly on pasture, using the Tier 1 methodology detailed in the [IPCC 2006 Guidelines (Volume 4, Chapter 11, Section 11.2)](https://www.ipcc-nggip.iges.or.jp/public/2006gl/pdf/4_Volume4/V4_02_Ch2_Generic.pdf).

## Calculates

TODO

## Requirements

TODO

## Implementation Status

- `Implemented`
- [Tested](../../../tests/tests_emissions/test_n2OToAirFertilizerAndExcretaDirectIpcc2006.py)
