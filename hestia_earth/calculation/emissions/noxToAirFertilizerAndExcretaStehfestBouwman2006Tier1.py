from hestia_earth.calculation.abstract_model import Model
from hestia_earth.utils.api import download_hestia
from hestia_earth.calculation.utils import convert_to_n, residue_nitrogen, summation, format_lookup

MODEL_KEY = 'noxToAirFertilizerAndExcretaStehfestBouwman2006Tier1'


class NOXToAirFertilizerAndExcretaStehfestBouwman2006Tier1(Model):
    def __init__(self):

        # Define model tier
        self.tier = 1
        self.term = download_hestia(MODEL_KEY)

        # Define model requirements
        self.N_total = None
        self.inorgN_total = None
        self.orgN_total = None
        self.excretaN_total = None
        self.res_nitrogen = None
        self.country = None

        # Instantiate variables
        self.noxToAirAllOrigins = None

        # Define model coeffients
        self.nox_conv_factors = format_lookup('region.csv', 'ef_nox')

    def calculate_noxToAirInorganicFertilizer(self):
        self.noxToAirInorganicFertilizer = \
            self.calculate_noxToAirAllOrigins() * self.inorgN_total / self.N_total

        return self.noxToAirInorganicFertilizer

    def calculate_noxToAirOrganicFertilizer(self):
        self.noxToAirOrganicFertilizer =\
            self.calculate_noxToAirAllOrigins() * self.orgN_total / self.N_total

        return self.noxToAirOrganicFertilizer

    def calculate_noxToAirExcreta(self):
        self.noxToAirExcreta =\
            self.calculate_noxToAirAllOrigins() * self.excretaN_total / self.N_total

        return self.noxToAirExcreta

    def calculate_noxToAirCropResidueDecomposition(self):
        self.noxToAirCropResidueDecomposition = \
            self.calculate_noxToAirAllOrigins() * self.res_nitrogen / self.N_total

        return self.noxToAirCropResidueDecomposition

    def calculate_noxToAirAllOrigins(self):
        # Calculate total NOX emission

        self.noxToAirAllOrigins = float(self.nox_conv_factors[self.country]) * self.N_total

        return self.noxToAirAllOrigins

    def complete(self, completeness):
        self.N_total = 0 if self.N_total == {} and (completeness['fertilizer'] or completeness['cropResidue'])\
            else self.N_total
        self.inorgN_total = 0 if self.inorgN_total == {} and completeness['fertilizer'] else self.inorgN_total
        self.orgN_total = 0 if self.orgN_total == {} and completeness['fertilizer'] else self.orgN_total
        self.excretaN_total = 0 if self.excretaN_total == {} and completeness['products'] else self.excretaN_total
        self.res_nitrogen = 0 if self.res_nitrogen == {} and completeness['cropResidue'] else self.res_nitrogen

    def check_noxToAirInorganicFertilizer(self, cycle):

        inputs = cycle['inputs'].evalues()

        self.inorgN_total = summation([sum(input['value']) if 'units' in input['term'] and
                                                              input['term']['units'] == 'kg N' and
                                                              input['term']['termType'] == 'inorganicFertilizer'
                                       else {} for input in inputs])

        self.complete(cycle['dataCompleteness'])

        return self.inorgN_total != {} and self.check_noxToAirAllOrigins(cycle) and self.N_total != 0

    def check_noxToAirOrganicFertilizer(self, cycle):

        inputs = cycle['inputs'].evalues()

        N_total_converted = [convert_to_n(input)
                             if 'units' in input['term'] and
                                input['term']['units'] == 'kg' and
                                input['term']['termType'] == 'organicFertilizer'
                             else {} for input in inputs]

        N_total_direct = [sum(input['value']) if 'units' in input['term'] and input['term']['units'] == 'kg N' and
                                                 input['term']['termType'] == 'organicFertilizer' else {}
                          for input in inputs]

        self.orgN_total = summation(N_total_converted + N_total_direct)

        self.complete(cycle['dataCompleteness'])

        return self.orgN_total != {} and self.check_noxToAirAllOrigins(cycle) and self.N_total != 0

    def check_noxToAirCropResidueDecomposition(self, cycle):

        self.res_nitrogen = residue_nitrogen(cycle['products'])

        self.complete(cycle['dataCompleteness'])

        return self.res_nitrogen != {} and self.check_noxToAirAllOrigins(cycle) and self.N_total != 0

    def check_noxToAirExcreta(self, cycle):

        products = cycle['products'].evalues()

        self.excretaN_total = summation([sum(product['value']) if 'units' in product['term'] and
                                                                  product['term']['units'] == 'kg N' and
                                                                  product['term']['termType'] == 'animalProduct' else {}
                                         for product in products])

        self.complete(cycle['dataCompleteness'])

        return self.excretaN_total != {} and self.check_noxToAirAllOrigins(cycle) and self.N_total != 0

    def check_noxToAirAllOrigins(self, cycle):
        # Check that we have all the inputs

        inputs = cycle['inputs'].evalues()
        n_inputs_converted = [convert_to_n(input)
                              if 'units' in input['term'] and 'defaultProperties' in input['term'] and 'nitrogenContent'
                                 in input['term']['defaultProperties'] and input['term']['units'] == 'kg'
                              else {} for input in inputs]

        n_inputs_direct = [sum(input['value']) if 'units' in input['term'] and
                                                  input['term']['units'] == 'kg N' else {} for input in inputs]
        n_outputs = residue_nitrogen(cycle['products'])

        self.N_total = summation(n_inputs_converted + n_inputs_direct + [n_outputs])

        self.country = cycle['site']['country']['@id']

        self.complete(cycle['dataCompleteness'])

        return self.N_total != {} and self.country != {} and self.country in self.nox_conv_factors
