# CO2, to air, lime and urea, IPCC (2006)

This model calculates the CO2 emissions due to the additions of liming materials and urea-containing fertiliser. It uses the Tier 1 factors from the [IPCC (2006, Volume 4, Chapter 11, Sections 11.3 and 11.4)](https://www.ipcc-nggip.iges.or.jp/public/2006gl/pdf/4_Volume4/V4_11_Ch11_N2O&CO2.pdf) guidelines.

## Calculates

- [emission.value](https://hestia.earth/schema/Emission#value) as [co2ToAirLimeHydrolysis](https://hestia.earth/term/co2ToAirLimeHydrolysis)
- [emission.value](https://hestia.earth/schema/Emission#value) as [co2ToAirUreaHydrolysis](https://hestia.earth/term/co2ToAirUreaHydrolysis)

## Requirements

### co2ToAirLimeHydrolysis

- [input.value](https://www.hestia.earth/schema/Input#value) WITH
[lime](https://hestia.earth/term/lime)
AND [aragonite](https://hestia.earth/term/aragonite)
AND [agriculturalLimestone](https://hestia.earth/term/agriculturalLimestone)
AND [hydratedLime](https://hestia.earth/term/hydratedLime)
AND [burntLime](https://hestia.earth/term/burntLime)
AND [dolomiticLime](https://hestia.earth/term/dolomiticLime)

### co2ToAirUreaHydrolysis

- [input.value](https://www.hestia.earth/schema/Input#value) WITH
[ureaAsN](https://hestia.earth/term/ureaAsN)
AND [ureaAmmoniumNitrateAsN](https://hestia.earth/term/ureaAmmoniumNitrateAsN)
AND [ureaAmmoniumSulphateAsN](https://hestia.earth/term/ureaAmmoniumSulphateAsN)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/tests_emissions/test_co2ToAirLimeAndUreaIpcc2006.py)
