# NOx, to air, fertilizer and excreta, Stehfest & Bouwman (2006)

This model calculates the direct NOx emissions due to the use of fertilizer using the regression model detailed in [Stehfest & Bouwman (2006)](https://link.springer.com/article/10.1007/s10705-006-9000-7). It takes data on factors including soil, climate, and crop type. Here we also extend it to crop residue and animal excreta deposited directly on pasture.

## Calculates

- [emission.value](https://hestia.earth/schema/Emission#value) as [noxToAirInorganicFertilizer](https://hestia.earth/term/noxToAirInorganicFertilizer)
- [emission.value](https://hestia.earth/schema/Emission#value) as [noxToAirOrganicFertilizer](https://hestia.earth/term/noxToAirOrganicFertilizer)
- [emission.value](https://hestia.earth/schema/Emission#value) as [noxToAirExcreta](https://hestia.earth/term/noxToAirExcreta)
- [emission.value](https://hestia.earth/schema/Emission#value) as [noxToAirCropResidueDecomposition](https://hestia.earth/term/noxToAirCropResidueDecomposition)

## Requirements

### noxToAirInorganicFertilizer

- [input.value](https://www.hestia.earth/schema/Input#value) WITH sum of inputs with [kg N](https://hestia.earth/schema/Term#units) units AND
  sum of inputs with [kg N](https://hestia.earth/schema/Term#units) units AND sum of inputs with [kg N](https://hestia.earth/schema/Term#units) units AND
  sum of inputs with [kg N](https://hestia.earth/schema/Term#units) and [inorganicFertilizer](https://hestia.earth/schema/Term#termType) termType

- [product](https://www.hestia.earth/schema/Input#term) WITH [name](https://hestia.earth/schema/Term#name)

- [product.value](https://www.hestia.earth/schema/Product#value) WITH\
  [aboveGroundCropResidueLeftOnField](https://hestia.earth/term/aboveGroundCropResidueLeftOnField) AND
  [aboveGroundCropResidueIncorporated](https://hestia.earth/term/aboveGroundCropResidueIncorporated) \
  OR\
  [aboveGroundCropResidueTotal](https://hestia.earth/term/aboveGroundCropResidueTotal) AND
  [aboveGroundCropResidueBurnt](https://hestia.earth/term/aboveGroundCropResidueBurnt) AND
  [aboveGroundCropResidueRemoved](https://hestia.earth/term/aboveGroundCropResidueRemoved)

- [measurement.value](https://www.hestia.earth/schema/Measurement#value) WITH
[ecoClimateZone](https://hestia.earth/term/ecoClimateZone)
AND [soilTotalNitrogenContent](https://hestia.earth/term/soilTotalNitrogenContent)

### noxToAirOrganicFertilizer

- [input.value](https://www.hestia.earth/schema/Input#value) WITH sum of inputs with [kg N](https://hestia.earth/schema/Term#units) units AND
  sum of inputs with [kg N](https://hestia.earth/schema/Term#units) units and [organicFertilizer](https://hestia.earth/schema/Term#termType) termType

- [product](https://www.hestia.earth/schema/Input#term) WITH [name](https://hestia.earth/schema/Term#name)

- [product.value](https://www.hestia.earth/schema/Product#value) WITH\
  [aboveGroundCropResidueLeftOnField](https://hestia.earth/term/aboveGroundCropResidueLeftOnField) AND
  [aboveGroundCropResidueIncorporated](https://hestia.earth/term/aboveGroundCropResidueIncorporated) \
  OR\
  [aboveGroundCropResidueTotal](https://hestia.earth/term/aboveGroundCropResidueTotal) AND
  [aboveGroundCropResidueBurnt](https://hestia.earth/term/aboveGroundCropResidueBurnt) AND
  [aboveGroundCropResidueRemoved](https://hestia.earth/term/aboveGroundCropResidueRemoved)

- [measurement.value](https://www.hestia.earth/schema/Measurement#value) WITH
[ecoClimateZone](https://hestia.earth/term/ecoClimateZone)
AND [soilTotalNitrogenContent](https://hestia.earth/term/soilTotalNitrogenContent)

### noxToAirExcreta

- [input.value](https://www.hestia.earth/schema/Input#value) WITH sum of inputs with [kg N](https://hestia.earth/schema/Term#units) units AND
  sum of inputs with [kg N](https://hestia.earth/schema/Term#units) units

- [product](https://www.hestia.earth/schema/Input#term) WITH [name](https://hestia.earth/schema/Term#name)

- [product.value](https://www.hestia.earth/schema/Product#value) WITH\
  [aboveGroundCropResidueLeftOnField](https://hestia.earth/term/aboveGroundCropResidueLeftOnField) AND
  [aboveGroundCropResidueIncorporated](https://hestia.earth/term/aboveGroundCropResidueIncorporated) \
  OR\
  [aboveGroundCropResidueTotal](https://hestia.earth/term/aboveGroundCropResidueTotal) AND
  [aboveGroundCropResidueBurnt](https://hestia.earth/term/aboveGroundCropResidueBurnt) AND
  [aboveGroundCropResidueRemoved](https://hestia.earth/term/aboveGroundCropResidueRemoved) \
  AND \
  sum of products with [kg N](https://hestia.earth/schema/Term#units) units AND [animalProduct](https://hestia.earth/schema/Term#termType) termType


- [measurement.value](https://www.hestia.earth/schema/Measurement#value) WITH
[ecoClimateZone](https://hestia.earth/term/ecoClimateZone)
AND [soilTotalNitrogenContent](https://hestia.earth/term/soilTotalNitrogenContent)

### noxToAirCropResidueDecomposition

- [input.value](https://www.hestia.earth/schema/Input#value) WITH sum of inputs with [kg N](https://hestia.earth/schema/Term#units) units AND
  sum of inputs with [kg N](https://hestia.earth/schema/Term#units) units

- [product](https://www.hestia.earth/schema/Input#term) WITH [name](https://hestia.earth/schema/Term#name)

- [product.value](https://www.hestia.earth/schema/Product#value) WITH\
  [aboveGroundCropResidueLeftOnField](https://hestia.earth/term/aboveGroundCropResidueLeftOnField) AND
  [aboveGroundCropResidueIncorporated](https://hestia.earth/term/aboveGroundCropResidueIncorporated) \
  OR\
  [aboveGroundCropResidueTotal](https://hestia.earth/term/aboveGroundCropResidueTotal) AND
  [aboveGroundCropResidueBurnt](https://hestia.earth/term/aboveGroundCropResidueBurnt) AND
  [aboveGroundCropResidueRemoved](https://hestia.earth/term/aboveGroundCropResidueRemoved)

- [measurement.value](https://www.hestia.earth/schema/Measurement#value) WITH
[ecoClimateZone](https://hestia.earth/term/ecoClimateZone)
AND [soilTotalNitrogenContent](https://hestia.earth/term/soilTotalNitrogenContent)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/tests_emissions/test_noxToAirFertilizerAndExcretaStehfestBouwman2006.py)
