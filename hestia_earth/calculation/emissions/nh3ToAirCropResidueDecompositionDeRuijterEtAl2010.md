# NH3, to air, crop residue decomposition, de Ruijter et al (2010)

This model calculates the NH3 emissions due to crop residue decomposition using the regression model in [de Ruijter et al (2010)](https://www.sciencedirect.com/science/article/pii/S1352231010004796).

## Calculates

- [emission.value](https://hestia.earth/schema/Emission#value) as [nh3ToAirCropResidueDecomposition](https://hestia.earth/term/nh3ToAirCropResidueDecomposition)

## Requirements

- [product.value](https://www.hestia.earth/schema/Product#value) WITH [aboveGroundCropResidueLeftOnField(https://hestia.earth/term/aboveGroundCropResidueLeftOnField)

- [product.properties.value](https://www.hestia.earth/schema/Property#value) WITH [nitrogenContent](https://hestia.earth/term/nitrogenContent)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/tests_emissions/test_nh3ToAirCropResidueDecompositionDeRuijterEtAl2010.py)
