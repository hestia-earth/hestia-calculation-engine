# Emissions from producing Inputs

This model calculates the background emissions related to the production of Inputs.

## Calculates

- [emission.value](https://hestia.earth/schema/Emission#value) WITH the given Input

## Requirements

- [input.value](https://www.hestia.earth/schema/Input#value)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/tests_emissions/test_backgroundmodel.py)
