from hestia_earth.calculation.abstract_model import Model
from hestia_earth.calculation.utils import summation
from hestia_earth.utils.api import download_hestia

MODEL_KEY = 'so2ToAirFuelCombustionEmea-Eea2019'


class SO2ToAirFuelCombustionEMEAEEA2019(Model):
    def __init__(self):

        # Define model tier
        self.tier = 1
        self.term = download_hestia(MODEL_KEY)

        # Define model requirements
        self.diesel = None
        self.gasoline = None

        # Instantiate variables
        self.so2ToAirFuelCombustion = None

        # Define model coeffients
        self.diesel_comb_so2 = 0.001
        self.gasoline_comb_so2 = 0.001

    def calculate_so2ToAirFuelCombustion(self):
        # Calculate total SO2 emissions

        diesel_emissions = 2 * self.diesel * (self.diesel_comb_so2) if self.diesel != {} else 0
        gasoline_emissions = 2 * self.gasoline * (self.gasoline_comb_so2) if self.gasoline != {} else 0

        self.so2ToAirFuelCombustion = diesel_emissions + gasoline_emissions

        return self.so2ToAirFuelCombustion

    def complete(self, completeness):

        self.diesel = 0 if self.diesel == {} and completeness['electricityFuel'] else self.diesel
        self.gasoline = 0 if self.gasoline == {} and completeness['electricityFuel'] else self.gasoline

    def check_so2ToAirFuelCombustion(self, cycle):
        # Check that we have all the inputs

        diesel_ids = ['diesel', 'diesel1D', 'diesel2D', 'diesel4D', 'en590Diesel']
        filter_diesel = [sum(element['value']) for element in cycle['inputs'].evalues()
                         if element['term']['@id'] in diesel_ids]
        self.diesel = summation(filter_diesel)

        gasoline_ids = ['motorGasoline', 'gasoline-TypeJetFuel', 'aviationGasoline']
        filter_gasoline = [sum(element['value']) for element in cycle['inputs'].evalues()
                           if element['term']['@id'] in gasoline_ids]
        self.gasoline = summation(filter_gasoline)

        self.complete(cycle['dataCompleteness'])

        return self.diesel != {} or self.gasoline != {}
