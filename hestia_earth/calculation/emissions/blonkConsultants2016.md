# Blonk Consultants (2016)

This model calculates the land transformation and emissions related to land use change, using the Blonk Consultants (2016) direct land use change assessment model.

## Calculates

- [emission.value](https://hestia.earth/schema/Emission#value) as [co2ToAirSoilCarbonStockChange](https://hestia.earth/term/co2ToAirSoilCarbonStockChange)
- [emission.value](https://hestia.earth/schema/Emission#value) as [co2ToAirOrganicSoilBurning](https://hestia.earth/term/co2ToAirOrganicSoilBurning)

## Requirements

### co2ToAirSoilCarbonStockChange

- [site.country](https://www.hestia.earth/schema/Site#country) the country where the site is located

- [measurement.value](https://www.hestia.earth/schema/Measurement) with [landTransformation20YearAverage](https://hestia.earth/term/landTransformation20YearAverage)

### co2ToAirOrganicSoilBurning

- [site.country](https://www.hestia.earth/schema/Site#country) the country where the site is located

- [measurement.value](https://www.hestia.earth/schema/Measurement) with [landTransformation20YearAverage](https://hestia.earth/term/landTransformation20YearAverage)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/tests_emissions/test_co2ToAirSoilCarbonStockChange.py)
- [Tested](../../../tests/tests_emissions/test_co2ToAirOrganicSoilBurning.py)
