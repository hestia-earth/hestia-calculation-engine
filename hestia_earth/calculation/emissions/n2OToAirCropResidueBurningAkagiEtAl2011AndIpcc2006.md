# N2O to air, crop residue burning, Akagi et al (2011) and IPCC (2006)

This model calculates the N2O emissions from crop residue burning, using the methodology detailed in the [IPCC (2006, Volume 4, Chapter 2, Section 2.4)](https://www.ipcc-nggip.iges.or.jp/public/2006gl/pdf/4_Volume4/V4_02_Ch2_Generic.pdf) guidelines and the emissions factors detailed in [Akagi et al (2011)](https://acp.copernicus.org/articles/11/4039/2011/acp-11-4039-2011.html).

## Calculates

- [emission.value](https://hestia.earth/schema/Emission#value) as [n2oToAirCropResidueBurning](https://hestia.earth/term/n2oToAirCropResidueBurning)

## Requirements

- [product.value](https://www.hestia.earth/schema/Product#value) WITH [aboveGroundCropResidueBurnt](https://hestia.earth/term/aboveGroundCropResidueBurnt)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/tests_emissions/test_n2OToAirCropResidueBurningAkagiEtAl2011AndIpcc2006.py)
