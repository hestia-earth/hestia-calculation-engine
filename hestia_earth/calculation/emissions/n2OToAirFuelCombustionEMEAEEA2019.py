from hestia_earth.calculation.abstract_model import Model
from hestia_earth.calculation.utils import summation
from hestia_earth.utils.api import download_hestia

MODEL_KEY = 'n2OToAirFuelCombustionEmea-Eea2019'


class N2OToAirFuelCombustionEMEAEEA2019(Model):
    def __init__(self):

        # Define model tier
        self.tier = 1
        self.term = download_hestia(MODEL_KEY)

        # Define model requirements
        self.diesel = None
        self.gasoline = None

        # Instantiate variables
        self.n2OToAirFuelCombustion = None

        # Define model coeffients
        self.diesel_comb_no2 = 0.000136
        self.gasoline_comb_no2 = 0.000059

    def calculate_n2OToAirFuelCombustion(self):
        # Calculate total N2O emissions

        diesel_emissions = self.diesel * (self.diesel_comb_no2) if self.diesel != {} else 0
        gasoline_emissions = self.gasoline * (self.gasoline_comb_no2) if self.gasoline != {} else 0

        self.n2OToAirFuelCombustion = diesel_emissions + gasoline_emissions

        return self.n2OToAirFuelCombustion

    def complete(self, completeness):

        self.diesel = 0 if self.diesel == {} and completeness['electricityFuel'] else self.diesel
        self.gasoline = 0 if self.gasoline == {} and completeness['electricityFuel'] else self.gasoline

    def check_n2OToAirFuelCombustion(self, cycle):
        # Check that we have all the inputs

        diesel_ids = ['diesel', 'diesel1D', 'diesel2D', 'diesel4D', 'en590Diesel']
        filter_diesel = [sum(element['value']) for element in cycle['inputs'].evalues()
                         if element['term']['@id'] in diesel_ids]
        self.diesel = summation(filter_diesel)

        gasoline_ids = ['motorGasoline', 'gasoline-TypeJetFuel', 'aviationGasoline']
        filter_gasoline = [sum(element['value']) for element in cycle['inputs'].evalues()
                           if element['term']['@id'] in gasoline_ids]
        self.gasoline = summation(filter_gasoline)

        self.complete(cycle['dataCompleteness'])

        return self.diesel != {} or self.gasoline != {}
