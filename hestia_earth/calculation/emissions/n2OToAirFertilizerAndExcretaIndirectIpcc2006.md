# N2O, to air, indirect, IPCC (2006)

This model calculates the indirect N2O emissions related to the redeposition of NOx and NH3, and nitrification and denitrification of NO3 using the Tier 1 methodology detailed in the [IPCC (2006 Volume 4, Chapter 11, Section 11.2)](https://www.ipcc-nggip.iges.or.jp/public/2006gl/pdf/4_Volume4/V4_02_Ch2_Generic.pdf) guidelines.

## Calculates

TODO

## Requirements

TODO

## Implementation Status

- `Implemented`
- [Tested](../../../tests/tests_emissions/test_n2OToAirFertilizerAndExcretaIndirectIpcc2006.py)
