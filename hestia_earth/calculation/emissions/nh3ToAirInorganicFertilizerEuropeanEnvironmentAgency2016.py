from hestia_earth.calculation.abstract_model import Model
from hestia_earth.calculation.data.constants.nh3 import NH3_FERTILIZER_FACTOR
from hestia_earth.calculation.utils import most_relevant_measurement
from hestia_earth.utils.api import download_hestia

MODEL_KEY = 'nh3ToAirInorganicFertilizerEuropeanEnvironmentAgency2016'

NO_PH = 0


class NH3ToAirInorganicFertilizerEuropeanEnvironmentAgency2016(Model):
    def __init__(self):

        # Define model tier
        self.tier = 1
        self.term = download_hestia(MODEL_KEY)

        # Define model requirements
        self.soilPh = None
        self.temperature = None
        self.ureaAsN = None
        self.ureaAmmoniumSulphateAsN = None
        self.npkBlendAsN = None
        self.ammoniumBicarbonateAsN = None
        self.ammoniumSulphateAsN = None
        self.ureaAmmoniumNitrateAsN = None
        self.ammoniumNitrateAsN = None
        self.ammoniumChlorideAsN = None
        self.potassiumNitrateAsN = None
        self.potassiumNitrateAsK2O = None
        self.calciumAmmoniumNitrateAsN = None
        self.anhydrousAmmoniaAsN = None
        self.diammoniumPhosphateAsN = None
        self.monoammoniumPhosphateAsP2O5 = None
        self.aqueousAmmoniaAsN = None
        self.calciumNitrateAsN = None
        self.nitricAcidAsN = None
        self.nkCompoundsAsN = None
        self.npCompoundsAsN = None
        self.sodiumNitrateAsN = None
        self.ammoniumSulphateNitrateAsN = None

        # Instantiate variables
        self.nh3ToAirInorganicFertilizer = None

        # Define model coeffients
        self.emf_lookup = NH3_FERTILIZER_FACTOR

    def calculate_nh3ToAirInorganicFertilizer(self):
        # Calculate total nh3 emissions

        emfs = self.get_emissions_mineral_fertilizer(self.soilPh, self.temperature)

        Urea_UAS_AmmBicarb = self.ureaAsN + self.ureaAmmoniumSulphateAsN + self.ammoniumBicarbonateAsN

        AS = self.ammoniumSulphateAsN + self.ammoniumSulphateNitrateAsN

        AN_ACl_NP_NPK =\
            self.ammoniumNitrateAsN + self.ammoniumChlorideAsN + self.potassiumNitrateAsN +\
            self.potassiumNitrateAsK2O + self.npkBlendAsN + self.calciumNitrateAsN + self.nitricAcidAsN +\
            self.nkCompoundsAsN + self.npCompoundsAsN + self.sodiumNitrateAsN

        AnhA_AquaA = self.anhydrousAmmoniaAsN + self.aqueousAmmoniaAsN

        AP = self.diammoniumPhosphateAsN + self.monoammoniumPhosphateAsP2O5

        n_inputs = [sum(Urea_UAS_AmmBicarb), sum(AS),
                    sum(self.ureaAmmoniumNitrateAsN), sum(AN_ACl_NP_NPK),
                    sum(self.calciumAmmoniumNitrateAsN),
                    sum(AnhA_AquaA), sum(AP)]

        self.nh3ToAirInorganicFertilizer = sum([x*y for x, y in zip(n_inputs, emfs)])

        return self.nh3ToAirInorganicFertilizer

    def get_emissions_mineral_fertilizer(self, phi, temperature):

        ph = phi if phi != {} else NO_PH

        conditions = [ph <= 7 and temperature <= 14,
                      ph <= 7 and temperature > 14 and temperature < 26,
                      ph <= 7 and temperature >= 26,
                      ph > 7 and temperature <= 14,
                      ph > 7 and temperature > 14 and temperature < 26,
                      ph > 7 and temperature >= 26]

        factors = [self.emf_lookup['soil ph < 7']['cool T < 14°C'].values(),
                   self.emf_lookup['soil ph < 7']['room temperature  15°C < T < 25°C'].values(),
                   self.emf_lookup['soil ph < 7']['warm T >= 26°C'].values(),
                   self.emf_lookup['soil ph >= 7']['cool T < 14°C'].values(),
                   self.emf_lookup['soil ph >= 7']['room temperature  15°C < T < 25°C'].values(),
                   self.emf_lookup['soil ph >= 7']['warm T >= 26°C'].values()]

        return factors[conditions.index(True)]

    def complete(self, completeness):

        self.ureaAsN = [0] if self.ureaAsN == {} and completeness['fertilizer'] else self.ureaAsN
        self.ureaAmmoniumSulphateAsN = [0] if self.ureaAmmoniumSulphateAsN == {} and completeness['fertilizer'] else\
            self.ureaAmmoniumSulphateAsN
        self.npkBlendAsN = [0] if self.npkBlendAsN == {} and completeness['fertilizer'] else\
            self.npkBlendAsN
        self.ammoniumBicarbonateAsN = [0] if self.ammoniumBicarbonateAsN == {} and completeness['fertilizer'] else\
            self.ammoniumBicarbonateAsN
        self.ammoniumSulphateAsN = [0] if self.ammoniumSulphateAsN == {} and completeness['fertilizer'] else\
            self.ammoniumSulphateAsN
        self.ureaAmmoniumNitrateAsN = [0] if self.ureaAmmoniumNitrateAsN == {} and completeness['fertilizer'] else\
            self.ureaAmmoniumNitrateAsN
        self.ammoniumNitrateAsN = [0] if self.ammoniumNitrateAsN == {} and completeness['fertilizer'] else\
            self.ammoniumNitrateAsN
        self.ammoniumChlorideAsN = [0] if self.ammoniumChlorideAsN == {} and completeness['fertilizer'] else\
            self.ammoniumChlorideAsN
        self.potassiumNitrateAsN = [0] if self.potassiumNitrateAsN == {} and completeness['fertilizer'] else\
            self.potassiumNitrateAsN
        self.potassiumNitrateAsK2O = [0] if self.potassiumNitrateAsK2O == {} and completeness['fertilizer'] else\
            self.potassiumNitrateAsK2O
        self.calciumAmmoniumNitrateAsN = [0] if self.calciumAmmoniumNitrateAsN == {} and completeness['fertilizer']\
            else self.calciumAmmoniumNitrateAsN
        self.anhydrousAmmoniaAsN = [0] if self.anhydrousAmmoniaAsN == {} and completeness['fertilizer'] else\
            self.anhydrousAmmoniaAsN
        self.diammoniumPhosphateAsN = [0] if self.diammoniumPhosphateAsN == {} and completeness['fertilizer'] else\
            self.diammoniumPhosphateAsN
        self.monoammoniumPhosphateAsP2O5 = [0] if self.monoammoniumPhosphateAsP2O5 == {} and completeness['fertilizer']\
            else self.monoammoniumPhosphateAsP2O5
        self.aqueousAmmoniaAsN = [0] if self.aqueousAmmoniaAsN == {} and completeness['fertilizer'] \
            else self.aqueousAmmoniaAsN
        self.calciumNitrateAsN = [0] if self.calciumNitrateAsN == {} and completeness['fertilizer'] \
            else self.calciumNitrateAsN
        self.nitricAcidAsN = [0] if self.nitricAcidAsN == {} and completeness['fertilizer'] else self.nitricAcidAsN
        self.nkCompoundsAsN = [0] if self.nkCompoundsAsN == {} and completeness['fertilizer'] else self.nkCompoundsAsN
        self.npCompoundsAsN = [0] if self.npCompoundsAsN == {} and completeness['fertilizer'] else self.npCompoundsAsN
        self.sodiumNitrateAsN = [0] if self.sodiumNitrateAsN == {} and completeness['fertilizer'] \
            else self.sodiumNitrateAsN
        self.ammoniumSulphateNitrateAsN = [0] if self.ammoniumSulphateNitrateAsN == {} and completeness['fertilizer'] \
            else self.ammoniumSulphateNitrateAsN

    def check_nh3ToAirInorganicFertilizer(self, cycle):
        # Check that we have all the inputs

        annual_temperature =\
            most_relevant_measurement(cycle['site']['measurements']['temperatureAnnual'],
                                      cycle['endDate'])
        longterm_temperature =\
            most_relevant_measurement(cycle['site']['measurements']['temperatureLongTermAnnualMean'],
                                      cycle['endDate'])
        self.temperature = annual_temperature if annual_temperature != {} else\
            longterm_temperature if longterm_temperature != {} else {}
        self.soilPh = most_relevant_measurement(cycle['site']['measurements']['soilPh'], cycle['endDate'])
        self.ureaAsN = cycle['inputs']['ureaAsN']['value']
        self.ureaAmmoniumSulphateAsN = cycle['inputs']['ureaAmmoniumSulphateAsN']['value']
        self.npkBlendAsN = cycle['inputs']['npkBlendAsN']['value']
        self.ammoniumBicarbonateAsN = cycle['inputs']['ammoniumBicarbonateAsN']['value']
        self.ammoniumSulphateAsN = cycle['inputs']['ammoniumSulphateAsN']['value']
        self.ureaAmmoniumNitrateAsN = cycle['inputs']['ureaAmmoniumNitrateAsN']['value']
        self.ammoniumNitrateAsN = cycle['inputs']['ammoniumNitrateAsN']['value']
        self.ammoniumChlorideAsN = cycle['inputs']['ammoniumChlorideAsN']['value']
        self.potassiumNitrateAsN = cycle['inputs']['potassiumNitrateAsN']['value']
        self.potassiumNitrateAsK2O = cycle['inputs']['potassiumNitrateAsK2O']['value']
        self.calciumAmmoniumNitrateAsN = cycle['inputs']['calciumAmmoniumNitrateAsN']['value']
        self.anhydrousAmmoniaAsN = cycle['inputs']['anhydrousAmmoniaAsN']['value']
        self.diammoniumPhosphateAsN = cycle['inputs']['diammoniumPhosphateAsN']['value']
        self.monoammoniumPhosphateAsP2O5 = cycle['inputs']['monoammoniumPhosphateAsP2O5']['value']
        self.aqueousAmmoniaAsN = cycle['inputs']['aqueousAmmoniaAsN']['value']
        self.calciumNitrateAsN = cycle['inputs']['calciumNitrateAsN']['value']
        self.nitricAcidAsN = cycle['inputs']['nitricAcidAsN']['value']
        self.nkCompoundsAsN = cycle['inputs']['nkCompoundsAsN']['value']
        self.npCompoundsAsN = cycle['inputs']['monoammoniumPhosphateAsP2O5']['value']
        self.sodiumNitrateAsN = cycle['inputs']['sodiumNitrateAsN']['value']
        self.ammoniumSulphateNitrateAsN = cycle['inputs']['ammoniumSulphateNitrateAsN']['value']

        inorganicNitrogenFertilizerUnspecifiedAsN = \
            cycle['inputs']['inorganicNitrogenFertilizerUnspecifiedAsN']['value']

        self.complete(cycle['dataCompleteness'])

        checkin = [self.ureaAsN, self.ureaAmmoniumSulphateAsN, self.ammoniumSulphateAsN, self.ureaAmmoniumNitrateAsN,
                   self.ammoniumNitrateAsN, self.ammoniumChlorideAsN, self.potassiumNitrateAsN,
                   self.potassiumNitrateAsK2O, self.calciumAmmoniumNitrateAsN, self.anhydrousAmmoniaAsN,
                   self.diammoniumPhosphateAsN, self.monoammoniumPhosphateAsP2O5, self.aqueousAmmoniaAsN,
                   self.calciumNitrateAsN, self.nitricAcidAsN, self.nkCompoundsAsN, self.npCompoundsAsN,
                   self.sodiumNitrateAsN, self.ammoniumSulphateNitrateAsN]

        return all(v != {} for v in checkin) and self.temperature != {} and\
            inorganicNitrogenFertilizerUnspecifiedAsN == {}
