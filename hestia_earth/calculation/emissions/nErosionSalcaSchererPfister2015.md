# N, erosion, SALCA / Scherer & Pfister (2015)

This model calculates the nitrogen (N) emissions due to soil erosion, extending the methodology detailed in [Scherer & Pfister (2015)](https://link.springer.com/article/10.1007/s11367-015-0880-0), originally developed in the SALCA guidelines.

## Calculates

- [emission.value](https://hestia.earth/schema/Emission#value) as [nErosionAllOrigins](https://hestia.earth/term/nErosionAllOrigins)

## Requirements

- [measurement.value](https://www.hestia.earth/schema/Measurement) with [nutrientLossToAquaticEnvironment](https://hestia.earth/term/nutrientLossToAquaticEnvironment)
- [measurement.value](https://www.hestia.earth/schema/Measurement) with [soilTotalNitrogenContent](https://hestia.earth/term/soilTotalNitrogenContent)
- [measurement.value](https://www.hestia.earth/schema/Measurement) with [rainfallLongTermAnnualMean](https://hestia.earth/term/rainfallLongTermAnnualMean)
- [measurement.value](https://www.hestia.earth/schema/Measurement) with [erodibility](https://hestia.earth/term/erodibility)
- [measurement.value](https://www.hestia.earth/schema/Measurement) with [slope](https://hestia.earth/term/slope)
- [measurement.value](https://www.hestia.earth/schema/Measurement) with [slopeLength](https://hestia.earth/term/slopeLength)
- [input.value](https://www.hestia.earth/schema/Input) with [waterPumpedGroundWater](https://hestia.earth/term/waterPumpedGroundWater)
- [site.country.@id](https://www.hestia.earth/schema/Site#Term)

- [practice.value](https://www.hestia.earth/schema/Practice) with [fullTillage](https://hestia.earth/term/fullTillage) or [reducedTillage](https://hestia.earth/term/reducedTillage) or with [noTillage](https://hestia.earth/term/noTillage)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/tests_emissions/test_nErosionSalcaSchererPfister2015.py)
