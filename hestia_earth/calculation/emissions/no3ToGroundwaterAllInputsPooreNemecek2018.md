# NO3, to groundwater, all inputs, Poore & Nemecek (2018)

This model calculates the NO3 emissions from leaching due to the application of organic and inorganic fertilizer and the decomposition of crop residue using the model described in [Poore & Nemecek (2018)](https://science.sciencemag.org/content/360/6392/987).It applies to cropland only. It calculates the effect of nitrogen inputs only, and excludes background leaching (e.g. due to precipitation).

## Calculates

- [emission.value](https://hestia.earth/schema/Emission#value) as [no3ToGroundwaterInorganicFertilizer](https://hestia.earth/term/no3ToGroundwaterInorganicFertilizer)
- [emission.value](https://hestia.earth/schema/Emission#value) as [no3ToGroundwaterOrganicFertilizer](https://hestia.earth/term/no3ToGroundwaterOrganicFertilizer)
- [emission.value](https://hestia.earth/schema/Emission#value) as [n2OToAirCropResidueDecompositionDirect](https://hestia.earth/term/n2OToAirCropResidueDecompositionDirect)
- [emission.value](https://hestia.earth/schema/Emission#value) as [no3ToGroundwaterCropResidueDecomposition](https://hestia.earth/term/no3ToGroundwaterCropResidueDecomposition)

## Requirements

### no3ToGroundwaterInorganicFertilizer

- [measurement.value](https://www.hestia.earth/schema/Measurement#value) WITH
[clayContent](https://hestia.earth/term/clayContent) AND [sandContent](https://hestia.earth/term/sandContent) \
AND [rainfallAnnual](https://hestia.earth/term/rainfallAnnual) OR [rainfallLongTermAnnualMean](https://hestia.earth/term/rainfallLongTermAnnualMean)

- [input.value](https://www.hestia.earth/schema/Input#value) WITH sum of inputs with [kg N](https://hestia.earth/schema/Term#units) units AND
  sum of inputs with [kg N](https://hestia.earth/schema/Term#units) units AND [inorganicFertilizer](https://hestia.earth/schema/Term#termType) termType

- [product.value](https://www.hestia.earth/schema/Product#value) WITH\
  [aboveGroundCropResidueLeftOnField](https://hestia.earth/term/aboveGroundCropResidueLeftOnField) AND
  [aboveGroundCropResidueIncorporated](https://hestia.earth/term/aboveGroundCropResidueIncorporated) \
  OR\
  [aboveGroundCropResidueTotal](https://hestia.earth/term/aboveGroundCropResidueTotal) AND
  [aboveGroundCropResidueBurnt](https://hestia.earth/term/aboveGroundCropResidueBurnt) AND
  [aboveGroundCropResidueRemoved](https://hestia.earth/term/aboveGroundCropResidueRemoved)

- [product.properties.value](https://www.hestia.earth/schema/Property#value) WITH [rootingDepth](https://hestia.earth/term/rootingDepth)

### no3ToGroundwaterOrganicFertilizer

- [measurement.value](https://www.hestia.earth/schema/Measurement#value) WITH
[clayContent](https://hestia.earth/term/clayContent) AND [sandContent](https://hestia.earth/term/sandContent) \
AND [rainfallAnnual](https://hestia.earth/term/rainfallAnnual) OR [rainfallLongTermAnnualMean](https://hestia.earth/term/rainfallLongTermAnnualMean)

- [input.value](https://www.hestia.earth/schema/Input#value) WITH sum of inputs with [kg N](https://hestia.earth/schema/Term#units) units AND
  sum of inputs with [kg N](https://hestia.earth/schema/Term#units) units AND [organicFertilizer](https://hestia.earth/schema/Term#termType) termType

- [product.value](https://www.hestia.earth/schema/Product#value) WITH\
  [aboveGroundCropResidueLeftOnField](https://hestia.earth/term/aboveGroundCropResidueLeftOnField) AND
  [aboveGroundCropResidueIncorporated](https://hestia.earth/term/aboveGroundCropResidueIncorporated) \
  OR\
  [aboveGroundCropResidueTotal](https://hestia.earth/term/aboveGroundCropResidueTotal) AND
  [aboveGroundCropResidueBurnt](https://hestia.earth/term/aboveGroundCropResidueBurnt) AND
  [aboveGroundCropResidueRemoved](https://hestia.earth/term/aboveGroundCropResidueRemoved)

- [product.properties.value](https://www.hestia.earth/schema/Property#value) WITH [rootingDepth](https://hestia.earth/term/rootingDepth)

### no3ToGroundwaterExcreta

- [measurement.value](https://www.hestia.earth/schema/Measurement#value) WITH
[clayContent](https://hestia.earth/term/clayContent) AND [sandContent](https://hestia.earth/term/sandContent) \
AND [rainfallAnnual](https://hestia.earth/term/rainfallAnnual) OR [rainfallLongTermAnnualMean](https://hestia.earth/term/rainfallLongTermAnnualMean)

- [input.value](https://www.hestia.earth/schema/Input#value) WITH sum of inputs with [kg N](https://hestia.earth/schema/Term#units) units AND
  sum of inputs with [kg N](https://hestia.earth/schema/Term#units) units

- [product.value](https://www.hestia.earth/schema/Product#value) WITH\
  [aboveGroundCropResidueLeftOnField](https://hestia.earth/term/aboveGroundCropResidueLeftOnField) AND
  [aboveGroundCropResidueIncorporated](https://hestia.earth/term/aboveGroundCropResidueIncorporated) \
  OR\
  [aboveGroundCropResidueTotal](https://hestia.earth/term/aboveGroundCropResidueTotal) AND
  [aboveGroundCropResidueBurnt](https://hestia.earth/term/aboveGroundCropResidueBurnt) AND
  [aboveGroundCropResidueRemoved](https://hestia.earth/term/aboveGroundCropResidueRemoved) \
  AND \
  sum of products with [kg N](https://hestia.earth/schema/Term#units) units AND [animalProduct](https://hestia.earth/schema/Term#termType) termType

- [product.properties.value](https://www.hestia.earth/schema/Property#value) WITH [rootingDepth](https://hestia.earth/term/rootingDepth)

### no3ToGroundwaterCropResidueDecomposition

- [measurement.value](https://www.hestia.earth/schema/Measurement#value) WITH
[clayContent](https://hestia.earth/term/clayContent) AND [sandContent](https://hestia.earth/term/sandContent) \
AND [rainfallAnnual](https://hestia.earth/term/rainfallAnnual) OR [rainfallLongTermAnnualMean](https://hestia.earth/term/rainfallLongTermAnnualMean)

- [input.value](https://www.hestia.earth/schema/Input#value) WITH sum of inputs with [kg N](https://hestia.earth/schema/Term#units) units AND
  sum of inputs with [kg N](https://hestia.earth/schema/Term#units) units AND [inorganicFertilizer](https://hestia.earth/schema/Term#termType) termType

- [product.value](https://www.hestia.earth/schema/Product#value) WITH\
  [aboveGroundCropResidueLeftOnField](https://hestia.earth/term/aboveGroundCropResidueLeftOnField) AND
  [aboveGroundCropResidueIncorporated](https://hestia.earth/term/aboveGroundCropResidueIncorporated) \
  OR\
  [aboveGroundCropResidueTotal](https://hestia.earth/term/aboveGroundCropResidueTotal) AND
  [aboveGroundCropResidueBurnt](https://hestia.earth/term/aboveGroundCropResidueBurnt) AND
  [aboveGroundCropResidueRemoved](https://hestia.earth/term/aboveGroundCropResidueRemoved)

- [product.properties.value](https://www.hestia.earth/schema/Property#value) WITH [rootingDepth](https://hestia.earth/term/rootingDepth)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/tests_emissions/test_no3ToGroundwaterAllInputsPooreNemecek2018.py)
