# CO2, to air, fuel combustion, EMEA-EEA (2019)

This model calculates the CO2 emissions from fuel combustion in engines (excluding the emission related to producing or extracting the fuel), using the emissions factors in the [EMEP-EEA Handbook (2019, Part B, Chapter 1.A.4, page 22)](https://www.eea.europa.eu/publications/emep-eea-guidebook-2019/part-b-sectoral-guidance-chapters/1-energy/1-a-combustion/1-a-4-non-road-1/view).

## Calculates

- [emission.value](https://hestia.earth/schema/Emission#value) as [co2ToAirFuelCombustionEmea-Eea2019](https://hestia.earth/term/co2ToAirFuelCombustionEmea-Eea2019)

## Requirements

- [input.value](https://www.hestia.earth/schema/Input#value) WITH
[diesel](https://hestia.earth/term/diesel)
or [diesel1D](https://hestia.earth/term/diesel1D)
or [diesel4D](https://hestia.earth/term/diesel4D)
or [diesel2D](https://hestia.earth/term/diesel2D)
or [en590Diesel](https://hestia.earth/term/en590Diesel)
or [motorGasoline](https://hestia.earth/term/motorGasoline)
or [gasoline-TypeJetFuel](https://hestia.earth/term/gasoline-TypeJetFuel)
or [aviationGasoline](https://hestia.earth/term/aviationGasoline)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/tests_emissions/test_co2ToAirFuelCombustionEMEAEEA2019.py)
