from hestia_earth.calculation.abstract_model import Model
from hestia_earth.utils.api import download_hestia
from hestia_earth.calculation.utils import summation, download_lookup
from hestia_earth.calculation.data.constants.generic import ATOMIC_WEIGHT_CONVERSIONS

MODEL_KEY = 'ipcc2019Tier1CountryAverageFertilizerMix'


class Ipcc2019Tier1CountryAverageFertilizerMix(Model):
    def __init__(self):

        # Define model tier
        self.tier = 1
        self.term = download_hestia(MODEL_KEY)

        # Define model requirements
        self.breakdown_country = None

        self.ureaAsN = None
        self.ureaAmmoniumNitrateAsN = None
        self.ureaAmmoniumSulphateAsN = None

        # Instantiate variables
        self.co2ToAirUreaHydrolysis = None

        # Define model coeffients
        self.co2c_co2 = ATOMIC_WEIGHT_CONVERSIONS['Conv_Mol_CO2C_CO2']
        self.co2_urea_factor = ATOMIC_WEIGHT_CONVERSIONS['Conv_Mol_CO2C_CO2'] * 12.012 / (14.007 * 2)
        self.conv_fert_UAN_Urea = 0.5
        self.conv_fert_UAS_Urea = 0.8
        self.inorg_fert_lookup = download_lookup('region-inorganicFertilizerBreakdown.csv')

    def calculate_co2ToAirUreaHydrolysis(self):

        # Calculate CO2 emissions

        Urea_UAS_AmmBicarb =\
            self.inorganicNitrogenFertilizerUnspecifiedAsN * self.breakdown_country['urea_uas_amm_bicarb'][0]
        ureaAmmoniumNitrateAsN =\
            self.inorganicNitrogenFertilizerUnspecifiedAsN * self.breakdown_country['uan_solu'][0]

        self.co2ToAirUreaHydrolysis =\
            (Urea_UAS_AmmBicarb + ureaAmmoniumNitrateAsN * self.conv_fert_UAN_Urea) * self.co2_urea_factor

        return self.co2ToAirUreaHydrolysis

    def complete(self, completeness):

        self.inorganicNitrogenFertilizerUnspecifiedAsN = 0 \
            if self.inorganicNitrogenFertilizerUnspecifiedAsN == {} and completeness['fertilizer']\
            else self.inorganicNitrogenFertilizerUnspecifiedAsN

    def check_co2ToAirUreaHydrolysis(self, cycle):

        # Get inputs

        self.inorganicNitrogenFertilizerUnspecifiedAsN =\
            summation(cycle['inputs']['inorganicNitrogenFertilizerUnspecifiedAsN']['value'])

        self.breakdown_country =\
            self.inorg_fert_lookup[self.inorg_fert_lookup['termid'] == cycle['site']['country']['@id']]

        self.complete(cycle['dataCompleteness'])

        return self.inorganicNitrogenFertilizerUnspecifiedAsN != {} and len(self.breakdown_country) > 0
