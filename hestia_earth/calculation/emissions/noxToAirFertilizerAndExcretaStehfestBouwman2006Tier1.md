# NOx, to air, fertilizer and excreta, Stehfest & Bouwman (2006) Tier 1

This model calculates the direct NOx emissions due to the use of fertilizer, by creating a Tier 1 version of the [Stehfest & Bouwman (2006)](https://link.springer.com/article/10.1007/s10705-006-9000-7) model using GIS software to create country average emissions factors.

## Calculates

- [emission.value](https://hestia.earth/schema/Emission#value) as [noxToAirAllOrigins](https://hestia.earth/term/noxToAirAllOrigins)

## Requirements

- [input.value](https://www.hestia.earth/schema/Input#value) WITH the sum of inputs with [kg N](https://hestia.earth/schema/Term#units) units

- [product.value](https://www.hestia.earth/schema/Product#value) WITH\
  [aboveGroundCropResidueLeftOnField](https://hestia.earth/term/aboveGroundCropResidueLeftOnField) AND
  [aboveGroundCropResidueIncorporated](https://hestia.earth/term/aboveGroundCropResidueIncorporated) \
  OR\
  [aboveGroundCropResidueTotal](https://hestia.earth/term/aboveGroundCropResidueTotal) AND
  [aboveGroundCropResidueBurnt](https://hestia.earth/term/aboveGroundCropResidueBurnt) AND
  [aboveGroundCropResidueRemoved](https://hestia.earth/term/aboveGroundCropResidueRemoved)

- [site.country](https://www.hestia.earth/schema/Site#country) the country where the site is located

## Implementation Status

- `Implemented`
- [Tested](../../../tests/tests_emissions/test_noxToAirFertilizerAndExcretaStehfestBouwman2006Tier1.py)
