# P, erosion, SALCA / Scherer & Pfister (2015)

This model computes the nitrogen emissions due to soil erosion, following the methodology detailed in [Scherer & Pfister (2015)](https://link.springer.com/article/10.1007/s11367-015-0880-0), originally developed in the SALCA guidelines.

## Calculates

- [emission.value](https://hestia.earth/schema/Emission#value) as [pErosionAllOrigins](https://hestia.earth/term/pErosionAllOrigins)

## Requirements

- [measurement.value](https://www.hestia.earth/schema/Measurement) WITH [nutrientLossToAquaticEnvironment](https://hestia.earth/term/nutrientLossToAquaticEnvironment)
- [measurement.value](https://www.hestia.earth/schema/Measurement) WITH [soilTotalNitrogenContent](https://hestia.earth/term/soilTotalNitrogenContent)
- [measurement.value](https://www.hestia.earth/schema/Measurement) WITH [rainfallLongTermAnnualMean](https://hestia.earth/term/rainfallLongTermAnnualMean)
- [measurement.value](https://www.hestia.earth/schema/Measurement) WITH [erodibility](https://hestia.earth/term/erodibility)
- [measurement.value](https://www.hestia.earth/schema/Measurement) WITH [slope](https://hestia.earth/term/slope)
- [measurement.value](https://www.hestia.earth/schema/Measurement) WITH [slopeLength](https://hestia.earth/term/slopeLength)
- [input.value](https://www.hestia.earth/schema/Input) WITH [waterPumpedGroundWater](https://hestia.earth/term/waterPumpedGroundWater)
- [site.country.@id](https://www.hestia.earth/schema/Site#country)

- [practice.value](https://www.hestia.earth/schema/Practice) WITH [fullTillage](https://hestia.earth/term/fullTillage) OR [reducedTillage](https://hestia.earth/term/reducedTillage) OR with [noTillage](https://hestia.earth/term/noTillage)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/tests_emissions/test_pErosionSalcaSchererPfister2015.py)
