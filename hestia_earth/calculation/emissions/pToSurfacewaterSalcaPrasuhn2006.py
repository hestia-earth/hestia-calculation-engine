from hestia_earth.calculation.abstract_model import Model
from hestia_earth.calculation.utils import convert_to_p, format_lookup, most_relevant_measurement, summation
from hestia_earth.utils.api import download_hestia

MODEL_KEY = 'pToSurfacewaterSalcaPrasuhn2006'


class PToSurfacewaterSalcaPrasuhn2006(Model):
    def __init__(self):

        # Define model tier
        self.tier = 1
        self.term = download_hestia(MODEL_KEY)

        # Define model requirements
        self.inorgP = None
        self.orgP = None
        self.excrP = None
        self.slope = None

        # Instantiate variables
        self.pToDrainageWaterAllInputs = None

        # Define model coeffients
        self.organic_fert_type = format_lookup('organicFertilizer.csv', 'organicfertilizerclassification')

    def calculate_pToSurfacewaterAllOrigins(self):

        self.pToSurfacewaterAllOrigins = \
            (0 if self.slope < 3 else 1) * (1 + self.inorgP * 0.2/80 + self.orgP * 0.7/80 *
                                            (0 if self.lss_ratio == {} else self.lss_ratio) +
                                            (self.orgP * (0 if self.lss_ratio == {} else self.lss_ratio) +
                                             self.excrP) * 0.4/80)

        return self.pToSurfacewaterAllOrigins

    def get_lss_ratio(self, cycle):

        inputs = list(cycle['inputs'].evalues())

        lqd_slurry_sluge = summation([convert_to_p(input)
                                      if input['term']['termType'] == 'organicFertilizer' and
                                      input['term']['@id'] in self.organic_fert_type and
                                      self.organic_fert_type[input['term']['@id']] == 'Liquid, Slurry, Sewage Sludge'
                                      else {} for input in inputs])

        total_organic_fert = summation([convert_to_p(input) if input['term']['termType'] == 'organicFertilizer'
                                        else {} for input in inputs])

        self.lss_ratio = lqd_slurry_sluge/total_organic_fert\
            if lqd_slurry_sluge != {} and total_organic_fert != {} and total_organic_fert > 0 else {}

    def complete(self, completeness):

        self.inorgP = 0 if self.inorgP == {} and completeness['fertilizer'] else self.inorgP
        self.orgP = 0 if self.orgP == {} and completeness['fertilizer'] else self.orgP
        self.excrP = 0 if self.excrP == {} and completeness['products'] else self.excrP

    def check_pToSurfacewaterAllOrigins(self, cycle):

        inputs = cycle['inputs'].evalues()

        self.inorgP = summation([sum(input['value']) if 'units' in input['term'] and
                                                        input['term']['units'] == 'kg P2O5' and
                                                        input['term']['termType'] == 'inorganicFertilizer'
                                 else {} for input in inputs])

        P_total_converted = [convert_to_p(input)
                             if 'units' in input['term'] and input['term']['units'] == 'kg' and
                             input['term']['termType'] == 'organicFertilizer' else {} for input in inputs]

        P_total_direct = [sum(input['value']) if 'units' in input['term'] and input['term']['units'] == 'kg P2O5' and
                                                 input['term']['termType'] == 'organicFertilizer' else {}
                          for input in inputs]

        self.orgP = summation(P_total_converted + P_total_direct)

        products = cycle['products'].evalues()

        self.excrP = summation([sum(product['value']) if 'units' in product['term'] and
                                                         product['term']['units'] == 'kg P2O5' and
                                                         product['term']['termType'] == 'animalProduct'
                                else {} for product in products])

        self.slope = most_relevant_measurement(cycle['site']['measurements']['slope'], cycle['endDate'])

        self.get_lss_ratio(cycle)

        self.complete(cycle['dataCompleteness'])

        return self.inorgP != {} and self.orgP != {} and self.excrP != {} and self.slope != {}
