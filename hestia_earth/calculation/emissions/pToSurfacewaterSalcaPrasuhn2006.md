# P, to surfacewater, SALCA / Prasuhn (2006)

This model computes the phosphorus emissions to surfacewater from runoff, following the methodology in [Scherer & Pfister (2015)](https://link.springer.com/article/10.1007/s11367-015-0880-0), originally developed in the SALCA guidelines and detailed in Prasuhn (2006).

## Calculates

- [emission.value](https://hestia.earth/schema/Emission#value) as [pToSurfacewaterAllInputs](https://hestia.earth/term/pToSurfacewaterAllInputs)

## Requirements

- [input.value](https://www.hestia.earth/schema/Input#value) WITH\
  sum of inputs with [kg N](https://hestia.earth/schema/Term#units) units AND [inorganicFertilizer](https://hestia.earth/schema/Term#termType) termType \
  AND \
  sum of inputs with [kg N](https://hestia.earth/schema/Term#units) units AND [organicFertilizer](https://hestia.earth/schema/Term#termType) termType \
  AND \
  sum of products with [kg N](https://hestia.earth/schema/Term#units) units AND [animalProduct](https://hestia.earth/schema/Term#termType) termType \
  AND \
  [slurryAndSludgeAsN](https://hestia.earth/term/slurryAndSludgeAsN) OR
  [cattleLiquidManureAsN](https://hestia.earth/term/cattleLiquidManureAsN) OR
  [pigsLiquidManureAsN](https://hestia.earth/term/pigsLiquidManureAsN) \
  AND\
  [cattleUrineAsN](https://hestia.earth/term/cattleUrineAsN) OR
  [compostAsN](https://hestia.earth/term/compostAsN) OR
  [greenManureAsN](https://hestia.earth/term/greenManureAsN) OR
  [oliveMillWastewaterAsN](https://hestia.earth/term/oliveMillWastewaterAsN) OR
  [palmOilMillEffluentAsN](https://hestia.earth/term/palmOilMillEffluentAsN) OR
  [pigsLiquidManureAsN](https://hestia.earth/term/pigsLiquidManureAsN) OR
  [sewageSludgeAsN](https://hestia.earth/term/sewageSludgeAsN) OR
  [slurryAndSludgeAsN](https://hestia.earth/term/slurryAndSludgeAsN) \
  AND\
  [compostAsN](https://hestia.earth/term/compostAsN) OR
  [mushroomCompostAsN](https://hestia.earth/term/mushroomCompostAsN) \
  AND\
  [greenManureAsN](https://hestia.earth/term/greenManureAsN)

- [measurement.value](https://www.hestia.earth/schema/Measurement) WITH [slope](https://hestia.earth/term/slope)


## Implementation Status

- `Implemented`
- [Tested](../../../tests/tests_emissions/test_pToSurfacewaterSalcaPrasuhn2006.py)
