from hestia_earth.calculation.abstract_model import Model
from hestia_earth.utils.api import download_hestia
from hestia_earth.calculation.utils import summation
from hestia_earth.calculation.data.constants.generic import ATOMIC_WEIGHT_CONVERSIONS

MODEL_KEY = 'co2ToAirLimeAndUreaIpcc2006'


class CO2ToAirLimeAndUreaIpcc2006(Model):
    def __init__(self):

        # Define model tier
        self.tier = 1
        self.term = download_hestia(MODEL_KEY)

        # Define model requirements
        self.total_lime = None
        self.total_dolomiticLime = None

        self.ureaAsN = None
        self.ureaAmmoniumNitrateAsN = None
        self.ureaAmmoniumSulphateAsN = None

        # Instantiate variables
        self.co2ToAirLimeHydrolysis = None
        self.co2ToAirUreaHydrolysis = None

        # Define model coeffients
        self.co2c_co2 = ATOMIC_WEIGHT_CONVERSIONS['Conv_Mol_CO2C_CO2']
        self.co2_lime_factor = 0.12
        self.co2_dolomite_factor = 0.13
        self.co2_urea_factor = ATOMIC_WEIGHT_CONVERSIONS['Conv_Mol_CO2C_CO2'] * 12.012 / (14.007 * 2)
        self.conv_fert_UAN_Urea = 0.5

    def calculate_co2ToAirLimeHydrolysis(self):

        # Calculate CO2 emissions

        self.co2ToAirLimeHydrolysis = self.total_lime * self.co2_lime_factor * self.co2c_co2 + \
                                      self.total_dolomiticLime * self.co2_dolomite_factor * self.co2c_co2

        return self.co2ToAirLimeHydrolysis

    def calculate_co2ToAirUreaHydrolysis(self):

        # Calculate CO2 emissions

        self.co2ToAirUreaHydrolysis = (self.ureaAsN + self.ureaAmmoniumNitrateAsN * self.conv_fert_UAN_Urea) * \
                                      self.co2_urea_factor

        return self.co2ToAirUreaHydrolysis

    def complete(self, completeness):

        self.total_lime = 0 if self.total_lime == {} and completeness['soilAmendments'] else self.total_lime
        self.total_dolomiticLime = 0 if self.total_dolomiticLime == {} and completeness['soilAmendments'] else\
            self.total_dolomiticLime
        self.ureaAsN = 0 if self.ureaAsN == {} and completeness['fertilizer'] else self.ureaAsN
        self.ureaAmmoniumNitrateAsN = 0 if self.ureaAmmoniumNitrateAsN == {} and completeness['fertilizer'] else\
            self.ureaAmmoniumNitrateAsN

    def check_co2ToAirLimeHydrolysis(self, cycle):

        # Get inputs

        inputs = cycle['inputs'].evalues()

        soilAmends = [input for input in inputs
                      if input['term']['termType'] == "soilAmendment"]

        self.total_lime = summation([sum(input['value']) for input in soilAmends
                                     if input['term']['@id'] == "lime" or
                                     input['term']['@id'] == "aragonite" or
                                     input['term']['@id'] == "agriculturalLimestone" or
                                     input['term']['@id'] == "hydratedLime" or
                                     input['term']['@id'] == "burntLime"])

        # Calculate total dolomitelime input

        self.total_dolomiticLime = summation([sum(input['value'])for input in soilAmends
                                              if input['term']['@id'] == "dolomiticLime"])

        self.complete(cycle['dataCompleteness'])

        return self.total_lime != {} and self.total_dolomiticLime != {}

    def check_co2ToAirUreaHydrolysis(self, cycle):

        # Get inputs

        inputs = cycle['inputs'].evalues()

        self.ureaAsN = summation([sum(input['value']) for input in inputs
                                  if input['term']['termType'] == "inorganicFertilizer" and
                                  input['term']['@id'] == "ureaAsN"])

        self.ureaAmmoniumNitrateAsN = summation([sum(input['value']) for input in inputs
                                                 if input['term']['termType'] == "inorganicFertilizer" and
                                                 input['term']['@id'] == "ureaAmmoniumNitrateAsN"])

        inorganicNitrogenFertilizerUnspecifiedAsN = \
            cycle['inputs']['inorganicNitrogenFertilizerUnspecifiedAsN']['value']

        self.complete(cycle['dataCompleteness'])

        return self.ureaAsN != {} and self.ureaAmmoniumNitrateAsN != {} and\
            inorganicNitrogenFertilizerUnspecifiedAsN == {}
