# NH3, to air, inorganic fertilizer, European Environment Agency (2016)

This model calculates NH3 emissions from inorganic fertilizer application following the methodology in the [EEA (2016, Chapter 4, Section 3.D)](https://www.eea.europa.eu/publications/emep-eea-guidebook-2016/part-b-sectoral-guidance-chapters/4-agriculture/3-d-crop-production-and) guidelines.

## Calculates

- [emission.value](https://hestia.earth/schema/Emission#value) as [nh3ToAirInorganicFertilizer](https://hestia.earth/term/nh3ToAirInorganicFertilizer)

## Requirements

- [input.value](https://www.hestia.earth/schema/Input#value) WITH
[ureaAsN](https://hestia.earth/term/ureaAsN)
AND [ureaAmmoniumSulphateAsN](https://hestia.earth/term/ureaAmmoniumSulphateAsN)
AND [ammoniumBicarbonateAsN](https://hestia.earth/term/ammoniumBicarbonateAsN)
AND [ammoniumSulphateAsN](https://hestia.earth/term/ammoniumSulphateAsN)
AND [ureaAmmoniumNitrateAsN](https://hestia.earth/term/ureaAmmoniumNitrateAsN)
AND [ammoniumNitrateAsN](https://hestia.earth/term/ammoniumNitrateAsN)
AND [ammoniumChlorideAsN](https://hestia.earth/term/ammoniumChlorideAsN)
AND [potassiumNitrateAsN](https://hestia.earth/term/potassiumNitrateAsN)
AND [potassiumNitrateAsK2O](https://hestia.earth/term/potassiumNitrateAsK2O)
AND [calciumAmmoniumNitrateAsN](https://hestia.earth/term/calciumAmmoniumNitrateAsN)
AND [anhydrousAmmoniumAsN](https://hestia.earth/term/anhydrousAmmoniumAsN)
AND [diammoniumPhosphateAsN](https://hestia.earth/term/diammoniumPhosphateAsN)
AND [monoammoniumPhosphateAsP2O5](https://hestia.earth/term/monoammoniumPhosphateAsP2O5)

- [measurement.value](https://www.hestia.earth/schema/Measurement#value) WITH
[temperatureAnnual](https://hestia.earth/term/temperatureAnnual) or [temperatureLongTermAnnualMean](https://hestia.earth/term/temperatureAnnual) \
AND [soilPh](https://hestia.earth/term/soilPh)
AND [soilOrganicCarbonContent](https://hestia.earth/term/soilOrganicCarbonContent)
AND [soilPh](https://hestia.earth/term/soilPh)
AND [ecoClimateZone](https://hestia.earth/term/ecoClimateZone)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/tests_emissions/test_nh3ToAirInorganicFertilizerEuropeanEnvironmentAgency2016.py)
