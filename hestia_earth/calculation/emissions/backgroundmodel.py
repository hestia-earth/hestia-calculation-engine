import json
import os
from hestia_earth.calculation.abstract_model import Model
from hestia_earth.calculation.data.impact_assessments import IMPACT_ASSESSMENTS_DIR
from hestia_earth.utils.api import download_hestia

MODEL_KEY = 'ecoinventV3'


class Backgroungmodel(Model):
    def __init__(self):

        self.tier = "background"
        self.term = download_hestia(MODEL_KEY)

        self.inputs = None
        self.result = None
        self.assessment_inputs = []

    def calculate_background(self, emission_id):

        # Calculate Background emissions

        result = 0
        for inp in self.inputsb:
            if inp['term']['@id'] in self.impact_assessment:
                result += sum(inp['value']) * self.impact_assessment[inp['term']['@id']][emission_id]
                self.assessment_inputs.append(inp['term'])

        self.result = result if result is not None else None

        return self.result

    def import_impact_assessment(self):

        raw_impact_assessment = []
        for filename in os.listdir(IMPACT_ASSESSMENTS_DIR):
            if len(filename.split(".")) == 2 and filename.split(".")[1] == 'jsonld':
                with open(os.path.join(IMPACT_ASSESSMENTS_DIR, filename), 'r') as file:
                    raw_impact_assessment.append(json.load(file))

        clean_impact_assessment = {}

        for element in raw_impact_assessment:
            subdict = {emission['term']['@id'].replace('-', ''): float(emission['value'])
                       for emission in element['emissionsResourceUse']}
            clean_impact_assessment[element['product']['@id']] = subdict

        return clean_impact_assessment

    def check(self, cycle):

        self.inputsb = cycle['inputs'].evalues()
        self.impact_assessment = self.import_impact_assessment()

        return True
