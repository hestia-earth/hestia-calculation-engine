# NH3, to air, organic fertilizer, Webb et al (2012) and Sintermann et al (2012)

This model calculates the NH3 emissions due to the addition of organic fertilizer based on a compilation of emissions factors from [Webb et al (2012)](https://link.springer.com/chapter/10.1007/978-94-007-1905-7_4) and [Sintermann et al (2012)](https://bg.copernicus.org/articles/9/1611/2012/). The methodology for compiling these emissions is detailed in [Poore & Nemecek (2018)](https://science.sciencemag.org/content/360/6392/987).

## Calculates

- [emission.value](https://hestia.earth/schema/Emission#value) as [nh3ToAirOrganicFertilizer](https://hestia.earth/term/nh3ToAirOrganicFertilizer)

## Requirements

- [input.value](https://www.hestia.earth/schema/Input#value) WITH\
  [slurryAndSludgeAsN](https://hestia.earth/term/slurryAndSludgeAsN) OR
  [cattleLiquidManureAsN](https://hestia.earth/term/cattleLiquidManureAsN) OR
  [pigsLiquidManureAsN](https://hestia.earth/term/pigsLiquidManureAsN) \
  AND\
  [cattleUrineAsN](https://hestia.earth/term/cattleUrineAsN) OR
  [compostAsN](https://hestia.earth/term/compostAsN) OR
  [greenManureAsN](https://hestia.earth/term/greenManureAsN) OR
  [oliveMillWastewaterAsN](https://hestia.earth/term/oliveMillWastewaterAsN) OR
  [palmOilMillEffluentAsN](https://hestia.earth/term/palmOilMillEffluentAsN) OR
  [pigsLiquidManureAsN](https://hestia.earth/term/pigsLiquidManureAsN) OR
  [sewageSludgeAsN](https://hestia.earth/term/sewageSludgeAsN) OR
  [slurryAndSludgeAsN](https://hestia.earth/term/slurryAndSludgeAsN) \
  AND\
  [compostAsN](https://hestia.earth/term/compostAsN) OR
  [mushroomCompostAsN](https://hestia.earth/term/mushroomCompostAsN) \
  AND\
  [greenManureAsN](https://hestia.earth/term/greenManureAsN)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/tests_emissions/test_nh3ToAirOrganicFertilizerWebbEtAl2012AndSintermannEtAl2012.py)
