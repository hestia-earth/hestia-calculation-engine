# P, to drainage water, SALCA / Prasuhn (2006)

This model calculates the phosphorus (P) emissions to drainage water, following the methodology in [Scherer & Pfister (2015)](https://link.springer.com/article/10.1007/s11367-015-0880-0), originally developed in the SALCA guidelines and detailed in Prasuhn (2006).

## Calculates

- [emission.value](https://hestia.earth/schema/Emission#value) as [pToDrainageWaterAllInputs](https://hestia.earth/term/pToDrainageWaterAllInputs)

## Requirements

- [input.value](https://www.hestia.earth/schema/Input#value) WITH the sum of inputs with [kg P2O5](https://hestia.earth/schema/Term#units) units AND [organicFertilizer](https://hestia.earth/schema/Term#termTye) \
AND \
  [slurryAndSludgeAsN](https://hestia.earth/term/slurryAndSludgeAsN) OR [cattleLiquidManureAsN](https://hestia.earth/term/cattleLiquidManureAsN) OR [pigsLiquidManureAsN](https://hestia.earth/term/pigsLiquidManureAsN)

- [emission.value](https://hestia.earth/schema/Emission#value) as [pToSurfacewaterAllInputs](https://hestia.earth/term/pToSurfacewaterAllInputs)

- [measurement.value](https://www.hestia.earth/schema/Measurement) WITH [drainageClass](https://hestia.earth/term/drainageClass)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/tests_emissions/test_pToDrainageWaterSalcaPrasuhn2006.py)
