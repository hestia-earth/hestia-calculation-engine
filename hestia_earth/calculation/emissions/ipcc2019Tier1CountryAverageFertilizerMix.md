# IPCC (2019) Tier 1 Country Average Fertilizer Mix

This model calculates the CO2 emissions due urea-containing fertiliser. It uses the Tier 1 factors from the [IPCC (2006, Volume 4, Chapter 11, Sections 11.3 and 11.4)](https://www.ipcc-nggip.iges.or.jp/public/2006gl/pdf/4_Volume4/V4_11_Ch11_N2O&CO2.pdf) guidelines.

## Calculates
- [emission.value](https://hestia.earth/schema/Emission#value) as [co2ToAirUreaHydrolysis](https://hestia.earth/term/co2ToAirUreaHydrolysis)

## Requirements

- [input.value](https://www.hestia.earth/schema/Input#value) WITH
[inorganicNitrogenFertilizerUnspecifiedAsN](https://hestia.earth/term/inorganicNitrogenFertilizerUnspecifiedAsN)

- [site.country](https://www.hestia.earth/schema/Site#country) the country where the site is located

## Implementation Status

- `Implemented`
- [Tested](../../../tests/tests_emissions/test_ipcc2019Tier1CountryAverageFertilizerMix.py)
