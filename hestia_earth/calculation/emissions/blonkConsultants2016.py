from hestia_earth.calculation.abstract_model import Model
from hestia_earth.utils.api import download_hestia
from hestia_earth.utils.lookup import get_table_value, download_lookup
from hestia_earth.calculation.utils import format_lookup, most_relevant_measurement, primary_product

MODEL_KEY = 'blonkConsultants2016'


class BlonkConsultants2016(Model):
    def __init__(self):

        # Define model tier
        self.tier = 1
        self.term = download_hestia(MODEL_KEY)

        # Define model requirements
        self.landOccupation = None
        self.emission_factor_0 = None
        self.emission_factor_1 = None
        self.emission_factor_2 = None
        self.country = None
        self.crop_grouping = None

        # Instantiate variables
        self.co2ToAirSoilCarbonStockChange = None
        self.co2ToAirOrganicSoilBurning = None

        # Define model coeffients
        self.grouping_lookup = format_lookup('crop.csv', 'cropgroupingfaostat')
        self.co2landuse = download_lookup('region-crop-cropGroupingFAOSTAT-co2landuse.csv')
        self.co2ch4n2olanduseburning = download_lookup('region-crop-cropGroupingFAOSTAT-co2ch4n2olanduseburning.csv')
        self.ch4n2olanduse = download_lookup('region-crop-cropGroupingFAOSTAT-ch4n2olanduse.csv')

    def calculate_co2ToAirSoilCarbonStockChange(self):
        # Calculate total co2 emissions

        self.co2ToAirSoilCarbonStockChange = (self.emission_factor_0) * self.landOccupation * 1000 / 10000

        return self.co2ToAirSoilCarbonStockChange

    def calculate_co2ToAirOrganicSoilBurning(self):
        # Calculate total co2 emissions

        self.co2ToAirOrganicSoilBurning = (self.emission_factor_1 + self.emission_factor_2) *\
                                          self.landOccupation / 1000

        return self.co2ToAirOrganicSoilBurning

    def complete(self, completeness):

        pass

    def check_co2ToAirSoilCarbonStockChange(self, cycle):
        # Check that we have all the inputs

        self.landOccupation =\
            most_relevant_measurement(cycle['site']['measurements']['landOccupation'],
                                      cycle['endDate'])
        self.country = cycle['site']['country']['@id']
        product = primary_product(cycle['products']) if cycle['products'] != {} else {}
        self.crop_grouping = \
            self.grouping_lookup.get(product['term']['@id'], {}).replace(",", "").replace(" ", "_").lower() \
            if product != {} else {}

        self.emission_factor_0 =\
            get_table_value(self.co2landuse, 'termid', self.country, self.crop_grouping) \
            if self.crop_grouping != {} else {}

        return self.landOccupation != {} and self.emission_factor_0 != {}

    def check_co2ToAirOrganicSoilBurning(self, cycle):
        # Check that we have all the inputs

        self.landOccupation =\
            most_relevant_measurement(cycle['site']['measurements']['landOccupation'],
                                      cycle['endDate'])
        self.country = cycle['site']['country']['@id']
        product = primary_product(cycle['products']) if cycle['products'] != {} else {}
        self.crop_grouping = \
            self.grouping_lookup.get(product['term']['@id'], {}).replace(",", "").replace(" ", "_").lower() \
            if product != {} else {}

        self.emission_factor_1 =\
            get_table_value(self.co2ch4n2olanduseburning, 'termid', self.country, self.crop_grouping) \
            if self.crop_grouping != {} else {}

        self.emission_factor_2 =\
            get_table_value(self.ch4n2olanduse, 'termid', self.country, self.crop_grouping) \
            if self.crop_grouping != {} else {}

        return self.landOccupation != {} and self.emission_factor_1 != {} and self.emission_factor_2 != {}
