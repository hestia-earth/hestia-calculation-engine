from hestia_earth.calculation.abstract_model import Model
from hestia_earth.calculation.utils import convert_to_p, format_lookup, most_relevant_measurement, summation
from hestia_earth.utils.api import download_hestia

MODEL_KEY = 'pToGroundwaterSalcaPrasuhn2006'

DC_HIGH = 100


class PToGroundwaterSalcaPrasuhn2006(Model):
    def __init__(self):

        # Define model tier
        self.tier = 1
        self.term = download_hestia(MODEL_KEY)

        # Define model requirements
        self.orgP = None
        self.drainageClass = None

        # Instantiate variables
        self.pToGroundwaterAllOrigins = None
        self.lss_ratio = None

        # Define model coeffients
        self.organic_fert_type = format_lookup('organicFertilizer.csv', 'organicfertilizerclassification')

    def calculate_pToGroundwaterAllOrigins(self):

        if self.orgP != {}:
            self.pToGroundwaterAllOrigins = \
                0.07 * (1 + (0 if self.orgP == 0 else self.orgP * 0.2/80 * (0 if self.lss_ratio == {} else
                                                                            self.lss_ratio))) *\
                (0 if self.drainageClass > 3 else 1)

        return self.pToGroundwaterAllOrigins

    def get_lss_ratio(self, cycle):

        inputs = list(cycle['inputs'].evalues())

        lqd_slurry_sluge = summation([convert_to_p(input)
                                      if input['term']['termType'] == 'organicFertilizer' and
                                      input['term']['@id'] in self.organic_fert_type and
                                      self.organic_fert_type[input['term']['@id']] == 'Liquid, Slurry, Sewage Sludge'
                                      else {} for input in inputs])

        total_organic_fert = summation([convert_to_p(input) if input['term']['termType'] == 'organicFertilizer'
                                        else {} for input in inputs])

        self.lss_ratio = lqd_slurry_sluge/total_organic_fert\
            if lqd_slurry_sluge != {} and total_organic_fert != {} and total_organic_fert > 0 else {}

    def complete(self, completeness):

        self.orgP = 0 if self.orgP == {} and completeness['fertilizer'] else self.orgP
        self.drainageClass = self.drainageClass if self.drainageClass != {} else DC_HIGH

    def check_pToGroundwaterAllOrigins(self, cycle):

        inputs = cycle['inputs'].evalues()

        P_total_converted = [convert_to_p(input)
                             if 'units' in input['term'] and input['term']['units'] == 'kg' and
                             input['term']['termType'] == 'organicFertilizer' else {} for input in inputs]

        P_total_direct = [sum(input['value']) if 'units' in input['term'] and input['term']['units'] == 'kg P2O5' and
                                                 input['term']['termType'] == 'organicFertilizer' else {}
                          for input in inputs]

        self.orgP = summation(P_total_converted + P_total_direct)

        self.drainageClass = most_relevant_measurement(cycle['site']['measurements']['drainageClass'], cycle['endDate'])

        self.get_lss_ratio(cycle)

        self.complete(cycle['dataCompleteness'])

        return self.orgP != {}
