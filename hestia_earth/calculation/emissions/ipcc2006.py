from hestia_earth.calculation.abstract_model import Model
from hestia_earth.utils.api import download_hestia
from hestia_earth.calculation.utils import most_relevant_measurement
from hestia_earth.calculation.data.constants.co2 import CO2_FACTORS_ORGANIC_SOILS
from hestia_earth.calculation.data.constants.n2o import N2O_FACTORS_ORGANIC_SOILS

MODEL_KEY = 'ipcc2006'


class IPCC2006(Model):

    def __init__(self):

        # Define model tier
        self.tier = 1
        self.term = download_hestia(MODEL_KEY)

        # Define model requirements
        self.histosol = None
        self.land_use_change = None
        self.eco_ClimateZone = None

        # Instantiate variables
        self.co2ToAirOrganicSoilCultivation = None
        self.n2OToAirOrganicSoilCultivationDirect = None

        # Define model coeffients
        self.co2_organigsoils_ef = CO2_FACTORS_ORGANIC_SOILS
        self.n2o_organigsoils_ef = N2O_FACTORS_ORGANIC_SOILS

    def calculate_co2ToAirOrganicSoilCultivation(self):
        # Calculate total co2 emissions

        self.co2ToAirOrganicSoilCultivation =\
            self.land_use_change * self.histosol * self.co2_organigsoils_ef[self.siteType][str(self.eco_ClimateZone)]

        return self.co2ToAirOrganicSoilCultivation

    def calculate_n2OToAirOrganicSoilCultivationDirect(self):
        # Calculate total n2o emissions

        self.n2OToAirOrganicSoilCultivationDirect =\
            self.land_use_change * self.histosol * self.n2o_organigsoils_ef[str(self.eco_ClimateZone)]

        return self.n2OToAirOrganicSoilCultivationDirect

    def check_co2ToAirOrganicSoilCultivation(self, cycle):
        # Check that we have all the inputs

        self.siteType = cycle['site']['siteType']
        self.histosol = most_relevant_measurement(cycle['site']['measurements']['histosol'], cycle['endDate'])
        self.eco_ClimateZone = most_relevant_measurement(cycle['site']['measurements']['ecoClimateZone'],
                                                         cycle['endDate'])
        self.land_use_change =\
            most_relevant_measurement(cycle['site']['measurements']['landTransformation20YearAverage'],
                                      cycle['endDate'])

        return self.histosol != {} and self.land_use_change != {} and self.eco_ClimateZone != {} and self.siteType != {}

    def check_n2OToAirOrganicSoilCultivationDirect(self, cycle):
        # Check that we have all the inputs

        self.histosol = most_relevant_measurement(cycle['site']['measurements']['histosol'], cycle['endDate'])
        self.eco_ClimateZone = most_relevant_measurement(cycle['site']['measurements']['ecoClimateZone'],
                                                         cycle['endDate'])
        self.land_use_change =\
            most_relevant_measurement(cycle['site']['measurements']['landTransformation20YearAverage'],
                                      cycle['endDate'])

        return self.histosol != {} and self.land_use_change != {} and self.eco_ClimateZone != {}
