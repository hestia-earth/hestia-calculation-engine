from hestia_earth.calculation.abstract_model import Model
from hestia_earth.utils.api import download_hestia
from hestia_earth.calculation.data.constants.n2o import DRY_MATTER_FACTOR_TO_NO2
from hestia_earth.calculation.utils import summation

MODEL_KEY = 'n2OToAirCropResidueBurningAkagiEtAl2011AndIpcc2006'


class N2OToAirCropResidueBurningAkagiEtAl2011AndIpcc2006(Model):
    def __init__(self):

        # Define model tier
        self.tier = 1
        self.term = download_hestia(MODEL_KEY)

        # Define model requirements
        self.dry_matter = None

        # Instantiate variables
        self.n2OToAirCropResidueBurningDirect = None

    def calculate_n2OToAirCropResidueBurningDirect(self):
        # Calculate total N2O emissions

        self.n2OToAirCropResidueBurningDirect = self.dry_matter * DRY_MATTER_FACTOR_TO_NO2

        return self.n2OToAirCropResidueBurningDirect

    def complete(self, completeness):

        self.dry_matter = 0 if self.dry_matter == {} and completeness['cropResidue'] else self.dry_matter

    def check_n2OToAirCropResidueBurningDirect(self, cycle):
        # Check that we have all the inputs

        self.dry_matter = summation(cycle['products']['aboveGroundCropResidueBurnt']['value'])

        self.complete(cycle['dataCompleteness'])

        return self.dry_matter != {}
