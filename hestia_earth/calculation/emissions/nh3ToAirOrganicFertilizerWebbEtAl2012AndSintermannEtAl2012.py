import numpy as np
from hestia_earth.calculation.abstract_model import Model
from hestia_earth.calculation.data.constants.nh3 import NH3_TAN_FACTOR, CONV_ORGFERT_N_TAN
from hestia_earth.utils.api import download_hestia
from hestia_earth.calculation.utils import convert_to_n, format_lookup, summation

MODEL_KEY = 'nh3ToAirOrganicFertilizerWebbEtAl2012AndSintermannEtAl2012'


class NH3ToAirOrganicFertilizerWebbEtAl2012AndSintermannEtAl2012(Model):
    def __init__(self):

        # Define model tier
        self.tier = 1
        self.term = download_hestia(MODEL_KEY)

        # Define model requirements
        self.lqd_slurry_sluge = None
        self.solid = None
        self.compost = None
        self.greenManure = None

        # Instantiate variables
        self.nh3ToAirOrganicFertilizer = None

        # Define model coeffients
        self.eafs_lookup = NH3_TAN_FACTOR
        self.Conv_OrgFert_N_TAN = CONV_ORGFERT_N_TAN
        self.organic_fert_type = format_lookup('organicFertilizer.csv', 'organicfertilizerclassification')

    def calculate_nh3ToAirOrganicFertilizer(self):
        # Calculate total nh3 emissions

        eafs = self.eafs_lookup.values()

        n_inputs = np.array([self.lqd_slurry_sluge, self.solid, self.compost, self.greenManure])

        org_tan = [x * y for x, y in zip(n_inputs, self.Conv_OrgFert_N_TAN)]

        self.nh3ToAirOrganicFertilizer = sum([x * y for x, y in zip(org_tan, eafs)])

        return self.nh3ToAirOrganicFertilizer

    def access_lqd_slurry_sluge(self, cycle):

        inputs = list(cycle['inputs'].evalues())

        N_total_converted = [convert_to_n(input)
                             if 'units' in input['term'] and
                                input['term']['units'] == 'kg' and
                                input['term']['termType'] == 'organicFertilizer' and
                                input['term']['@id'] in self.organic_fert_type and
                                self.organic_fert_type[input['term']['@id']] == 'Liquid, Slurry, Sewage Sludge'
                             else {} for input in inputs]

        N_total_direct = [sum(input['value']) if 'units' in input['term'] and input['term']['units'] == 'kg N' and
                                                 input['term']['termType'] == 'organicFertilizer' and
                                                 input['term']['@id'] in self.organic_fert_type and
                                                 self.organic_fert_type[input['term']['@id']] ==
                                                 'Liquid, Slurry, Sewage Sludge'
                          else {}
                          for input in inputs]

        self.lqd_slurry_sluge = summation(N_total_converted + N_total_direct)

    def access_solid(self, cycle):

        inputs = list(cycle['inputs'].evalues())

        N_total_converted = [convert_to_n(input)
                             if 'units' in input['term'] and
                                input['term']['units'] == 'kg' and
                                input['term']['termType'] == 'organicFertilizer' and
                                input['term']['@id'] in self.organic_fert_type and
                                self.organic_fert_type[input['term']['@id']] == 'Solid'
                             else {} for input in inputs]

        N_total_direct = [sum(input['value']) if 'units' in input['term'] and input['term']['units'] == 'kg N' and
                                                 input['term']['termType'] == 'organicFertilizer' and
                                                 input['term']['@id'] in self.organic_fert_type and
                                                 self.organic_fert_type[input['term']['@id']] == 'Solid' else {}
                          for input in inputs]

        self.solid = summation(N_total_converted + N_total_direct)

    def access_compost(self, cycle):

        inputs = list(cycle['inputs'].evalues())

        N_total_converted = [convert_to_n(input)
                             if 'units' in input['term'] and
                                input['term']['units'] == 'kg' and
                                input['term']['termType'] == 'organicFertilizer' and
                                input['term']['@id'] in self.organic_fert_type and
                                self.organic_fert_type[input['term']['@id']] == 'Compost'
                             else {} for input in inputs]

        N_total_direct = [sum(input['value']) if 'units' in input['term'] and input['term']['units'] == 'kg N' and
                                                 input['term']['termType'] == 'organicFertilizer' and
                                                 input['term']['@id'] in self.organic_fert_type and
                                                 self.organic_fert_type[input['term']['@id']] == 'Compost' else {}
                          for input in inputs]

        self.compost = summation(N_total_converted + N_total_direct)

    def access_greenManure(self, cycle):

        inputs = list(cycle['inputs'].evalues())

        N_total_converted = [convert_to_n(input)
                             if 'units' in input['term'] and
                                input['term']['units'] == 'kg' and
                                input['term']['termType'] == 'organicFertilizer' and
                                input['term']['@id'] in self.organic_fert_type and
                                self.organic_fert_type[input['term']['@id']] == 'Green Manure'
                             else {} for input in inputs]

        N_total_direct = [sum(input['value']) if 'units' in input['term'] and input['term']['units'] == 'kg N' and
                                                 input['term']['termType'] == 'organicFertilizer' and
                                                 input['term']['@id'] in self.organic_fert_type and
                                                 self.organic_fert_type[input['term']['@id']] == 'Green Manure' else {}
                          for input in inputs]

        self.greenManure = summation(N_total_converted + N_total_direct)

    def complete(self, completeness):

        self.lqd_slurry_sluge = 0 if self.lqd_slurry_sluge == {} and completeness['fertilizer'] else\
            self.lqd_slurry_sluge
        self.solid = 0 if self.solid == {} and completeness['fertilizer'] else self.solid
        self.compost = 0 if self.compost == {} and completeness['fertilizer'] else self.compost
        self.greenManure = 0 if self.greenManure == {} and completeness['fertilizer'] else self.greenManure

    def check_nh3ToAirOrganicFertilizer(self, cycle):
        # Check that we have all the inputs

        self.access_lqd_slurry_sluge(cycle)
        self.access_solid(cycle)
        self.access_compost(cycle)
        self.access_greenManure(cycle)

        self.complete(cycle['dataCompleteness'])

        return self.lqd_slurry_sluge != {} and self.solid != {} and self.compost != {} and self.greenManure != {}
