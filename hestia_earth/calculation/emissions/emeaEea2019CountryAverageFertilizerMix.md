# EMEA-EEA (2019) Country Average Fertilizer Mix

This model calculates NH3 emissions from inorganic fertilizer application following the methodology in the [EEA (2016, Chapter 4, Section 3.D)](https://www.eea.europa.eu/publications/emep-eea-guidebook-2016/part-b-sectoral-guidance-chapters/4-agriculture/3-d-crop-production-and) guidelines.

## Calculates

- [emission.value](https://hestia.earth/schema/Emission#value) as [nh3ToAirInorganicFertilizer](https://hestia.earth/term/nh3ToAirInorganicFertilizer)

## Requirements

- [input.value](https://www.hestia.earth/schema/Input#value) WITH
[inorganicNitrogenFertilizerUnspecifiedAsN](https://hestia.earth/term/inorganicNitrogenFertilizerUnspecifiedAsN)

- [site.country](https://www.hestia.earth/schema/Site#country) the country where the site is located

- [measurement.value](https://www.hestia.earth/schema/Measurement#value) WITH
[temperatureAnnual](https://hestia.earth/term/temperatureAnnual) or [temperatureLongTermAnnualMean](https://hestia.earth/term/temperatureAnnual) \
AND [soilPh](https://hestia.earth/term/soilPh)
AND [soilOrganicCarbonContent](https://hestia.earth/term/soilOrganicCarbonContent)
AND [soilPh](https://hestia.earth/term/soilPh)
AND [ecoClimateZone](https://hestia.earth/term/ecoClimateZone)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/tests_emissions/test_emeaEea2019CountryAverageFertilizerMix.py)
