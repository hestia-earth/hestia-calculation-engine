from hestia_earth.calculation.abstract_model import Model
from hestia_earth.calculation.utils import convert_to_n, format_lookup, primary_product, summation
from hestia_earth.utils.api import download_hestia
from hestia_earth.calculation.data.constants.generic import ATOMIC_WEIGHT_CONVERSIONS

MODEL_KEY = 'n2OToAirFertilizerAndExcretaDirectAndIndirectIpcc2006'


class N2OToAirFertilizerAndExcretaDirectIpcc2006(Model):
    def __init__(self):

        # Define model tier
        self.tier = 1
        self.term = download_hestia(MODEL_KEY)

        # Define model requirements
        self.inorgN_total = None
        self.orgN_total = None
        self.excretaN_total = None
        self.products = None
        self.N2ON_FERT = None

        # Instantiate variables
        self.n2OToAirInorganicFertilizerDirect = None
        self.n2OToAirOrganicFertilizerDirect = None

        # Define model coeffients
        self.ConvMol_N2ON_N2O = ATOMIC_WEIGHT_CONVERSIONS['Conv_Mol_N2ON_N2O']
        self.EF_ExcrSO_N_N2O = 0.01 * ATOMIC_WEIGHT_CONVERSIONS['Conv_Mol_N2ON_N2O']
        self.EF_ExcrCPP_N_N2O = 0.02 * ATOMIC_WEIGHT_CONVERSIONS['Conv_Mol_N2ON_N2O']
        self.N2ON_FERT_lookup = format_lookup('crop.csv', 'n2on_fert')

    def calculate_n2OToAirInorganicFertilizerDirect(self):

        # If the cycle is producing rice
        self.n2OToAirInorganicFertilizerDirect = self.inorgN_total * self.N2ON_FERT * self.ConvMol_N2ON_N2O

        return self.n2OToAirInorganicFertilizerDirect

    def calculate_n2OToAirOrganicFertilizerDirect(self):

        # If the cycle is producing rice
        self.n2OToAirOrganicFertilizerDirect = self.orgN_total * self.N2ON_FERT * self.ConvMol_N2ON_N2O

        return self.n2OToAirOrganicFertilizerDirect

    def calculate_n2OToAirExcretaDirect(self):

        # If the cycle is producing rice
        if 'sheep' in self.products or 'goat' in self.products:
            self.n2OToAirExcretaDirect = self.excretaN_total * self.EF_ExcrSO_N_N2O
        else:
            self.n2OToAirExcretaDirect = self.excretaN_total * self.EF_ExcrCPP_N_N2O

        return self.n2OToAirExcretaDirect

    def complete(self, completeness):

        self.inorgN_total = 0 if self.inorgN_total == {} and completeness['fertilizer'] else self.inorgN_total
        self.orgN_total = 0 if self.orgN_total == {} and completeness['fertilizer'] else self.orgN_total
        self.excretaN_total = 0 if self.excretaN_total == {} and completeness['products'] else self.excretaN_total

    def check_n2OToAirInorganicFertilizerDirect(self, cycle):

        # Calculate total inorganic N fertilizer input

        inputs = cycle['inputs'].evalues()

        self.inorgN_total = summation([sum(input['value']) if 'units' in input['term'] and
                                                              input['term']['units'] == 'kg N' and
                                                              input['term']['termType'] == 'inorganicFertilizer'
                                       else 0 for input in inputs])
        self.products = cycle['products']

        product = primary_product(cycle['products']) if cycle['products'] != {} else {}
        self.N2ON_FERT = self.N2ON_FERT_lookup.get(product['term']['@id'], {}) if product != {} else {}

        self.complete(cycle['dataCompleteness'])

        return self.inorgN_total != {} and len(self.products) > 0 and self.N2ON_FERT != {}

    def check_n2OToAirOrganicFertilizerDirect(self, cycle):

        # Calculate total organic N fertilizer input

        inputs = cycle['inputs'].evalues()

        N_total_converted = [convert_to_n(input)
                             if 'units' in input['term'] and
                                input['term']['units'] == 'kg' and
                                input['term']['termType'] == 'organicFertilizer'
                             else {} for input in inputs]

        N_total_direct = [sum(input['value']) if 'units' in input['term'] and input['term']['units'] == 'kg N' and
                                                 input['term']['termType'] == 'organicFertilizer' else {}
                          for input in inputs]

        self.orgN_total = summation(N_total_converted + N_total_direct)

        self.products = cycle['products']

        product = primary_product(cycle['products']) if cycle['products'] != {} else {}
        self.N2ON_FERT = self.N2ON_FERT_lookup.get(product['term']['@id'], {}) if product != {} else {}

        self.complete(cycle['dataCompleteness'])

        return self.orgN_total != {} and len(self.products) > 0 and self.N2ON_FERT != {}

    def check_n2OToAirExcretaDirect(self, cycle):

        # Calculate total Excreta product

        inputs = cycle['inputs'].evalues()

        self.excretaN_total = summation([sum(input['value']) if 'units' in input['term'] and
                                                                input['term']['units'] == 'kg N' and
                                                                input['term']['termType'] == 'animalProduct' else 0
                                         for input in inputs])

        self.products = cycle['products']

        self.complete(cycle['dataCompleteness'])

        return self.excretaN_total != {} and len(self.products) > 0
