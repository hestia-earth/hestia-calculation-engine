# N2O, to air, fuel combustion, EMEA-EEA (2019)

This model calculates the N2O emissions from fuel combustion in engines (excluding the emission related to producing or extracting the fuel), using the emissions factors in the [EMEP-EEA Handbook (2019, Part B, Chapter 1.A.4, page 22)](https://www.eea.europa.eu/publications/emep-eea-guidebook-2019/part-b-sectoral-guidance-chapters/1-energy/1-a-combustion/1-a-4-non-road-1/view).

## Calculates

- [emission.value](https://hestia.earth/schema/Emission#value) as [n2OToAirFuelCombustionEmea-Eea2019](https://hestia.earth/term/n2OToAirFuelCombustionEmea-Eea2019)

## Requirements

- [input.value](https://www.hestia.earth/schema/Input#value) WITH
[diesel](https://hestia.earth/term/diesel)
OR [diesel1D](https://hestia.earth/term/diesel1D)
OR [diesel4D](https://hestia.earth/term/diesel4D)
OR [diesel2D](https://hestia.earth/term/diesel2D)
OR [en590Diesel](https://hestia.earth/term/en590Diesel)
OR [motorGasoline](https://hestia.earth/term/motorGasoline)
OR [gasoline-TypeJetFuel](https://hestia.earth/term/gasoline-TypeJetFuel)
OR [aviationGasoline](https://hestia.earth/term/aviationGasoline)

## Implementation Status

- `Implemented`
- [Tested](../../../tests/tests_emissions/test_n2OToAirFuelCombustionEMEAEEA2019.py)
