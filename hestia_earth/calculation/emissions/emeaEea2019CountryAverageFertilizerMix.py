from hestia_earth.calculation.abstract_model import Model
from hestia_earth.calculation.data.constants.nh3 import NH3_FERTILIZER_FACTOR
from hestia_earth.calculation.utils import most_relevant_measurement, summation, download_lookup
from hestia_earth.utils.api import download_hestia

MODEL_KEY = 'emeaEea2019CountryAverageFertilizerMix'

NO_PH = 0


class EmeaEea2019CountryAverageFertilizerMix(Model):
    def __init__(self):

        # Define model tier
        self.tier = 1
        self.term = download_hestia(MODEL_KEY)

        # Define model requirements
        self.soilPh = None
        self.temperature = None
        self.inorganicNitrogenFertilizerUnspecifiedAsN = None
        self.breakdown_country = None

        # Instantiate variables
        self.nh3ToAirInorganicFertilizer = None

        # Define model coeffients
        self.emf_lookup = NH3_FERTILIZER_FACTOR
        self.inorg_fert_lookup = download_lookup('region-inorganicFertilizerBreakdown.csv')

    def calculate_nh3ToAirInorganicFertilizer(self):
        # Calculate total nh3 emissions

        emfs = self.get_emissions_mineral_fertilizer(self.soilPh, self.temperature)

        n_inputs = [
            self.inorganicNitrogenFertilizerUnspecifiedAsN * self.breakdown_country['urea_uas_amm_bicarb'][0],
            self.inorganicNitrogenFertilizerUnspecifiedAsN * self.breakdown_country['as'][0],
            self.inorganicNitrogenFertilizerUnspecifiedAsN * self.breakdown_country['uan_solu'][0],
            self.inorganicNitrogenFertilizerUnspecifiedAsN * self.breakdown_country['an_np_kn_npk'][0],
            self.inorganicNitrogenFertilizerUnspecifiedAsN * self.breakdown_country['can'][0],
            self.inorganicNitrogenFertilizerUnspecifiedAsN * self.breakdown_country['anha_aquaa'][0],
            self.inorganicNitrogenFertilizerUnspecifiedAsN * self.breakdown_country['ap_dap_map'][0]
            ]

        self.nh3ToAirInorganicFertilizer = sum([x*y for x, y in zip(n_inputs, emfs)])

        return self.nh3ToAirInorganicFertilizer

    def get_emissions_mineral_fertilizer(self, phi, temperature):

        ph = phi if phi != {} else NO_PH

        conditions = [ph <= 7 and temperature <= 14,
                      ph <= 7 and temperature > 14 and temperature < 26,
                      ph <= 7 and temperature >= 26,
                      ph > 7 and temperature <= 14,
                      ph > 7 and temperature > 14 and temperature < 26,
                      ph > 7 and temperature >= 26]

        factors = [self.emf_lookup['soil ph < 7']['cool T < 14°C'].values(),
                   self.emf_lookup['soil ph < 7']['room temperature  15°C < T < 25°C'].values(),
                   self.emf_lookup['soil ph < 7']['warm T >= 26°C'].values(),
                   self.emf_lookup['soil ph >= 7']['cool T < 14°C'].values(),
                   self.emf_lookup['soil ph >= 7']['room temperature  15°C < T < 25°C'].values(),
                   self.emf_lookup['soil ph >= 7']['warm T >= 26°C'].values()]

        return factors[conditions.index(True)]

    def complete(self, completeness):

        self.inorganicNitrogenFertilizerUnspecifiedAsN = 0 \
            if self.inorganicNitrogenFertilizerUnspecifiedAsN == {} and completeness['fertilizer']\
            else self.inorganicNitrogenFertilizerUnspecifiedAsN

    def check_nh3ToAirInorganicFertilizer(self, cycle):
        # Check that we have all the inputs

        annual_temperature =\
            most_relevant_measurement(cycle['site']['measurements']['temperatureAnnual'],
                                      cycle['endDate'])
        longterm_temperature =\
            most_relevant_measurement(cycle['site']['measurements']['temperatureLongTermAnnualMean'],
                                      cycle['endDate'])
        self.temperature = annual_temperature if annual_temperature != {} else\
            longterm_temperature if longterm_temperature != {} else {}
        self.soilPh = most_relevant_measurement(cycle['site']['measurements']['soilPh'], cycle['endDate'])
        self.inorganicNitrogenFertilizerUnspecifiedAsN =\
            summation(cycle['inputs']['inorganicNitrogenFertilizerUnspecifiedAsN']['value'])

        self.breakdown_country =\
            self.inorg_fert_lookup[self.inorg_fert_lookup['termid'] == cycle['site']['country']['@id']]

        self.complete(cycle['dataCompleteness'])

        return self.inorganicNitrogenFertilizerUnspecifiedAsN != {} and self.temperature != {} and \
            len(self.breakdown_country) > 0
