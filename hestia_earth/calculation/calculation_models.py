from hestia_earth.calculation.abstract_model import Model


class Assesment_model(Model):
    def __init__(self):

        # Define model id
        self.lookup = None
        self.results = {}

    def calculate(self):
        result = 0
        for emissionsResourceUse in self.emissionsResourceUses:
            iden = emissionsResourceUse['term']['@id'] if '@id' in emissionsResourceUse['term'] else None
            if iden in self.lookup:
                result += emissionsResourceUse['value'] * float(self.lookup[iden])

        return result

    def check(self, cycle, product, emissionsResourceUses):
        self.representation_cycle = cycle
        self.product = product
        self.emissionsResourceUses = emissionsResourceUses.evalues()

        return len(self.emissionsResourceUses) > 0
