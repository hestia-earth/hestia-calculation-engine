from hestia_earth.calculation.calculation_models import Assesment_model
from hestia_earth.calculation.utils import format_lookup

MODEL_KEY = 'co2EqGwp100IncludingClimate-CarbonFeedbacksIpcc2013'


class Co2EqGwp100IncludingClimateCarbonFeedbacksIpcc2013(Assesment_model):
    def __init__(self):

        super().__init__()

        # Model id
        self.id = MODEL_KEY

        # Define model coefficients
        self.lookup = format_lookup('emission.csv', 'co2eqgwp100includingclimatecarbonfeedbacksipcc2013')
