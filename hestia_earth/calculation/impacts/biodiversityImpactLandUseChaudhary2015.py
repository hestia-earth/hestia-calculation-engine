from hestia_earth.calculation.abstract_model import Model
from hestia_earth.calculation.utils import parse_float
from hestia_earth.utils.lookup import get_table_value, download_lookup
from hestia_earth.calculation.data.lookups import load_lookup

MODEL_KEY = 'speciesDestinedToExtinctionPerYearBiodiversityLossPermanentEffectsLc-Impact'


class BiodiversityImpactLandUseChaudhary2015(Model):
    def __init__(self):

        super().__init__()

        # Model id
        self.id = MODEL_KEY

        # Define model requirements
        self.country = None
        self.ecoregion = None
        self.prod_id = None
        self.economicValueShare = None
        self.land_ocupation = None

        # Instantiate variables
        self.biodiversityImpact = None

        # look up tables
        self.ecoregion_lookup = download_lookup('ecoregion.csv')
        self.region_lookup = download_lookup('region.csv')
        self.crop_lookup = download_lookup('crop.csv')

        self.look_up_biodiversity_country_LUC =\
            load_lookup('Biodiversity_LandUseChange_PercentPerYear_BlonkConsultants2014.csv')

    def calculate(self):
        # TODO: check if emissionsResourceUse should be called like this, currently called from within cycle
        # TODO: How to deal with products with multiple land use types for a single product, for example, beef.
        # Calculate biodiversity impacts (Chaudhary 2015)

        # 1) Estimate land occupation biodiversity impact - Potential global species loss due to land use occupation

        # Identify the broad land use category (cropGrouping) based on crop type
        # TODO: Add a catch to break if greater than 1 land use type here
        LU_type =\
            get_table_value(self.crop_lookup, 'termid', self.prod_id, 'cropgroupingfao').replace(" ", "_").lower()

        # use ecoregion if available, else use country and extract occupation CF
        if self.ecoregion != {}:
            loc_1 = self.ecoregion
            # Extract the occCF for the ecoregion and broad land use in question.
            LU_column_name = LU_type + '_taxa_aggregated_median_occupation'
            temp_occCF = parse_float(get_table_value(self.ecoregion_lookup, 'termid', loc_1,
                                                     LU_column_name))
        else:
            loc_1 = self.country
            # Extract the occCF for the ecoregion and broad land use in question.
            LU_column_name = LU_type + '_taxa_aggregated_median_occupation'
            temp_occCF = parse_float(get_table_value(self.region_lookup, 'termid', loc_1,
                                                     LU_column_name))

        # occBioImpact = occCF * landOccupation (m2*year of land needed to produce 1kg of the product)
        occBioImpact = self.land_ocupation * temp_occCF

        # TODO: Pasture land use change calculated as sum of temp. and perm. pasture land use change percent

        # Pull the transCF for the ecoregion/region and land use in question
        if self.ecoregion != {}:
            # Extract the occCF for the ecoregion and broad land use in question.
            LU_column_name_transformation = LU_type + '_taxa_aggregated_median_transformation'
            temp_transCF = parse_float(get_table_value(self.ecoregion_lookup, 'termid', loc_1,
                                                       LU_column_name_transformation))
        else:
            # Extract the occCF for the ecoregion and broad land use in question.
            LU_column_name_transformation = LU_type + '_taxa_aggregated_median_transformation'
            temp_transCF = parse_float(get_table_value(self.region_lookup, 'termid', loc_1,
                                                       LU_column_name_transformation))

        # self.transCF_region = (landOccupation * proportion of land converted) * temp_transCF
        # Estimate transformation impact.
        transBioImpact = self.land_ocupation * self.land_use_change * temp_transCF

        # 3) Estimate potential global species loss due to land use impacts.
        self.biodiversityImpact = occBioImpact + transBioImpact  # Sum the two impacts to calculaet the overall
        # biodiversity impact

        # return [self.biodiversity_M1, self.occCF_region, self.transCF_region]
        # create hierarchies resource use node
        # impacts = {}

        # impacts[
        #    'speciesDestinedToExtinctionPerYearBiodiversityLossPermanentEffectsLc-Impact'] = biodiversityImpact
        # emissionsResourceUses[
        #    'speciesDestinedToExtinctionPerYearBiodiversityLossLandOccupationLc-Impact'] = occBioImpact
        # emissionsResourceUses[
        #    'speciesDestinedToExtinctionPerYearBiodiversityLossLandTransformationLc-Impact'] = transBioImpact
        return self.biodiversityImpact  # return the biodiversity impact value

    def check(self, cycle, product, emissionsresourceuse):

        # extract landOccupation - if it exists
        self.country = cycle['site']['country']['@id'] if cycle != {} else {}
        self.ecoregion = cycle['site']['ecoregion'] if cycle != {} else {}
        self.prod_id = product['term']['@id'] if cycle != {} else {}
        self.economicValueShare = product['economicValueShare'] if 'economicValueShare' in product else {}

        self.land_ocupation = emissionsresourceuse['landOccupation']['value']
        self.land_use_change = emissionsresourceuse['landTransformation20YearAverage']['value']

        # if we have landOccupation data and the product has an EVS > 0 then pass the check
        return self.prod_id != {} and self.land_ocupation != {} and self.country != {} and\
            self.land_use_change != {} and self.economicValueShare != {} and self.economicValueShare > 0
