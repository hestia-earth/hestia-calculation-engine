from hestia_earth.calculation.calculation_models import Assesment_model
from hestia_earth.calculation.utils import format_lookup

MODEL_KEY = 'po4-EqEutrophicationExcludingFateCml2001Baseline'


class Po4_EqEutrophicationExcludingFateCml2001Baseline(Assesment_model):
    def __init__(self):

        super().__init__()

        # Model id
        self.id = MODEL_KEY

        # Define model coefficients
        self.lookup = format_lookup('emission.csv', 'po4eqeutrophicationexcludingfatecml2001baseline')
