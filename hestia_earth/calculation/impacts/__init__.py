IMPACTS_ORDER = [
    'co2EqGwp100ExcludingClimateCarbonFeedbacksIpcc2013',
    'co2EqGwp100IncludingClimateCarbonFeedbacksIpcc2013',
    'po4EqEutrophicationExcludingFateCml2001Baseline',
    'noxeqEutrophicationIncludingFateAverageEuropeCml2001NonBaseline',
    'so2EqTerrestrialAcidificationExcludingFateCml2001NonBaseline',
    'so2EqTerrestrialAcidificationIncludingFateAverageEuropeCml2001Baseline',
    'biodiversityImpactLandUseChaudhary2015',
    'leqScarcityWeightedWaterUseAware'
]
