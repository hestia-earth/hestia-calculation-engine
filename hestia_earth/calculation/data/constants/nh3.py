NH3_FERTILIZER_FACTOR = {
    'soil ph < 7': {
        'cool T < 14°C': {
            'Urea, UAS, Amm Bicarb': 0.155,
            'AS': 0.09,
            'UAN Solu': 0.098,
            'AN, ACl, NP, NPK': 0.038,
            'CAN': 0.008,
            'AnhA, AquaA': 0.019,
            'AP, DAP, MAP': 0.05
        },
        'room temperature  15°C < T < 25°C': {
            'Urea, UAS, Amm Bicarb': 0.159,
            'AS':	0.092,
            'UAN Solu':	0.1,
            'AN, ACl, NP, NPK':	0.052,
            'CAN':	0.008,
            'AnhA, AquaA':	0.02,
            'AP, DAP, MAP':	0.067
        },
        'warm T >= 26°C': {
            'Urea, UAS, Amm Bicarb': 0.198,
            'AS': 0.115,
            'UAN Solu': 0.126,
            'AN, ACl, NP, NPK': 0.049,
            'CAN': 0.01,
            'AnhA, AquaA': 0.025,
            'AP, DAP, MAP': 0.064
        }
    },
    'soil ph >= 7': {
        'cool T < 14°C': {
            'Urea, UAS, Amm Bicarb': 0.164,
            'AS': 0.165,
            'UAN Solu': 0.095,
            'AN, ACl, NP, NPK': 0.071,
            'CAN': 0.017,
            'AnhA, AquaA': 0.035,
            'AP, DAP, MAP': 0.091
        },
        'room temperature  15°C < T < 25°C': {
            'Urea, UAS, Amm Bicarb': 0.168,
            'AS': 0.17,
            'UAN Solu': 0.097,
            'AN, ACl, NP, NPK': 0.074,
            'CAN': 0.017,
            'AnhA, AquaA': 0.036,
            'AP, DAP, MAP': 0.074
        },
        'warm T >= 26°C': {
            'Urea, UAS, Amm Bicarb': 0.21,
            'AS': 0.212,
            'UAN Solu': 0.122,
            'AN, ACl, NP, NPK': 0.092,
            'CAN': 0.021,
            'AnhA, AquaA': 0.046,
            'AP, DAP, MAP': 0.117
        }
    }
}

NH3_TAN_FACTOR = {
    'Liquid, Slurry & Sewage': 0.307877242878561,
    'Solid': 0.685083144186046,
    'Compost': 0.710000000000000,
    'Grn Manr.': 0
}

DRY_MATTER_FACTOR_TO_NH3 = 2.17/1000

CONV_ORGFERT_N_TAN = [0.6054877046164920,
                      0.1156,
                      0.107916666666667,
                      0]
