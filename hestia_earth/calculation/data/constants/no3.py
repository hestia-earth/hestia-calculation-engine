NO3_LEECHING_FACTORS = {
    'high': 0.23,
    'low': 0.067,
    'pasture': 0.07,
    'other': 0.12,
    'flooded_rice': 0.035
}
