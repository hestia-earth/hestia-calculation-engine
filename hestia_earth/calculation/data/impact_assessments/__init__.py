import os
import sys

IMPACT_ASSESSMENTS_DIR = os.path.dirname(os.path.abspath(__file__)) + "/"
sys.path.append(IMPACT_ASSESSMENTS_DIR)
