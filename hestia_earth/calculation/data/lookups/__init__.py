import os
import sys
from hestia_earth.utils.lookup import load_lookup as _load_lookup

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__)) + '/'
sys.path.append(CURRENT_DIR)


def load_lookup(filename: str):
    return _load_lookup(os.path.join(CURRENT_DIR, filename))
