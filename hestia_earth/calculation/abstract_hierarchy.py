import math
from collections import OrderedDict
from hestia_earth.schema import TermJSONLD


class Hierarchy():
    """Model_Hierarchy class. It contains a set of common methods and instance variables that are typically used by
    every hierarchy. A hierarchy defines the order to run models in Hestia."""

    context = None

    def __init__(self):
        """Constructor method.
            Arguments:
                None
            Returns instance."""

        self.chosen_model = None

    def get_reference(self, term):

        reference = TermJSONLD()

        reference.fields = OrderedDict(list(reference.fields.items()))
        reference.fields['@id'] = term['@id'] if '@id' in term else None
        reference.fields['@type'] = term['@type'] if '@type' in term else None
        reference.fields['name'] = term['name'] if 'name' in term else None
        reference.fields['termType'] = term['termType'] if 'termType' in term else None
        reference.fields['units'] = term['units'] if 'units' in term else None

        return reference.to_dict()

    def process_hierarchy(self, cycle):
        """Method that procesess the hierarchy through all the models for a given cycle. It is overwritten in for
        different kind of engines
            Arguments:
                cycle: This is a cycle in the representation required by the engine.
                (Different from the Schema representation)
            Returns a file object."""

        pass

    def round_to_significant(self, number, sig_number):
        """Method to round to a given number of significant numbers.
            Arguments:
                number: The number to round.
                sig_number: The number of significant numbers.
            Returns The rounded number."""
        return round(number, sig_number - int(math.floor(math.log10(abs(number)))) - 1) if number != 0 else 0

    def update_representation(self, cycle):
        """Method that checks if the model has enough data to do the calculation.
            Arguments:
                cycle: This is a cycle in the representation required by the calculation engine.
                (Different from the Schema representation)
            Returns boolean."""

        pass
