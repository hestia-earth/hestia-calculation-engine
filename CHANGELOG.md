# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [0.1.0](https://gitlab.com/hestia-earth/hestia-calculation-engine/compare/v0.0.16...v0.1.0) (2021-04-10)


### Features

* access primary product by key ([b5f3834](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/b5f38344569f85217501ded17df379a361fb4ff9))
* add AWARE100 scarcity water impact assessment ([e10868f](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/e10868f359da1248ced40cba53d3a3141398b695)), closes [#61](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/61)
* add leqScarcityWeightedWaterUseAware model to impacts hierarchy ([0cbbfaf](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/0cbbfaf86a9f8205b2f894be0393adeaf1977637))
* add method term to recalculated emissions ([9d86428](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/9d86428d53e98c66b39397e89dd83ef37736857d))
* add remaining combustion models ([c16dfb2](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/c16dfb2869d8650047e6a579dfec813d87faaf15))
* do not creat assessments for cropResidue products ([2f1cdf0](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/2f1cdf006edf9e5e45e8809e0e0c643f8c50a249))
* get practiceFactor from lookup table ([81acebb](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/81acebbef864880c904b7025476126b85b27975d))
* handle aboveGroundCropResidueIncorporatedDryMatter in calculations ([ab4eb59](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/ab4eb59b6ea695ac920af5201ee5a923bab840a4))
* handle cycles with sparse data storage for p models ([b07e180](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/b07e180af75611094f97fd80523d6656b7d88fbd))
* handle dataCompleteness in emissionsResourceUse models ([98ddaf9](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/98ddaf9339e11c487c91dbd3477e5976cafcd771))
* handle dataCompleteness in impactassessment models ([b368af8](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/b368af8d756c250837efa3523a874ee07a8623a7))
* handle sparse storage data in co2ToAirLimeAndUreaIpcc2006 model ([c2d9baa](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/c2d9baa44199e17957b33478587b22b200879ab2))
* handle sparse storage data in combustion models ([0e7d9c1](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/0e7d9c12fbddff892a0536ba2e1f5403262feec8))
* handle sparse storage data in crop residue burning models ([df4013c](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/df4013cc8bb920fb765e9c272b5816d01c9859c3))
* handle sparse storage data in erosion models ([3de43ab](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/3de43abffcfda119265bfe758b3c255cc45557b3))
* handle sparse storage data in model n2OToAirFertilizerAndExcretadirectStehfestBouwman2006 ([90147f5](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/90147f563ea529b65b9c10e1769548652d29cefa))
* handle sparse storage data in n2OToAirFertilizerAndExcretaDirectIpcc2006 model ([49ae267](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/49ae2679191106e8b126caa1ceeb20e852ddea6b))
* handle sparse storage data in n2OToAirFertilizerAndExcretaIndirectIpcc2006 model ([03a7a38](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/03a7a38ebf7f0f29bf01f6e66e1dc1bec83a0757))
* handle sparse storage data in nh3ToAirCropResidueDecompositionDeRuijterEtAl2010 model ([bc50115](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/bc5011509601cb41cdbba0abc1e44654467e5829))
* handle sparse storage in nh3ToAirInorganicFertilizerEuropeanEnvironmentAgency2016 model ([cde88e4](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/cde88e41599bf7ba7204ffec2b51a475b6783cf7))
* handle sparse storage in nh3ToAirOrganicFertilizerWebbEtAl2012AndSintermannEtAl2012 model ([b850236](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/b85023676ea1d0c4d2c460761678d7c3ecbcc050))
* handle sparse storage in no3ToGroundwaterAllInputsPooreNemecek2018 model ([d58584e](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/d58584e1829f61cd85d81b65d062fe99cea1c367))
* handle sparse storage in noxToAirFertilizerAndExcretaStehfestBouwman2006 model ([a552453](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/a5524536e21c744303caaa4fa3f166151bc42023))
* handle sparse storage in noxToAirFertilizerAndExcretaStehfestBouwman2006Tier1 model ([09af354](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/09af3546a8f292a551924ced69229d6bb340b8e2))
* include zero emissions in the JSONLD file ([b163896](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/b1638963954592cc82a70dab663a000abcb88fb2)), closes [#106](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/106)
* only use light links in the JSONLD file ([e3c2add](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/e3c2add6979bc8c3b4675d7e02d491694c046b6c)), closes [#110](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/110)
* update calc engine to schema 2.10.0 ([859aba8](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/859aba8492bb2714a4fa513c1d32a5983f0ce779))
* **log:** add logging module ([b75c367](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/b75c3676bac5d8dcf50b6ffb15670906bcf7ca7b))


### Bug Fixes

* add extra recalculated keys due to schema requirements ([c46b098](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/c46b0984e230c438a1f40a7cb7974d6dabf1f4f3))
* calculate properly cycles with multiple rootingDepths ([e7fd6de](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/e7fd6de9fce84ee1e787d973666733765c167ed0))
* check that the product is in the lookup ([4d0f6aa](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/4d0f6aaf159f5c416ebe8c5667dce9d58ed962b0)), closes [#104](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/104)
* dataCompleteness in model n2OToAirFertilizerAndExcretadirectStehfestBouwman2006 ([259a61f](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/259a61f0d4f4f11e69b9af5999ae06da3163f845))
* do not divide by zero in n2OToAirFertilizerAndExcretadirectStehfestBouwman2006 model ([ba88198](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/ba88198ddf8b7f67d5386d0cb615d1608342d3a5))
* do not run the biodiversity model without a country ([8fd0f73](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/8fd0f73c80da531d58a451438d3fd000eaf783f9)), closes [#109](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/109)
* do not run the model without a product value ([e920df3](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/e920df3dacdd1cdc2af38a571f7cf97282e07075)), closes [#108](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/108)
* do not run the model without products ([fa02ec0](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/fa02ec01d92532b53c8438d6f42f2cbfb0915251))
* fix bug in nh3ToAirInorganicFertilizerEuropeanEnvironmentAgency2016 model ([967e09b](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/967e09b8e73c6b36849124a36d8d0429fb99c820))
* fix check conditions to take into account dataCompleteness feature ([58b5285](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/58b52853d3d2fc432253916dd5dfa35a5230c951))
* fix dataCompleteness bug in n2OToAirFertilizerAndExcretaIndirectIpcc2006 model ([9c83807](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/9c8380708e58896a1b18aadcbbf5f6dcecfa7aac))
* fix model key ([620b409](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/620b409d42dd7eb2c68cedd0de3d5e25fefcd0f9))
* fix models for reconciliation ([94c45d9](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/94c45d9b3e054f2ebee3ee24ddefd1cd07f3fffd))
* fix relative path in biodiversity model ([8b137b8](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/8b137b85892ccd004f63a01a4091dd4ba328b151))
* handle case with no value in rootingDepth ([29d5427](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/29d5427fedb143ea34b540265218aae25c70d719)), closes [#114](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/114)
* handle compostAsN and mushroomCompostAsN as a list in pToSurfacewaterSalcaPrasuhn2006 model ([cd2aaa1](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/cd2aaa12016ed07bdaaae493623b893b1a796a85))
* handle list inputs in pmodels ([3b6df76](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/3b6df763ad89c5ba837678b023078375fe205e5b))
* product amount has to be greater than zero to carry out the calculation ([96864ac](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/96864acda10b4fc896ee57c386614d9583a82e62))
* re-scale irrigation in erosion models ([cb820a3](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/cb820a30e1b31672f26dd943f86afbae3df9feb2))
* revert accidental commit and push ([7bbf505](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/7bbf505d1e4e63058d5c740a6030fa9ea2a3aae5))
* **biodiversityImpactLandUseChaudhary2015:** update model to use glossary based lookup and crop-country specific land use change estimates ([dc98a76](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/dc98a767d0b34bd38e569fa8ebe5bb54ac61a005)), closes [#91](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/91)
* **lookups:** update land use change data to match product names and country id ([0a2cfdf](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/0a2cfdfd50fa8df8e4564caa7aa8f03772fddf19))
* **summation:** fix summation partial empty issue ([7ef2339](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/7ef2339be7d6c76474dd528b8e460c509133f6a2))
* **utils:** return empty dictionary when there is no date in most_relevant_measurements ([956291b](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/956291b94bc16611514de0b25779039b5b95bf4a)), closes [#101](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/101)

### [0.0.16](https://gitlab.com/hestia-earth/hestia-calculation-engine/compare/v0.0.15...v0.0.16) (2021-02-01)


### Features

* allow selective assessments calculations ([4af90e0](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/4af90e0c72c9625d8f69586fd0361d682972b68f))
* pick most recent measurements ([9738e62](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/9738e628fda7ef3347413992f505b4cd9a5022bc))
* pick up most relevant measurement ([f9bbe8d](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/f9bbe8d6e8a195353e2a296cc6b06dc18c73a8a5))
* remove context from non main nodes ([ac7e284](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/ac7e2847c5d826cd9cdddfaeaf9e545f7dbf21af))
* **calculation:** Add biodiversity Chaudhary 2015 model ([44fafc0](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/44fafc0835a9d03aa20034dbffd9be91d7ca8111)), closes [#74](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/74)
* **utils:** pick shallowest measurements always ([5fdefe7](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/5fdefe770150dab203d914f8a2cd7615ed3c59d2))


### Bug Fixes

* allow assessments without cycle ([a2b7479](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/a2b747928c8551c6f3d55d7a89e5359a7ab1ae8b))
* fix bug processing assessment with no cycle ([8219296](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/821929677d52a784b6229de88ba534390b66abdd))
* take into account that Ph might be provided for different depths ([f900c09](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/f900c0964d3d09ecc9a39d3d826d38890dbd8ed9)), closes [#88](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/88)
* **assessment_hierarchies:** avoid side effects while importing assessments ([8c649e6](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/8c649e63c99d22713bc087c248ff2978a3af232a))
* **utils:** avoid crash with empty measurements lists ordering ([db44c0c](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/db44c0ca370bd7a30b1347525cc51a45509a749a))

### [0.0.15](https://gitlab.com/hestia-earth/hestia-calculation-engine/compare/v0.0.14...v0.0.15) (2021-01-22)


### Features

* add additional impact assessments ([32233bb](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/32233bbcaaa5b68ed85054e7851056af558f8a4d))
* add fresh water midpoint calculation ([43aaf05](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/43aaf057c13899ff5863d01fcabfda007da9403b))
* add multi-product assessments ([bb74178](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/bb7417805c820a357fa11d93d47dadc2cf21b073))
* add so2EqTerrestrialAcidificationExcludingFateCml2001NonBaseline midpoint calculation ([72755c6](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/72755c6a22cba7ba9cf8fdd6bd513dd91d31de21))
* allow cycle import from assessment ([f3c79c9](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/f3c79c9f4fcda9e79f2f17bdfb6de95857a37239))
* allow to run models individually ([89b3f44](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/89b3f4457cd00377e46c992ca9f71972fde949bb))
* build indicators in parallel ([f33e310](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/f33e310e3bae5ef7ed6608509b2a6bf51a35b494))
* handle the case where there is an uploaded assessment ([2e3b858](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/2e3b85874de4578750af96b003aed86968089d53)), closes [#78](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/78)
* hierarchy of emissionsResourceUse models ([5bc1470](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/5bc147063e45976e5bc0d5189c01573de14d80ca))
* keep cycle original nesting ([5f2dd6d](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/5f2dd6d0d426ed117cf0e665599d239c0073fb51))
* only recalculate cycle if no assessment is provided ([a5105c6](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/a5105c650dff7235d32f121a2d824270f954a5ac))
* **assessment_hierarchies:** update assessment hierarchies with new models ([96c859f](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/96c859f9b8e4a0211741139f10aa4f1f0de4a3e0))
* **calculation:** Add landOccupation calculation ([7ed466c](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/7ed466c144a073dbdc9fd23ea0ef3cbfafe3b14c)), closes [#76](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/76)


### Bug Fixes

* allow cycles with no products ([c55c8a9](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/c55c8a9e1b225704ea520050735648d4f9a64198))
* fix aws multiprocessing bug ([bce3aa8](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/bce3aa8e2bc6c676b25380854b50ef89b6d44f75))
* output only with standard python elements ([a53833c](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/a53833c61534e37617ef0da132bd6c810a5312a4))
* update schema version ([a5baa8d](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/a5baa8d398e7261c9bf25815f84ae17d3a0e8fc7))
* **abstract_engine:** cycle key argument has priority over the cycle in assessment ([cdc11db](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/cdc11dbe7209d6b88e9eae0ed051e38cbf0a5caa))
* **glossary match:** update to match glossary ([8db4ed8](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/8db4ed84e4782f47a56cb3f3a3c728f406d2272b)), closes [#76](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/76)
* **glossary match:** Update to match new glossary term ([bde712b](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/bde712b707eafbcaff90351c348cb9e89aaaae25)), closes [#76](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/76)
* **lookup:** Update the Chaudhary 2015 biodiversity CFs ([ff21a97](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/ff21a97419b710aac44af1a10d9c09b35dd2b38c)), closes [#74](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/74)
* **po4EqEutrophicationExcludingFateCml2001Baseline:** fix id name ([ba9f94c](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/ba9f94ceabcef73c1ef0753f5a7e967d0bf3c302))
* **utils:** allow empty assessments array ([d493833](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/d49383352575cfed8ec918d0c223419ebc93dc21))

### [0.0.14](https://gitlab.com/hestia-earth/hestia-calculation-engine/compare/v0.0.13...v0.0.14) (2020-12-18)


### Features

* **calculation:** automatic import of ordered hierarchies ([51775c9](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/51775c9fd3b013bb1b86580a0e340ce8194838cb))
* **utils:** add download lookup script ([4c04592](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/4c04592339f5553d53c063a00b0c223d6f0b4b5f))


### Bug Fixes

* fix no site bug ([badf241](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/badf24191fba617ad9fcc23eab393216108d7a0a))
* **utils:** handle error when fetching node through API ([c835d2d](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/c835d2dc21e91e9e724eb1934327ded5721581e3))

### [0.0.13](https://gitlab.com/hestia-earth/hestia-calculation-engine/compare/v0.0.12...v0.0.13) (2020-12-05)


### Features

* add co2EqGwp100IncludingClimateCarbonFeedbacksIpcc2013 impact assessment ([6ea28a8](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/6ea28a856ae48722029c6140a5f9f1e66a5b8699))
* add n2OToAirFertilizerAndExcretaIndirectIpcc2006 model ([8bdfd09](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/8bdfd09acfe48a46e0aed6ba2342a65edbdd8fa5))
* add p models hierarchies ([5f5f15d](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/5f5f15d3291e610ca15c3f5b141ac785320bbc13))
* check if the emission is different before update ([04ab2a9](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/04ab2a9dfc2c7b7b5c9b226450a3a8ea2ead7280))
* introduce background emissions input references ([40a6c82](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/40a6c825f07b2a8dce9249e7aaa4442c7f77ea0b))


### Bug Fixes

* fix bug in model n2OToAirFertilizerAndExcretaIndirectIpcc2006 ([39fe504](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/39fe50409c273a38c35202e4b526121640efc4ce))
* **abstract_engine:** import properties without term ([259a724](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/259a7247c7140d47febd348154b586a299788609)), closes [#73](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/73)
* **calculation_hierarchies:** define a clear cutoff relative comparison ([bca5799](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/bca57993ba45ef68d4c814717f60d941c02357a1))
* **pErosionSalcaSchererPfister2015:** check that there is slope ([9cfe87f](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/9cfe87f7b1de0f1c5c3a003716a769a60d517248))

### [0.0.12](https://gitlab.com/hestia-earth/hestia-calculation-engine/compare/v0.0.11...v0.0.12) (2020-11-24)


### Bug Fixes

* fix bugs for several models ([a74ee32](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/a74ee32d2b108e40e5729dfb37d38f85a42525ab))

### [0.0.11](https://gitlab.com/hestia-earth/hestia-calculation-engine/compare/v0.0.10...v0.0.11) (2020-11-21)


### Features

* add midpoints calculations ([b1fefc7](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/b1fefc7f3661072a9456c47c62192230917ca9d3))
* add pErosionSalcaSchererPfister2015 model ([3a6ef2c](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/3a6ef2c5ac31bedb1279950ca745aa131130ceeb))
* add pToGroundwaterSalcaPrasuhn2006 model ([7058c8a](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/7058c8acbc38e37ca2f4a5a9315e6bde8d5479b9))
* **pToSurfacewaterSalcaPrasuhn2006:** add pToSurfacewaterSalcaPrasuhn2006 model ([be038c2](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/be038c2d4cc5e24cd577d98f2817e2a7a09063dd))


### Bug Fixes

* update to schema 0.3.0 ([c19a478](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/c19a478f90a4a5662594d701fdc0bc407ede0f9f))
* **calculation_hierarchies:** set empty emissions node as engine dictionary ([9dac9cf](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/9dac9cf6051edc22954e60623e8f621c315f4a6b))

### [0.0.10](https://gitlab.com/hestia-earth/hestia-calculation-engine/compare/v0.0.9...v0.0.10) (2020-11-06)


### Features

* add background emissions ([4b9fe7e](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/4b9fe7ea330368cce7678bb2a658bb30e5facc2b))
* add extra background emissions ([8734d22](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/8734d221bc590c8b7328ba2bc648b0c60397ec2a))
* allow parallel update ([606a94a](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/606a94a26e39d4714410582d5d7c02c612735ec2))
* set output with a given number of significant figures ([e889aa5](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/e889aa548a85fc365a15eb0651d99ae28ab03387))


### Bug Fixes

* fix bug in n2OToAirFertilizerAndExcretadirectStehfestBouwman2006 model ([598e870](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/598e87039f6031674ff1a0b69ca72b763b1c2922))
* fix bug in n2OToAirFertilizerAndExcretadirectStehfestBouwman2006 model ([e1d698e](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/e1d698e38000f2c100e31778e07214fb40fe0370))
* fix output inconsistencies ([89ebfad](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/89ebfada96124170f5b2e3b3b679707b4963cf97)), closes [#30](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/30) [#65](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/65)
* **models:** replace pandas Series with numpy array ([730fbef](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/730fbeff4705b6c76395c2593efddba399bc5155))
* **no3ToGroundwaterAllInputsPooreNemecek2018:** fix no3ToGroundwaterAllInputsPooreNemecek2018 model ([019f7f0](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/019f7f0861b4ea1d184362a99089e9117d33fe3f))
* **noxToAirFertilizerAndExcretaStehfestBouwman2006:** fix bug in model noxToAirFertilizerAndExcretaStehfestBouwman2006 ([5a550af](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/5a550af108d4c5eb18af942a0e3ee5b18065a8b6))
* **noxToAirFertilizerAndExcretaStehfestBouwman2006:** fix noxToAirFertilizerAndExcretaStehfestBouwman2006 model ([ead1d48](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/ead1d48624163c18dca41ec1bbd74b6c52126ef0))

### [0.0.9](https://gitlab.com/hestia-earth/hestia-calculation-engine/compare/v0.0.8...v0.0.9) (2020-10-22)


### Features

* add hierarchies ([f012eba](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/f012eba2d3f62656b27f5f1f9f0cb31e94021ada))
* **co2ToAirFuelCombustionBiophysical:** add co2ToAirFuelCombustionBiophysical model ([5c308a3](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/5c308a33b9dedf9dfba6cdee1b0ac6bda6c873e5)), closes [#33](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/33)
* **n2OToAirCropResidueBurningAkagiEtAl2011AndIpcc2006:** add n2OToAirCropResidueBurningAkagiEtAl2011AndIpcc2006 model ([f0b84a7](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/f0b84a7d37ae6ee55bc27d783368a7fee6f04970)), closes [#35](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/35)
* **nh3ToAirCropResidueBurningAkagiEtAl2011AndIpcc2006:** add nh3ToAirCropResidueBurningAkagiEtAl2011AndIpcc2006 model ([74f56f1](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/74f56f1596b9034f7272d35f03094afb78612a42)), closes [#37](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/37)
* **noxToAirCropResidueBurningAkagiEtAl2011AndIpcc2006:** add noxToAirCropResidueBurningAkagiEtAl2011AndIpcc2006 model ([2225f50](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/2225f5090a6ef879b9f08614ee46c98f828c538f)), closes [#40](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/40)
* **noxToAirFertilizerAndExcretaStehfestBouwman2006Tier1:** add noxToAirFertilizerAndExcretaStehfestBouwman2006Tier1 model ([911c2a0](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/911c2a0cf7f140bba966e532423a6a6da2436b11)), closes [#49](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/49)


### Bug Fixes

* fix evalues() bug in several models ([6af44d0](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/6af44d0b9443ff73c6efd6b193e250d2fe0c490a))
* **no3ToGroundwaterAllInputsPooreNemecek2018:** fix check method bugs ([8a4cd80](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/8a4cd804caf40d79490d04d4b0eed86b32db344f))
* **no3ToGroundwaterAllInputsPooreNemecek2018:** fix problem with crop residue decomposition calculation ([bd2c4d7](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/bd2c4d7e70fda3081cff371676edba371e8a0391))

### [0.0.8](https://gitlab.com/hestia-earth/hestia-calculation-engine/compare/v0.0.7...v0.0.8) (2020-09-07)


### Bug Fixes

* **co2ToAirLimeAndUreaIpcc2006:** fix bug due to the change of version of the abstract engine ([d0fd8eb](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/d0fd8ebdc442dfcc918ea9278b748abe914fb2e1))
* **n2OToAirFertilizerAndExcretaDirectIpcc2006:** fix bug due to the change of version of the abstract engine ([72ef1c5](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/72ef1c508a755fa278d7fb7451a00d7e9afb4d4e))
* **n2OToAirFertilizerAndExcretaDirectIpcc2006:** fix bug due to the change of version of the abstract engine ([4651d74](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/4651d74316f6f38aafe1f2cea79ef3bbb9691a0b))
* **n2OToAirFertilizerAndExcretadirectStehfestBouwman2006:** fix bug after update ([e25ef26](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/e25ef26187efc45b15871fb974171fd68e828c85))
* **nh3ToAirCropResidueDecompositionDeRuijterEtAl2010:** fix check and calculation bugs ([c73ceb7](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/c73ceb7822e512c5b6a7c2009c4dd8954628be72))
* **no3ToGroundwaterAllInputsPooreNemecek2018:** fix bug due to the change of version of the abstract engine ([5c894a9](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/5c894a94c2c5e1338d6894be676b31120968bbae))

### [0.0.7](https://gitlab.com/hestia-earth/hestia-calculation-engine/compare/v0.0.6...v0.0.7) (2020-09-02)


### Features

* **nh3ToAirOrganicFertilizerWebbEtAl2012AndSintermannEtAl2012:** add full calculation ([57020ed](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/57020edc13aa6fe9f66e689e44e67b45f97ce545))
* **nh3ToAirOrganicFertilizerWebbEtAl2012AndSintermannEtAl2012:** add input checking to run the model ([2faf8ad](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/2faf8ad0d5875675e047030e655c2c4d17a8eb74))


### Bug Fixes

* **calculation_hierarchies:** update hierarchies with new schema version ([498008b](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/498008bc350269d92de01656c137ca044ee233e6))
* **nh3ToAirOrganicFertilizerWebbEtAl2012AndSintermannEtAl2012:** fix lookup reading format ([a0fd542](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/a0fd54224cbad2b36606cfebc2d075e58f15f8ac))
* **nh3ToAirOrganicFertilizerWebbEtAl2012AndSintermannEtAl2012:** output emission ([146bc0a](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/146bc0a4cd58e4bc1312133a1fb14d97d78c3830))
* **test_calculation:** update test with new schema version ([cd1b199](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/cd1b199330fbdd2d87af7aecdab39c6f2afaae93))

### [0.0.6](https://gitlab.com/hestia-earth/hestia-calculation-engine/compare/v0.0.5...v0.0.6) (2020-08-31)


### Bug Fixes

* **calculation_hierarchies:** fix methodTier error parser ([ff0c1a3](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/ff0c1a30ed112086e332c38fd4ab17d016703920)), closes [#32](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/32)
* **calculation_hierarchies:** use enum from schema lib ([df3b26c](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/df3b26c87ca3e0468fc953fb0436d07a0716e26c))

### [0.0.5](https://gitlab.com/hestia-earth/hestia-calculation-engine/compare/v0.0.4...v0.0.5) (2020-08-31)


### Features

* **ch4N2ONh3AndNoxToAirCropResidueBurningAkagiEtAl2011AndIpcc2006:** add ch4N2ONh3AndNoxToAirCropResidueBurningAkagiEtAl2011AndIpcc2006 model ([f7e8359](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/f7e83597d8cb46fb545216be0bf1d82716032189))
* **nh3ToAirInorganicFertilizerEuropeanEnvironmentAgency2016:** add emission formula ([7aacc15](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/7aacc15d7c3beb7353f69f79873681f488fb5405))
* **nh3ToAirInorganicFertilizerEuropeanEnvironmentAgency2016:** add model inputs ([1a56a7e](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/1a56a7e86f33ad8795a42ae4ecfc01d595a44690))
* **nh3ToAirInorganicFertilizerEuropeanEnvironmentAgency2016:** add template for model nh3ToAirInorganicFertilizerEuropeanEnvironmentAgency2016 ([8671b84](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/8671b846b19af83b9289e05214a9f8cf6d4278d1))
* **nh3ToAirInorganicFertilizerEuropeanEnvironmentAgency2016:** include condition to run the model ([80c5f59](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/80c5f59e04a4761002b124b35681c10b823029e0))
* **nh3ToAirInorganicFertilizerEuropeanEnvironmentAgency2016:** update model requirements ([1414273](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/141427339e3ff6447c88b6703c828329cd445460))


### Bug Fixes

* **calculation_hierarchies:** emission value as array ([277bf1e](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/277bf1e5197d487f229e8c36a006cfe6ed0ad84d))
* **co2ToAirLimeAndUreaIpcc2006:** Ammend co2ToAirUreaHydrolysis calculation ([13a12f9](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/13a12f916600669fd06ba772086730d431c8da59))
* **n2OToAirFertilizerAndExcretadirectStehfestBouwman2006:** fix formula for n2OToAirAllOriginsDirect ([b6db167](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/b6db167bcc0ba5dd157335dea8ea5f6721139ccf))
* **nh3ToAirInorganicFertilizerEuropeanEnvironmentAgency2016:** fix inputs bug ([a6ff265](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/a6ff2653a4f2bd625afdf502fecb6b6da8803e66))
* **nh3ToAirInorganicFertilizerEuropeanEnvironmentAgency2016:** prepare input data for matrix multiplication ([7556929](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/7556929f11c5dbfc12f15b914c720cf13d7d9128))
* **nh3ToAirInorganicFertilizerEuropeanEnvironmentAgency2016:** remove unwanted init ([b0721ab](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/b0721ab5778ebe711c32ad656fbd41002eebf1ef))
* **noxToAirFertilizerAndExcretaStehfestBouwman2006:** fix bug in the model ([03a36f8](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/03a36f874d801b897bbaab0c60494dbc86f704dc))
* **noxToAirFertilizerAndExcretaStehfestBouwman2006:** fix problem accessing lookup table ([359a864](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/359a864851cf9b7acd11dc7438f2fa5659658f85))

### [0.0.4](https://gitlab.com/hestia-earth/hestia-calculation-engine/compare/v0.0.3...v0.0.4) (2020-08-20)


### Bug Fixes

* fix unreadable version ([133e7af](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/133e7afed071fd76411649ec193a5f590b626f40))

### [0.0.3](https://gitlab.com/hestia-earth/hestia-calculation-engine/compare/v0.0.2...v0.0.3) (2020-08-20)


### Features

* **calculation:** add dataState and datatVersion to Emission ([a12f154](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/a12f1546f67c658550ea24a5fd6ffc5d37d0604e))
* **calculation:** add dataState and datatVersion to Emission ([d809511](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/d809511799f8fc9bbaf8feb07006c96e76a36173))
* **calculation:** add new engine representation ([c93fc15](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/c93fc15bb6dbfb643632bd02f4c880f86124a120))
* **calculation:** handle no site provided during import ([303508c](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/303508cf364f30821a7809df224e381d92166fa9))
* **calculation:** rename class name ([a7dbaa3](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/a7dbaa3b3ed299990121f7116138dcda174e8e9c))
* **calculation_hierarchies:** update emission only if version lower than the current one ([3b0e106](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/3b0e106f01fe56a1c4508ac3899820cb94318c7f))
* **co2ToAirLimeAndUreaIpcc2006:** add condition to run the model ([6f24e83](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/6f24e83d670939c40675541ca9e22383d903caf8))
* **co2ToAirLimeAndUreaIpcc2006:** add condition to run the model ([9728d39](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/9728d39361ab3b1498377123a525209dbc8be00e))
* **lookups:** add Burning_Drymatter_Emissions lookup table ([77fcdff](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/77fcdffe89bec8d078ff4237c9102f2d511808e4))
* **lookups:** add List_Climate_NOxN lookup table ([d64bb20](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/d64bb20c994fb2b2bb3f13b6d9c96567d3872b61))
* **models:** add nh3ToAirCropResidueDecompositionDeRuijterEtAl2010 model ([965d789](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/965d789b46d39bcf314823c0038e43162c3a8928))
* **models:** add no3no3ToGroundwaterAllInputsPooreNemecek2018 model ([bdc955f](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/bdc955f21e3025179b8430e1efb211e75439c151))
* **n2OToAirFertilizerAndExcretaDirectIpcc2006:** add condition to run the model ([84aa071](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/84aa07195772e4070b2b1907e9615384a50fadb6))
* **n2OToAirFertilizerAndExcretaDirectIpcc2006:** add condition to run the model ([9571f00](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/9571f00cbc58bd98adc5d428267998cbad01edea))
* **n2OToAirFertilizerAndExcretaDirectIpcc2006:** add condition to run the model ([0f9be58](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/0f9be584129c7d61373c594dba73e2d9b1549644))
* **n2OToAirFertilizerAndExcretadirectStehfestBouwman2006:** add condition to run the model ([9a05d2d](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/9a05d2d79b31de2e0015f34ad0c8311721327635))
* **n2OToAirFertilizerAndExcretadirectStehfestBouwman2006:** add condition to run the model ([8f6d814](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/8f6d814d94f4b9e679b1629851120ea77e5b9d20))
* **n2OToAirFertilizerAndExcretadirectStehfestBouwman2006:** full version model ([1c99090](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/1c99090e2427abf89e91878218451e5f8cc46876))
* **n2OToAirFertilizerAndExcretadirectStehfestBouwman2006:** full version model ([6be9419](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/6be9419092ff4dc9a9f218da69f5a9d178d6826d))
* **nh3ToAirCropResidueDecompositionDeRuijterEtAl2010:** add condition to run the model ([15a9012](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/15a901224ab5a9d78f7b3bafa6f9e58fe1901c58))
* **nh3ToAirCropResidueDecompositionDeRuijterEtAl2010:** add condition to run the model ([d0c7635](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/d0c7635512405d678b86991be80a4cac80c9c2c3))
* **no3ToGroundwaterAllInputsPooreNemecek2018:** add condition to run the model ([313f878](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/313f878ec138ebafbb327b4867e711de6b0f96b3))
* **no3ToGroundwaterAllInputsPooreNemecek2018:** add condition to run the model ([72f4db3](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/72f4db37b5d008a2109a4f67084fa4c332d03af4))
* **no3ToGroundwaterAllInputsPooreNemecek2018:** add condition to run the model ([952d4ed](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/952d4ed60107f4224d8f0a4c0828e4974ab8c1e5))
* **no3ToGroundwaterAllInputsPooreNemecek2018:** add condition to run the model ([ede16e7](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/ede16e7ecaed5421d7d39372486380c089b9b78d))
* **noxToAirFertilizerAndExcretaStehfestBouwman2006:** add condition to run the model ([77d491f](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/77d491ff93ae03961513205e7c3af0b2f103abcc)), closes [#12](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/12)
* **noxToAirFertilizerAndExcretaStehfestBouwman2006:** add condition to run the model ([a051245](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/a05124579eb40dfba569a7f1d05b958faebc2ed2)), closes [#12](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/12)
* **noxToAirFertilizerAndExcretaStehfestBouwman2006:** Add noxToAirFertilizerAndExcretaStehfestBouwman2006 model ([79bf11e](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/79bf11e1082945ee207f2ea3f70cf0bd7a392377))


### Bug Fixes

* fix hierarchies imports and methods after refactoring ([6d94d08](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/6d94d0816679c29022c3018a406282b4f3bd35b7))
* **calculation:** allow to import a cycle containing a site ([db46523](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/db4652327df3a3dd9c6ea73d42be14491452a44c)), closes [#17](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/17)
* **calculation:** update methodTier values ([4ee4c48](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/4ee4c48ce3a827d91d0acefa2e3b307642ff0ffc))
* **calculation_hierarchies:** format type properly ([0503450](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/0503450302d7497d8d803ec5ac1f439c1242e198))
* **calculation_hierarchies:** update abstrac_hierarchy interface ([0b07e21](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/0b07e211e2f0ffeb5a24f49e9c135274194c1c96))
* **lookups:** update atomic_weight_conversions values ([a8fb103](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/a8fb103f04351f255b98ef5ff0774868d1ff9dcb))
* **n2OToAirFertilizerAndExcretadirectStehfestBouwman2006:** check that crop name is in IPCC lookup table ([3e51af9](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/3e51af9a15c9ba5e435dbbfb6f1b3dcdf38b8143)), closes [#9](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/9)
* **n2OToAirFertilizerAndExcretadirectStehfestBouwman2006:** check that crop name is in IPCC lookup table ([d834dfe](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/d834dfed94616f1e255e105ae4449e5fbcd20d81)), closes [#9](https://gitlab.com/hestia-earth/hestia-calculation-engine/issues/9)
* **n2OToAirFertilizerAndExcretadirectStehfestBouwman2006:** fix missing return value ([b6ceb83](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/b6ceb830534dba6554692448ee1dbed719852713))
* **n2OToAirFertilizerAndExcretadirectStehfestBouwman2006:** update term refs with the new schema ids ([a9d2f87](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/a9d2f875eabcbde751096b7875364b88ef3c295b))
* **nh3ToAirCropResidueDecompositionDeRuijterEtAl2010:** fix bug in nitrogeContent access ([1139f3f](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/1139f3f4242d0396bbef1b1373a0a93862c25f37))
* **nh3ToAirCropResidueDecompositionDeRuijterEtAl2010:** fix missing return value ([329a9f0](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/329a9f0088e39f401d70830fb52a3fb56613f833))
* **no3ToGroundwaterAllInputsPooreNemecek2018:** fixing a few bugs in the model ([95d81de](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/95d81de124e381d12d6b98d249ae4fde3e8fa0dc))
* **no3ToGroundwaterAllInputsPooreNemecek2018:** fixing a few bugs in the model ([aaf2704](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/aaf27043fc28d604aa6db719b170d7c399b2a9f3))
* **no3ToGroundwaterAllInputsPooreNemecek2018:** update glossary refs with the new schema ids ([ff38c4f](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/ff38c4f2f2da38c766bc25e3bf4fa66c7cebbf95))
* **noxToAirFertilizerAndExcretaStehfestBouwman2006:** fix several bugs in the model ([47dd927](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/47dd9274666fe781a678943fac9679026c7fb8ab))
* **noxToAirFertilizerAndExcretaStehfestBouwman2006:** missing return value ([5590f6b](https://gitlab.com/hestia-earth/hestia-calculation-engine/commit/5590f6ba85aad2266cb2cc8b39960388814671be))
