const { readFileSync, writeFileSync, readdirSync, existsSync, lstatSync } = require('fs');
const { resolve, join } = require('path');

const ROOT = resolve(join(__dirname, '../'));
const encoding = 'UTF-8';
const version = require(join(ROOT, 'package.json')).version;

const VERSION_PATH = resolve(join(ROOT, 'hestia_earth/calculation', 'version.py'));
let content = readFileSync(VERSION_PATH, encoding);
content = content.replace(/VERSION\s=\s\'[\d\-a-z\.]+\'/, `VERSION = '${version}'`);
writeFileSync(VERSION_PATH, content, encoding);

const LAYER_REQUIREMENTS_PATH = resolve(join(ROOT, 'layer', 'requirements.txt'));
content = readFileSync(LAYER_REQUIREMENTS_PATH, encoding);
content = content.replace(/hestia_earth.calculation>=[\d\-a-z\.]+/, `hestia_earth.calculation>=${version}`);
writeFileSync(LAYER_REQUIREMENTS_PATH, content, encoding);

// Update the fixtures files to make sure we test on the new version
const FIXTURES_DIR = resolve(join(__dirname, '../', 'tests', 'fixtures'));

const recursiveFixtures = directory =>
  readdirSync(directory).flatMap(entry =>
    lstatSync(join(directory, entry)).isDirectory() ?
      recursiveFixtures(join(directory, entry)) :
      (join(directory, entry).endsWith('.jsonld') ? [join(directory, entry)] : [])
  );

const updateValue = value => [
  value,
  ...(value.emissions || []),
  ...(value.emissionsResourceUse || []),
  ...(value.impacts || [])
]
  .filter(node => (
    'recalculatedVersion' in node && !node.recalculatedVersion.includes('-1.0.0')
  ) || (
    'version' in node
  ))
  .map(node => {
    if ('recalculatedVersion' in node) {
      node.recalculatedVersion = node.recalculatedVersion.map(() => version);
    }
    if ('version' in node) {
      node.version = version;
    }
    return node;
  });

recursiveFixtures(join(FIXTURES_DIR))
  .filter(filePath => existsSync(filePath))
  .map(filePath => {
    try {
      const value = JSON.parse(readFileSync(filePath, encoding));
      const updates = Array.isArray(value) ? value.map(updateValue) : updateValue(value);

      if (updates.length) {
        writeFileSync(filePath, JSON.stringify(value, null, 2), encoding);
      }
    }
    catch (err) {
      console.error(err);
    }
  });
