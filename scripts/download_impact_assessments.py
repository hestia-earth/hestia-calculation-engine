import sys
import os
from concurrent.futures import ThreadPoolExecutor
import json
from hestia_earth.schema import SchemaType
from hestia_earth.utils.api import download_hestia, find_node_exact, find_related


names = ["Electricity, hydroelectric",
         "Electricity, oil",
         "Electricity, hard coal",
         "Electricity, nuclear",
         "Electricity, solar PV",
         "Electricity, wind",
         "Electricity, geothermal",
         "Electricity, biomass",
         "Electricity, natural gas",
         "Electricity, waste",
         "Liquefied petroleum gases (LPG)",
         "Motor gasoline",
         "Diesel",
         "Fuel oil",
         "Lubricant",
         "Wood pellets",
         "Bagasse",
         "Inorganic Nitrogen fertilizer, unspecified (as N)",
         "Inorganic Phosphorus fertilizer, unspecified (as P2O5)",
         "Inorganic Potassium fertilizer, unspecified (as K2O)",
         "Ammonium Bicarbonate (as N)",
         "Ammonium Chloride (as N)",
         "Ammonium Nitrate (as N)",
         "Ammonium Sulphate (as N)",
         "Anhydrous Ammonia (as N)",
         "Calcium Ammonium Nitrate (as N)",
         "Calcium Nitrate (as N)",
         "Nitric Acid (as N)",
         "Urea (as N)",
         "Urea Ammonium Nitrate (as N)",
         "Urea Ammonium Sulphate (as N)",
         "Diammonium Phosphate (as P2O5)",
         "Diammonium Phosphate (as N)",
         "Monoammonium Phosphate (as P2O5)",
         "Monoammonium Phosphate (as N)",
         "Monopotassium Phosphate (as P2O5)",
         "Phosphate Rock (as P2O5)",
         "Single Super Phosphate (as P2O5)",
         "Triple Super Phosphate (as P2O5)",
         "Potassium Chloride (as K2O)",
         "Potassium Nitrate (as K2O)",
         "Potassium Nitrate (as N)",
         "Potassium Sulphate (as K2O)",
         "Machinery infrastructure, depreciated amount per Cycle",
         "Polyethylene",
         "Lime",
         "Agricultural limestone",
         "Hydrated lime",
         "Burnt lime",
         "Dolomitic lime",
         "Aluminium",
         "Concrete"
         ]


def mkdirs(folder: str):
    if not os.path.isdir(folder):
        os.makedirs(folder)


def empty_dir(folder: str):
    files = list(map(lambda file: f"{folder}/{file}", os.listdir(folder)))
    files = list(filter(lambda file: file.endswith('.jsonld') and os.path.isfile(file), files))
    list(map(lambda file: os.unlink(file), files))


def download_assessment(assessment: dict): return download_hestia(assessment.get('@id'), SchemaType.IMPACTASSESSMENT)


def save_assessment(dest_folder: str):
    def save(assessment: dict):
        id = assessment['@id']
        print(f"saving {id}...")
        file_path = f"{dest_folder}/{id}.jsonld"
        with open(file_path, 'w') as f:
            json.dump(assessment, f, indent=2)
    return save


def main(args):
    dest_folder = args[0]

    mkdirs(dest_folder)
    empty_dir(dest_folder)

    actor = find_node_exact(SchemaType.ACTOR, {'email': 'community@hestia.earth'})
    nodes = find_related(SchemaType.ACTOR, actor.get('@id'), SchemaType.IMPACTASSESSMENT)
    with ThreadPoolExecutor() as executor:
        nodes = list(executor.map(download_assessment, nodes))
    assessments = list(filter(lambda node: node.get('product', {}).get('name') in names, nodes))
    print(f"found {len(assessments)} matching assessments")

    with ThreadPoolExecutor() as executor:
        return list(executor.map(save_assessment(dest_folder), assessments))


if __name__ == "__main__":
    main(sys.argv[1:])
