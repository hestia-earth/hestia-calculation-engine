#!/bin/bash

# Script to download all the data necessary prior to running the engine.

API_URL=$1
BASE=$PWD

echo "Downloading Impact Assessments..."

DEST="${2:-"./"}hestia_earth/calculation/data/impact_assessments"

pip install hestia_earth.schema hestia_earth.utils
API_URL=$API_URL python scripts/download_impact_assessments.py $DEST
