#!/bin/sh
API_URL=$1

docker build \
  -t hestia-calculation-engine:latest \
  --build-arg API_URL=$API_URL \
  .
docker run --rm \
  --name hestia-calculation-engine \
  -v ${PWD}:/app \
  hestia-calculation-engine:latest python run.py "$@"
