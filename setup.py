import pathlib
from setuptools import find_packages, setup

from hestia_earth.calculation.version import VERSION

# The directory containing this file
HERE = pathlib.Path(__file__).parent

# The text of the README file
README = (HERE / "README.md").read_text()

REQUIRES = (HERE / "requirements.txt").read_text().splitlines()

# This call to setup() does all the work
setup(
    name='hestia-earth-calculation',
    version=VERSION,
    description="Hestia's set of modules for creating structured data models from LCA observations and evaluating "
                "biogeochemical aspects of specific farming cycles",
    long_description=README,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/hestia-earth/hestia-calculation-engine",
    author="Hestia Team",
    author_email="juan.sabuco@ouce.ox.ac.uk",
    license="GPL",
    classifiers=[
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Programming Language :: Python :: 3.6",
    ],
    packages=find_packages(exclude=("envs", "tests", "scripts")),
    include_package_data=True,
    install_requires=REQUIRES
)
