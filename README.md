# Hestia Calculation Engine

[![Pipeline Status](https://gitlab.com/hestia-earth/hestia-calculation-engine/badges/master/pipeline.svg)](https://gitlab.com/hestia-earth/hestia-calculation-engine/commits/master)
[![Coverage Report](https://gitlab.com/hestia-earth/hestia-calculation-engine/badges/master/coverage.svg)](https://gitlab.com/hestia-earth/hestia-calculation-engine/commits/master)
[![Documentation Status](https://readthedocs.org/projects/hestia-calculation-engine/badge/?version=latest)](https://hestia-calculation-engine.readthedocs.io/en/latest/?badge=latest)

Hestia's set of modules for creating structured data models from LCA observations and evaluating biogeochemical aspects of specific farming cycles.

## Documentation

Official documentation can be found on [Read the Docs](https://hestia-calculation-engine.readthedocs.io/en/latest/index.html).

Additional models documentation can be found in the [source folder](./hestia_earth/calculation).

## Install

1. Install the module:
```bash
pip install hestia_earth.calculation
```

2. Download the latest data:
```bash
curl https://gitlab.com/hestia-earth/hestia-calculation-engine/-/raw/master/scripts/download_data.sh?inline=false -o download_data.sh && chmod +x download_data.sh
# pip default install directory is /usr/local/lib/python<version>/site-packages
./download_data.sh <API_URL> <pip install directory>
```

To use the production-ready version of the data, please use `API_URL=https://api.hestia.earth`. For development version, please contact us at <community@hestia.earth>

## Usage

```python
from hestia_earth.calculation import CalculationEngine
# Create CalculationEngine object
engine = CalculationEngine('url')
# ingest data ('cycle', 'site', 'assessments' are keyword arguments)
engine.import_data(cycle=ingested_cycle, site=ingested_site, assessments=ingested_assessments)
# do the calculations
engine.run()
# retrieve the calculated data
engine.export_data()
```
