# usage:
# 1. Install python-dotenv
# 2. Set your env variables in a `.env` file
# 3. Put your test cycle into the `samples` folder
# 3. Run `python run.py samples/cycle.jsonld`
from dotenv import load_dotenv
load_dotenv()


import sys
import json
from hestia_earth.utils.tools import current_time_ms
from hestia_earth.calculation import CalculationEngine


def calculate_cycle(data):
    engine = CalculationEngine('baseUrl')
    engine.import_data(cycle=data)
    engine.run()
    return engine.export_data()


def calculate_impact_assessment(data):
    engine = CalculationEngine('baseUrl')
    engine.import_data(assessments=[data])
    engine.run()
    [data] = engine.export_data()
    return data


def main(args):
    filepath = args[0]
    with open(filepath) as f:
        data = json.load(f)

    now = current_time_ms()
    print(f"processing {filepath}")
    data = calculate_cycle(data) if data['@type'] == 'Cycle' else calculate_impact_assessment(data)
    print(f"processed in {current_time_ms() - now} ms")

    [filename, ext] = filepath.split('.')
    with open(f"{filename}-recalculated.{ext}", 'w') as f:
        f.write(json.dumps(data, indent=2, ensure_ascii=False))


if __name__ == "__main__":
    main(sys.argv[1:])
